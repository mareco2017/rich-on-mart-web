const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/now-ui-dashboard.js', 'public/js')
   .js('resources/js/now-ui-kit.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/styles.scss', 'public/css')
   .sass('resources/sass/now-ui-dashboard.scss', 'public/css/now-ui-dashboard.css')
   .sass('resources/sass/now-ui-kit.scss', 'public/css/now-ui-kit.css');
   
if (mix.inProduction()) {
   mix.version();
}
