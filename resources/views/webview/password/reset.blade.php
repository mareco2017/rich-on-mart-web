@if (Session::get('error'))
	{{ Session::get('error') }}
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(['route' => 'webview.reset.password', 'method' => 'POST']) !!}

{!! Form::hidden('token', $token) !!}
{!! Form::text('email', '', array('placeholder' => 'Email')) !!}
{!! Form::password('password', array('placeholder' => 'Password')) !!}
{!! Form::password('password_confirmation', array('placeholder' => 'Password Confirmation')) !!}

<button type="submit">Submit</button>

{!! Form::close() !!}