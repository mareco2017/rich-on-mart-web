@extends('structure')
@section('title', $promo->title ?? 'Promo Detail')

@section('body')
<body>
    <div class="container px-0 promo-webview">
        <div class="bg-white shadow-sm">
            <p class="big-title">{{ $promo->title }}</p>
            <img class="img-fluid w-100" src="{{ asset($promo->cover_url) }}">
            <div class="description">
                @if ($promo->category == \App\Enums\PromoCategory::BANNER)
                <!-- <div class="d-flex align-items-center mb-3">
                    <i class="material-icons mr-2">access_time</i>
                    <div>
                        <p class="text-muted mb-0">Valid Till</p>
                        <p class="mb-0">{{ $promo->end_date->format('j M Y') }}</p>
                    </div>
                </div> -->
                @endif
                @if ($promo->description)
                <div>
                    <!-- <p class="text-muted mb-1">Description</p> -->
                    {!! $promo->description !!}
                </div>
                @endif
                @if ($promo->tnc)
                <hr>
                <a class="terms-button text-body" data-toggle="collapse" href="#collapseExample" data-target="#termsConditions" role="button" aria-expanded="false" aria-controls="termsConditions">
                    <div class="d-flex align-items-center">
                        <h5 class="mb-0">Terms and Conditions</h5>
                        <i class="material-icons ml-auto">expand_more</i>
                    </div>
                </a>
                <div id="termsConditions" class="collapse">
                    {!! $promo->tnc !!}
                </div>
                @endif
            </div>
        </div>
    </div>
</body>
@endsection