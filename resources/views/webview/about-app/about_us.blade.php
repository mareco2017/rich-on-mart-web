@extends('structure')
@section('title', 'About Us')

@section('body')
<body>
    <section>
        <div class="container">
            <div class="logo-text"></div>
            <hr>
            <div class="long-text">
                @if (isset($data) && $data)
                    {!! $data->value !!}
                @else
                    <p class="indent">Richonpay adalah Aplikasi digital payment dari Kota Batam untuk kemudahan transaksi anda. Richonpay dapat melakukan transaksi atau pembayaran seperti halnya ATM, Internet atau Mobile Banking, atau PPOB.</p>
                    <p class="indent">Dengan Richonpay, Handphone anda dapat melakukan pembelian atau pembayaran seperti isi ulang pulsa,isi paket data, Token listrik, Bayartagihan telepon, Listrik, PDAM, BPJS Kesehatan, Beli Tiket Pesawat dan Kereta Api, cicilan kendaraan, dan masih banyak lagi. Sangat mudah dan praktis tanpa harus keluar rumah, lebih hemat waktu dan tenaga, Bisa untuk transaksi pribadi maupun orang lain.</p>
                    <p class="indent">Richonpay memberikan kemudahan dan keutungan seperti adanya Cashback transaksi, komisi jaringan, dan juga Reward sehingga anda berpotensi meraih kesuksesan lebih cepat hanya dengan cara sederhana yaitu Memasyarakatkan Richonpay dan Me-richonpaykan masyarakat.</p>
                @endif
            </div>
        </div>
    </section>
</body>
@endsection