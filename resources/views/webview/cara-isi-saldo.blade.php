@extends('structure')
@section('title', 'Cara Isi Saldo ROM')

@section('body')
<body>
    <div class="container">
        <div class="py-3">
            <div class="mb-3">
                <img width="100%" src="/images/cara-top-up/1.png" alt="image-1">
            </div>
            <div class="mb-3">
                <img width="100%" src="/images/cara-top-up/2.png" alt="image-2">
            </div>
            <div class="mb-3">
                <img width="100%" src="/images/cara-top-up/3.png" alt="image-3">
            </div>
            <div class="mb-4">
                <img width="100%" src="/images/cara-top-up/4.png" alt="image-4">
            </div>
            <div class="text-center">
                <img width="70%" src="/images/cara-top-up/5.png" alt="image-5">
            </div>
        </div>
    </div>
</body>
@endsection