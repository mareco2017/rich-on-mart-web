@extends('structure')
@section('title', 'Informasi Paket')

@section('body')
<body class="bg-light">
    @if ($user)
    <div class="bg-white px-3 py-2 d-flex align-items-center">
        <p class="mb-0"><b>{{ $user->fullname }}</b></p>
        @if ($user->packages == 1)
        <img class="ml-auto" width="70" src="/images/package-info/silver-logo.png" alt="">
        @elseif ($user->packages == 2)
        <img class="ml-auto" width="70" src="/images/package-info/gold-logo.png" alt="">
        @elseif ($user->packages == 3)
        <img class="ml-auto" width="70" src="/images/package-info/platinum-logo.png" alt="">
        @endif
    </div>
    @endif
    <div class="container">
        <div class="py-3">
            <div class="mb-3">
                <img width="100%" src="/images/package-info/silver.png" alt="image-1">
            </div>
            <div class="mb-3">
                <img width="100%" src="/images/package-info/gold.png" alt="image-2">
            </div>
            <div class="mb-3">
                <img width="100%" src="/images/package-info/platinum.png" alt="image-3">
            </div>
            <div class="bg-white p-3 d-flex rounded">
                <i class="fab fa-whatsapp h5 mr-2" style="color: #4ced69"></i>
                <div>
                    <p class="mb-1 text-secondary">Pendaftaran paket silahkan hubungi:</p>
                    <p class="mb-0"><b>{{ $phoneNumber ?? 'Nomor belum di set' }}</b></p>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection