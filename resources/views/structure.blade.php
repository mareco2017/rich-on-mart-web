<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') Rich On Mart </title>
    {{-- <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/> --}}
	<link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script>
        FontAwesomeConfig = { searchPseudoElements: true };
    </script>
    <script src="{{ asset('js/fontawesome-all.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/styles.css') }}">
    @stack('pageRelatedCss')
</head>
@yield('body')
<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@stack('pageRelatedJs')