@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Bank Account @parent
@endsection
@section('groupName', 'Bank Account')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Bank Account</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.bank-account.create') }}">Create bank account</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Owner</th>
                    <th>Name</th>
                    <th>No</th>
                    <th>Bank</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.bank-account.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'owner', name: 'owner', className: 'text-center', sortable: false },
                { data: 'account_name', name: 'account_name', className: 'text-center' },
                { data: 'account_no', name: 'account_no', className: 'text-center' },
                { data: 'bank.name', name: 'bank.name', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');
	});
</script>
@endpush