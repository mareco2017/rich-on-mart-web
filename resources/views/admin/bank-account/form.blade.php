@extends('templates.admin.master')

@section('title')
    {{ $bankAccount->id ? 'Edit' : 'Create' }} Bank Account @parent
@endsection
@section('groupName', 'Bank Account')

@section('content')
@if (!$bankAccount->id)
{!! Form::open(['route' => 'admin.bank-account.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($bankAccount, ['route' => ['admin.bank-account.update', $bankAccount], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $bankAccount->id ? 'Edit' : 'Create' }} Bank Account</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="account_name" class="col-2 col-form-label">Account Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('account_name', old('account_name'), ['class' => 'form-control', 'id' => 'account_name', 'placeholder' => 'Account Name']) }}
                        @if($errors->has('account_name'))
                        <p class="status-text">{{ $errors->first('account_name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="account_no" class="col-2 col-form-label">Account Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('account_no', old('account_no'), ['class' => 'form-control', 'id' => 'account_no', 'placeholder' => 'Account Number']) }}
                        @if($errors->has('account_no'))
                        <p class="status-text">{{ $errors->first('account_no') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="bank_id" class="col-2 col-form-label">Select Bank</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('bank_id', $bankList, old('bank_id'), array('class' => 'form-control', 'id' => 'bankList', 'placeholder' => 'Bank List')) }}
                        @if($errors->has('bank_id'))
                        <p class="status-text">{{ $errors->first('bank_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="admin_id" class="col-2 col-form-label">Select Admin</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('admin_id', $adminList, $bankAccount->id ? $bankAccount->ownable->id : old('admin_id'), array('class' => 'form-control', 'id' => 'admin_id', 'placeholder' => 'Admin List')) }}
                        @if($errors->has('admin_id'))
                        <p class="status-text">{{ $errors->first('admin_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="" class="col-2 col-form-label">Set Primary</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                {{ Form::radio('is_primary', 1, $bankAccount->id ? $bankAccount->is_primary : true, array('id' => 'primary', 'class' => 'form-check-input')) }}
                                <span class="form-check-sign"></span>
                                Primary
                            </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                {{ Form::radio('is_primary', 0, $bankAccount->is_primary, array('id' => 'primary', 'class' => 'form-check-input')) }}
                                <span class="form-check-sign"></span>
                                Not Primary
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-primary"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection