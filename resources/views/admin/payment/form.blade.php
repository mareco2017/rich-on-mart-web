@extends('templates.admin.master')

@section('title')
    {{ $payment->id ? 'Edit' : 'Create' }} Payment @parent
@endsection

@section('groupName', 'Payments')

@section('content')
@if (!$payment->id)
{!! Form::open(['route' => 'admin.payments.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($payment, ['route' => ['admin.payments.update', $payment], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $payment->id ? 'Edit' : 'Create' }} Payments</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label class="col-2 col-form-label">Icon</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ $payment->id && $payment->cover_url ? $payment->cover_url : asset('/images/img-placeholder.png') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                            <div>
                                <span class="btn btn-info btn-round btn-file">
                                    <span class="fileinput-new"><i class="fas fa-image"></i> Select image</span>
                                    <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                                    {{ Form::file('icon', ["accept" => "image/x-png,image/gif,image/jpeg"]) }}
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                            </div>
                        </div>
                        @if($errors->has('icon'))
                        <p class="status-text">{{ $errors->first('icon') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="title" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title']) }}
                        @if($errors->has('title'))
                        <p class="status-text">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="code" class="col-2 col-form-label">Code</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('code', old('code'), ['class' => 'form-control', 'id' => 'code', 'placeholder' => 'Code']) }}
                        @if($errors->has('code'))
                        <p class="status-text">{{ $errors->first('code') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="price" class="col-2 col-form-label">Price</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('price', old('price'), ['class' => 'form-control', 'id' => 'price', 'placeholder' => 'Price']) }}
                        @if($errors->has('price'))
                        <p class="status-text">{{ $errors->first('price') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="discount" class="col-2 col-form-label">Discount</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('discount', old('discount'), ['class' => 'form-control', 'id' => 'discount', 'placeholder' => 'Discount']) }}
                        @if($errors->has('discount'))
                        <p class="status-text">{{ $errors->first('discount') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="fee" class="col-2 col-form-label">Fee</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('fee', old('fee'), ['class' => 'form-control', 'id' => 'fee', 'placeholder' => 'Fee']) }}
                        @if($errors->has('fee'))
                        <p class="status-text">{{ $errors->first('fee') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="type" class="col-2 col-form-label">Type</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('type', $types, old('type'), array('class' => 'form-control', 'id' => 'type', 'placeholder' => 'Select Type')) }}
                        @if($errors->has('type'))
                        <p class="status-text">{{ $errors->first('type') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="active" class="col-2 col-form-label">Active</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::checkbox('active', true, old('active'), array('class' => 'form-control', 'id' => 'active', 'placeholder' => 'Select Active')) }}
                        @if($errors->has('active'))
                        <p class="status-text">{{ $errors->first('active') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
@endpush