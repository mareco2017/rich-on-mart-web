@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Payment @parent
@endsection

@section('groupName', 'Payments')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">External</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <!-- <a class="dropdown-item" href="{{ route('admin.payments.create') }}">Create product</a> -->
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterType">Filter Type</label>
                    {{ Form::select('status', $types, '', ['class' => 'form-control', 'id' => 'filterType', 'placeholder' => 'Filter Type']) }}
                </div>
                <div class="form-group">
                    <label for="filterType">Filter Voucher</label>
                    {{ Form::select('filter_voucher', [], '', ['class' => 'form-control', 'id' => 'filterVoucher', 'placeholder' => 'Filter Voucher']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Product Id</th>
                    <th>Voucher</th>
                    <th>Nominal</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterType = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: false,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.payments.external-ajax') !!}',
                data: function(d) { 
                    d.type = filterType;
                },
				type: 'POST'
			},
			columns: [
                { data: 'produk_id', name: 'produk_id', className: 'text-center' },
                { data: 'voucher', name: 'voucher', className: 'text-center' },
                { data: 'nominal', name: 'nominal', className: 'text-center' },
                { data: 'harga', name: 'harga', className: 'text-center' },
			],
            initComplete(settings, json) {
                setVoucherList(json.data);
            }
		});

		$(table.table().container()).removeClass( 'form-inline');

        $('#filterType').on('change', function(e) { 
            filterType = e.currentTarget.value; 
            table.draw(); 
        });

        $('#filterVoucher').on('change', function(e) { 
            table.columns(1)
            .search("^" + e.currentTarget.value + "$", true, false, true)
            .draw(); 
        });

        function setVoucherList(d) {
            var select = document.getElementById('filterVoucher');
            const map = new Map();

            for (const item of d) {
                if (!map.has(item.voucher)) {
                    map.set(item.voucher, true);
                    var option = document.createElement('option');
                    option.appendChild(document.createTextNode(item.voucher));
                    option.value = item.voucher;
                    select.appendChild(option);
                }
            }
            return;
        }
	});
</script>
@endpush