@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Payment @parent
@endsection

@section('groupName', 'Payments')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Payment</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.payments.create') }}">Create product</a>
                <a class="dropdown-item" href="{{ route('admin.payments.external') }}">External (Nusantara)</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterType">Filter Type</label>
                    {{ Form::select('status', $types, '', ['class' => 'form-control', 'id' => 'filterType', 'placeholder' => 'Filter Type']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Icon</th>
                    <th>Title</th>
                    <th>Code</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Fee</th>
                    <th>Type</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterType = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.payments.ajax') !!}',
                data: function(d) { 
                    d.type = filterType;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'icon', name: 'icon', className: 'text-center', orderable: false, searchable: false, render: function (data, type, full, meta) {
                        if (!data) return '';
                        var str = '<a href="'+ data +'" target="_blank"><img src="'+ data +'"/></a>';
                        return str;
                    }
                },
                { data: 'title', name: 'title', className: 'text-center' },
                { data: 'code', name: 'code', className: 'text-center' },
                { data: 'price', name: 'price', className: 'text-center' },
                { data: 'discount', name: 'discount', className: 'text-center' },
                { data: 'fee', name: 'fee', className: 'text-center' },
                { data: 'type', name: 'type', className: 'text-center' },
                { data: 'active', name: 'active', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false  }
			]
		});

		$(table.table().container()).removeClass( 'form-inline');

        $('#filterType').on('change', function(e) { 
            filterType = e.currentTarget.value; 
            table.draw(); 
        });
	});
</script>
@endpush