@extends('templates.admin.master')

@section('title')
    {{ $product->id ? 'Edit' : 'Create' }} Product @parent
@endsection

@section('groupName', 'Products')

@section('content')
@if (!$product->id)
{!! Form::open(['route' => 'admin.products.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($product, ['route' => ['admin.products.update', $product], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $product->id ? 'Edit' : 'Create' }} Products</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="title" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title']) }}
                        @if($errors->has('title'))
                        <p class="status-text">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="price" class="col-2 col-form-label">Price<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('price', old('price'), ['class' => 'form-control', 'id' => 'price', 'placeholder' => 'Price']) }}
                        @if($errors->has('price'))
                        <p class="status-text">{{ $errors->first('price') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="discount" class="col-2 col-form-label">Discount<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('discount', old('discount'), ['class' => 'form-control', 'id' => 'discount', 'placeholder' => 'Discount']) }}
                        @if($errors->has('discount'))
                        <p class="status-text">{{ $errors->first('discount') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="unit" class="col-2 col-form-label">Unit</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('unit', $unitList, old('unit'), array('class' => 'form-control', 'id' => 'unit', 'placeholder' => 'Select Unit')) }}
                        @if($errors->has('unit'))
                        <p class="status-text">{{ $errors->first('unit') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="category" class="col-2 col-form-label">Category</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('category', $categories, old('category') ? old('category') : $product->category ? $product->category->id : '', array('class' => 'form-control', 'id' => 'category', 'placeholder' => 'Select Category')) }}
                        @if($errors->has('category'))
                        <p class="category-text">{{ $errors->first('category') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="description" class="col-2 col-form-label">Description<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('description', old('description'), array('class' => 'form-control', 'id' => 'description', 'placeholder' => 'Apel fuji pertama kali dikembangkan di negara Jepang pada tahun 1930-an. Apel tersebut merupakan seleksi dari persilangan antara Ralls Janet dan Red Delicious...')) }}
                        @if($errors->has('description'))
                        <p class="status-text">{{ $errors->first('description') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Cover</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ $product->id && $product->cover_url ? $product->cover_url : asset('/images/img-placeholder.png') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                            <div>
                                <span class="btn btn-primary btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                                    {{ Form::file('cover') }}
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                            </div>
                        </div>
                        @if($errors->has('cover'))
                        <p class="status-text">{{ $errors->first('cover') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
@endpush