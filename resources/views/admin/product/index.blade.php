@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Product @parent
@endsection

@section('groupName', 'Products')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Product</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.products.create') }}">Create product</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control', 'id' => 'filterStatus', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterUnit">Filter Unit</label>
                    {{ Form::select('unit', $unitList, '', ['class' => 'form-control', 'id' => 'filterUnit', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterCategory">Filter Category</label>
                    {{ Form::select('category', $categories, '', ['class' => 'form-control', 'id' => 'filterCategory', 'placeholder' => 'Filter Category']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Unit</th>
                    <th>Category</th>
                    <th>C. Commission</th>
                    <th>Calculated Price</th>
                    <th>Cover</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;
        var filterUnitEnum = null;
        var filterCategory = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.products.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                    d.unit = filterUnitEnum;
                    d.category = filterCategory;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'title', name: 'title', className: 'text-center' },
                { data: 'price', name: 'price', className: 'text-center', render: function (data) {
                    return 'Rp'+data;
                }},
                { data: 'discount', name: 'discount', className: 'text-center', render: function (data) {
                    return 'Rp'+data;
                }},
                { data: 'unit', name: 'unit', className: 'text-center' },
                { data: 'category.title', name: 'category.title', className: 'text-center' },
                { data: 'category.commission', name: 'category.commission', className: 'text-center' },
                { data: 'calculated_price', name: 'calculated_price', className: 'text-center', render: function (data) {
                    return 'Rp'+data;
                }},
                { data: 'cover', name: 'cover', className: 'text-center', orderable: false, searchable: false, render: function (data, type, full, meta) {
                    if (!data) return '';
                    var str = '<a href="'+ data +'" target="_blank"><img id="previewImage" class="sm" src="'+ data +'"/></a>';
                    return str;
                }},
                { data: 'status', name: 'status', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false  }
			]
		});
		$(table.table().container()).removeClass( 'form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 

        $('#filterUnit').on('change', function(e) { 
            filterUnitEnum = e.currentTarget.value; 
            table.draw(); 
        }); 

        $('#filterCategory').on('change', function(e) { 
            filterCategory = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush