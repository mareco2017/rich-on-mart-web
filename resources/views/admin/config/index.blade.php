@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Config @parent
@endsection
@section('groupName', 'Config')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Config List</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.config.create') }}">Create new config</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Key</th>
                    <th>Value</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.config.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'key', name: 'key', className: 'text-center' },
                { data: 'value', name: 'value', render: function (data, type, full, meta) {
                    return `<div class="html-render-box">${data}</div>`;
                    }
                },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
			]
		});
        $(table.table().container()).removeClass('form-inline');
	});
</script>
@endpush