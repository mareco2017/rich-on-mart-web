@extends('templates.admin.master')

@section('title')
    {{ $promo->id ? 'Edit' : 'Create' }} Banner @parent
@endsection

@section('groupName', 'Promos')

@section('content')
@if (!$promo->id)
{!! Form::open(['route' => 'admin.promos.banner.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($promo, ['route' => ['admin.promos.banner.update', $promo], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $promo->id ? 'Edit' : 'Create' }} Banner</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="title" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title']) }}
                        @if($errors->has('title'))
                        <p class="status-text">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="desc" class="col-2 col-form-label">Description<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('description', old('description'), array('class' => 'form-control d-none', 'id' => 'desc', 'placeholder' => 'Banner Description (Optional)')) }}
                        @if($errors->has('description'))
                        <p class="status-text">{{ $errors->first('description') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="tnc" class="col-2 col-form-label">Terms & Conditions<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('tnc', old('tnc'), array('class' => 'form-control d-none', 'id' => 'tnc', 'placeholder' => 'Terms & Conditions (Optional)')) }}
                        @if($errors->has('tnc'))
                        <p class="status-text">{{ $errors->first('tnc') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="category" class="col-2 col-form-label">Banner Category</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('category', $promoCategories, old('category'), array('class' => 'form-control', 'id' => 'category', 'placeholder' => 'Select Banner Category')) }}
                        @if($errors->has('category'))
                        <p class="category-text">{{ $errors->first('category') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="startDate" class="col-2 col-form-label">Start Date</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('start_date', old('start_date'), array('class' => 'form-control datepickers', 'id' => 'startDate', 'placeholder' => 'Start Date')) }}
                        @if($errors->has('start_date'))
                        <p class="status-text">{{ $errors->first('start_date') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="endDate" class="col-2 col-form-label">End Date</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('end_date', old('end_date'), array('class' => 'form-control datepickers', 'id' => 'endDate', 'placeholder' => 'End Date')) }}
                        @if($errors->has('end_date'))
                        <p class="status-text">{{ $errors->first('end_date') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Cover</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ $promo->id && $promo->cover_url ? $promo->cover_url : asset('/images/img-placeholder.png') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                            <div>
                                <span class="btn btn-primary btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                                    {{ Form::file('cover') }}
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                            </div>
                        </div>
                        @if($errors->has('cover'))
                        <p class="status-text">{{ $errors->first('cover') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var inputStart = $('input[name="start_date"]');
    var inputEnd = $('input[name="end_date"]');

    inputStart.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: new Date(),
        todayHighlight: true
    });

    inputStart.on('change', function() {
        let val = inputStart.datepicker('getDates');
        let endVal = new Date(inputEnd.val());
        // let endVal = inputEnd.datepicker('getDates');

        if (val.length != 0) {
            if (endVal.length != 0 && val[0] > endVal) {
                inputEnd.val('');
            }

            inputEnd.datepicker('destroy');
            inputEnd.datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                startDate: val[0]
            });

            inputEnd.prop('disabled', false);
        } else {
            inputEnd.prop('disabled', true);
        }
    });

    CKEDITOR.replace('description', {
        height: 300
    });

    CKEDITOR.replace('tnc', {
        height: 300
    });

});
</script>
@endpush