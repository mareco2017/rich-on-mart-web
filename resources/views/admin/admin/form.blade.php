@extends('templates.admin.master')

@section('title', ($admin->id ? 'Edit' : 'Create').' Admin')
@section('groupName', 'Admins')

@section('content')
@if (!$admin->id)
{!! Form::open(['route' => 'admin.admins.admin.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($admin, ['route' => ['admin.admins.admin.update', $admin], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $admin->id ? 'Edit' : 'Create' }} Admin</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="firstName" class="col-2 col-form-label">First Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'id' => 'firstName', 'placeholder' => 'First Name']) }}
                        @if($errors->has('first_name'))
                        <p class="status-text">{{ $errors->first('first_name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="lastName" class="col-2 col-form-label">Last Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'id' => 'lastName', 'placeholder' => 'Last Name']) }}
                        @if($errors->has('last_name'))
                        <p class="status-text">{{ $errors->first('last_name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="phoneNumber" class="col-2 col-form-label">Phone Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('phone_number', old('phone_number'), ['class' => 'form-control', 'id' => 'phoneNumber', 'placeholder' => 'Phone Number']) }}
                        @if($errors->has('phone_number'))
                        <p class="status-text">{{ $errors->first('phone_number') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="email" class="col-2 col-form-label">Email</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::email('email', old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) }}
                        @if($errors->has('email'))
                        <p class="status-text">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            @if (!$admin->id)
            <div class="row">
                <label for="password" class="col-2 col-form-label">Password</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password']) }}
                        @if($errors->has('password'))
                        <p class="status-text">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection