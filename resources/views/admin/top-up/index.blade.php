@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Top Up @parent
@endsection
@section('groupName', 'Top Up')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Top Up</h4>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Reference Number</th>
                    <th>Status</th>
                    <th>Request By</th>
                    <th>Date / Time</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div class="card card-chart">
    <p>Top Up Completed amount : Rp{{ number_format($floatingWallet, 2,',','.') }} </p>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.top-up.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                },
				type: 'POST'
			},
			columns: [
				{ data: 'id', name: 'id', className: 'text-center' },
				{ data: 'reference_number', name: 'reference_number', className: 'text-center' },
				{ data: 'status', name: 'status', className: 'text-center' },
                { data: 'created_by', name: 'created_by', className: 'text-center' },
				{ data: 'created_at', name: 'created_at', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush