@extends('templates.admin.master')

@section('title', 'Top Up Confirmation')
@section('groupName', 'Top Up')

@section('content')
@include('templates.modal.modal-decline', ['choices' => $choices])
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Top Up Confirmation</h3>
    </div>
    <div class="card-body">
        <form class="form-horizontal">
            <div class="row">
                <label for="referenceNumber" class="col-2 col-form-label">Reference Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('reference_number', $order->reference_number, ['class' => 'form-control', 'id' => 'referenceNumber', 'readonly', 'placeholder' => 'Reference Number']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="fullname" class="col-2 col-form-label">Request By</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('fullname', $ownable->fullname, ['class' => 'form-control', 'id' => 'fullname', 'readonly', 'placeholder' => 'Fullname']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="amount" class="col-2 col-form-label">Amount</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('amount', GlobalHelper::toCurrency($order->total_before_discount), ['class' => 'form-control', 'id' => 'amount', 'readonly', 'placeholder' => 'Amount']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="discount" class="col-2 col-form-label">Discount</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('discount', GlobalHelper::toCurrency($order->total_discount), ['class' => 'form-control', 'id' => 'discount', 'readonly', 'placeholder' => 'Discount']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="total" class="col-2 col-form-label">Total</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('total', GlobalHelper::toCurrency($order->total), ['class' => 'form-control', 'id' => 'total', 'readonly', 'placeholder' => 'Total']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="dateTime" class="col-2 col-form-label">Date / Time</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('date_time', $order->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i'), ['class' => 'form-control', 'id' => 'dateTime', 'readonly', 'placeholder' => 'Date / Time']) }}
                    </div>
                </div>
            </div>
            @if (isset($order->virtual->bank_account))
                <div class="row">
                    <label class="col-2 col-form-label">Bank</label>
                    <div class="col-10">
                        <div class="form-group">
                            {{ Form::text('', $order->virtual->bank_account ? $order->virtual->bank_account->bank->name : '', ['class' => 'form-control', 'disabled']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-2 col-form-label">Account Name</label>
                    <div class="col-10">
                        <div class="form-group">
                            {{ Form::text('', $order->virtual->bank_account ? $order->virtual->bank_account->account_name : '', ['class' => 'form-control', 'disabled']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-2 col-form-label">Account No</label>
                    <div class="col-10">
                        <div class="form-group">
                            {{ Form::text('', $order->virtual->bank_account ? $order->virtual->bank_account->account_no : '', ['class' => 'form-control', 'disabled']) }}
                        </div>
                    </div>
                </div>
            @endif
            <hr>
            <h4 class="card-title mt-2 mb-4">Destination Bank Account</h4>
            <div class="row">
                <label class="col-2 col-form-label">Bank</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $destBankAccount ? $destBankAccount->bank->name : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Account Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $destBankAccount ? $destBankAccount->account_name : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Account No</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $destBankAccount ? $destBankAccount->account_no : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($order->isActive())
        <div class="text-center">
            <div class="d-inline-block mx-1">
                <button type="button" class="btn btn-round btn-danger" data-url="{{ route('admin.top-up.decline', ['order' => $order]) }}" data-toggle="modal" data-target="#modalDecline"><i class="fas fa-times-circle"></i> Decline</button>
            </div>
            <div class="d-inline-block mx-1">
                {!! Form::open(['route' => ['admin.top-up.approve', $order], 'method' => 'post']) !!}
                    <button type="submit" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Approve</button>
                {!! Form::close() !!}
            </div>
        </div>
        @else
        <div class="text-center">
            <div class="d-inline-block mx-1">
                <button type="button" class="btn btn-round btn-info">{{ OrderStatus::getString($order->status) }}</button>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection