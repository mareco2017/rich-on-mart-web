@extends('templates.admin.master')

@section('title')
    {{ $order->reference_number }} Detail @parent
@endsection
@section('groupName', 'Order')

@section('content')
<form action="" class="form-horizontal">
    <div class="row">
        <div class="col-12 col-lg-9">
            <div class="row mb-4">
                <div class="col-12 col-lg-7">
                    <div class="card h-100 mb-0">
                        <div class="card-header">
                            <h4 class="card-title">Transaction Detail</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-2 col-form-label">Reference Number</label>
                                <div class="col-10">
                                    <div class="form-group">
                                        {{ Form::text('', $order->reference_number, array('class' => 'form-control', 'readonly', 'title' => $order->reference_number.' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-2 col-form-label">Order Type</label>
                                <div class="col-10">
                                    <div class="form-group">
                                        {{ Form::text('', OrderType::getString($order->type), array('class' => 'form-control', 'readonly', 'title' => OrderType::getString($order->type).' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-2 col-form-label">Status</label>
                                <div class="col-10">
                                    <div class="form-group">
                                        {{ Form::text('', OrderStatus::getString($order->status), array('class' => 'form-control', 'readonly', 'title' => OrderStatus::getString($order->status).' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-2 col-form-label">Total Payment</label>
                                <div class="col-10">
                                    <div class="form-group">
                                        {{ Form::text('', GlobalHelper::toCurrency($order->total), array('class' => 'form-control', 'readonly', 'title' => GlobalHelper::toCurrency($order->total).' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="card h-100 mb-0">
                        <div class="card-header">
                            <a class="float-right h6 pr-2 pt-3" title="See Detail" target="_blank" href="{{ route('admin.users.user.detail', $order->ownable) }}"><i class="fas fa-external-link-alt"></i></a>
                            <h4 class="card-title">Ordered By</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-3 col-form-label">Fullname</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', ($order->ownable ? $order->ownable->fullname : '-'), array('class' => 'form-control', 'readonly', 'title' => ($order->ownable ? $order->ownable->fullname : '-').' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-3 col-form-label">Email</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', ($order->ownable ? $order->ownable->email : '-'), array('class' => 'form-control', 'readonly', 'title' => ($order->ownable ? $order->ownable->email : '-').' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-3 col-form-label">Phone Number</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', ($order->ownable ? $order->ownable->phone_number : '-'), array('class' => 'form-control', 'readonly', 'title' => ($order->ownable ? $order->ownable->phone_number : '-').' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <label class="col-3 col-form-label">Created At</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', $order->created_at->setTimezone('Asia/Jakarta'), array('class' => 'form-control', 'readonly', 'title' => $order->created_at->setTimezone('Asia/Jakarta').' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Products</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($order->orderDetails as $detail)
                        <div class="col-12 col-lg-6">
                            <h6 class="text-center my-3">Product - {{ $loop->iteration }}</h6>
                            <div class="row">
                                <div class="col-10">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Name</label>
                                        <div class="col-9">
                                            <div class="form-group">
                                                {{ Form::text('', $detail->productable ? $detail->productable->title : '-', array('class' => 'form-control', 'readonly', 'title' => $detail->productable ? $detail->productable->title : '-'.' | Readonly')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-3 col-form-label">Price</label>
                                        <div class="col-9">
                                            <div class="form-group">
                                                {{ Form::text('', GlobalHelper::toCurrency($detail->price), array('class' => 'form-control', 'readonly', 'title' => GlobalHelper::toCurrency($detail->price).' | Readonly')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-3 col-form-label">Quantity</label>
                                        <div class="col-9">
                                            <div class="form-group">
                                                {{ Form::text('', $detail->qty, array('class' => 'form-control', 'readonly', 'title' => $detail->qty.' | Readonly')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-3 col-form-label">Discount</label>
                                        <div class="col-9">
                                            <div class="form-group">
                                                {{ Form::text('', GlobalHelper::toCurrency($detail->discount), array('class' => 'form-control', 'readonly', 'title' => GlobalHelper::toCurrency($detail->discount).' | Readonly')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-3 col-form-label">Total</label>
                                        <div class="col-9">
                                            <div class="form-group">
                                                {{ Form::text('', GlobalHelper::toCurrency($detail->total - $detail->discount), array('class' => 'form-control', 'readonly', 'title' => GlobalHelper::toCurrency($detail->total - $detail->discount).' | Readonly')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 d-flex">
                                    <button type="button" class="btn btn-danger btn-round btn-icon m-auto">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Manage Status</h4>
                </div>
                <div class="card-body">
                    <p class="text-muted">NB: Click to set Status</p>
                    @foreach($statusList as $k => $status)
                        @php
                            $active = $k == $order->status;
                        @endphp
                        <a href="{{ route('admin.order.set-status', [$order->id, $k]) }}" class="btn btn-block btn-round btn-secondary text-left {{ $active ? 'btn-primary' : 'btn-secondary' }}">
                            <i class="fas fa-check mr-2 {{ $active ? 'visible' : 'invisible' }}"></i>
                            <span>{{ $status }}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>
@endsection