@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Order @parent
@endsection
@section('groupName', 'Order')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Order List</h4>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col">
                <div class="form-group">
                    <label for="filterType">Filter Type</label>
                    {{ Form::select('type', $typeList, '', ['class' => 'form-control nullable', 'id' => 'filterType', 'placeholder' => 'Show All Type']) }}
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Show All Status']) }}
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="startDate">Filter Start Date</label>
                    {{ Form::text('start_date', old('start_date'), array('class' => 'form-control datepickers', 'id' => 'startDate', 'autocomplete' => 'off', 'placeholder' => 'Start Date')) }}
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="endDate">Filter End Date</label>
                    {{ Form::text('end_date', old('end_date'), array('class' => 'form-control datepickers', 'id' => 'endDate', 'autocomplete' => 'off', 'placeholder' => 'End Date')) }}
                </div>
            </div>
            <div class="px-3 form-group d-flex align-items-center mt-3">
                <label class="mb-0 mr-2" for="showActive">Show Active</label><br>
                <div class="pr-3">
                    <input id="showActive" type="checkbox" name="showActive" class="bootstrap-switch" data-on-label="ON" data-off-label="OFF"/>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Reference No</th>
                    <th>Type</th>
                    <th>Owner</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th colspan="4" style="text-align:right"></th>
                    <th></th>
                    <th style="text-align:right">Total:</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-switch.min.js') }}"></script>
<script>
	$(document).ready(function(){
        var filterTypeEnum = null;
        var filterStatusEnum = null;
        var filterStartDate = null;
        var filterEndDate = null;
        var showActive = null;

        var inputStart = $('input[name="start_date"]');
        var inputEnd = $('input[name="end_date"]');

        inputStart.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true
        });

        inputEnd.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,
            endDate: new Date()
        });

		var table = $('#table').DataTable(
		{
            language: {
                search: `
                <a class="search-icon" href="#" data-toggle="tooltip" data-html="true" data-placement="left" 
                   title='
                   <p class="mb-1">Searchable Field:</p>
                   <span class="tooltip-label">Id</span>
                   <span class="tooltip-label">Reference Number</span>
                   '>
                    <i class="fas fa-question-circle search-tooltip"></i>
                </a> Search`
            },
            "initComplete": function(settings, json) {
                $('[data-toggle="tooltip"]').tooltip(); 
            },
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.order.ajax') !!}',
                data: function(d) { 
                    d.type = filterTypeEnum;
                    d.status_enum = filterStatusEnum;
                    d.start_date = filterStartDate;
                    d.end_date = filterEndDate;
                    d.show_active = showActive;
                },
				type: 'POST'
			},
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(), data;
     
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[^0-9\,]+/g,"").replace(",", ".")*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Total over this page
                pageTotal = api
                    .column(4, {page: 'current'})
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Update footer
                $(api.column(4).footer()).html(toCurrency(pageTotal));
                $(api.column(6).footer()).html(toCurrency(total));
            },
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'reference_number', name: 'reference_number', className: 'text-center' },
                { data: 'type', name: 'type', className: 'text-center' },
                { data: 'owner', name: 'owner', className: 'text-center', sortable: false, searchable: false  },
                { data: 'total', name: 'total', className: 'text-center' },
                { data: 'status', name: 'status', className: 'text-center' },
                { data: 'created_at', name: 'created_at', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
			]
		});
        $(table.table().container()).removeClass('form-inline');
        
        $('#filterType').on('change', function(e) { 
            filterTypeEnum = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#startDate').on('change', function(e) { 
            filterStartDate = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#endDate').on('change', function(e) { 
            filterEndDate = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#showActive').on('switchChange.bootstrapSwitch', function (event, state) {
            showActive = state; 
            table.draw(); 
        });

        function toCurrency(value) {
            return "Rp" + (value).toFixed(2) // always two decimal digits
                                .replace('.', ',') // replace decimal point character with ,
                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        }
	});
</script>
@endpush