@extends('templates.admin.master')

@section('title')
    User Detail @parent
@endsection
@section('groupName', 'Users')

@section('content')
@php
    $document = $user->getDocument(UserDocumentType::NRIC);
    if ($document) {
        $verified = $document->status == UserVerificationStatus::VERIFIED;
    } else {
        $verified = false;
    }
@endphp
<div class="row">
    <div class="col-4">
        <div class="card card-user">
            <div class="image">
                <img src="{{ asset('/images/admin-bg.jpg') }}" alt="...">
            </div>
            <div class="card-body">
                <div class="author">
                    <a href="#">
                        <img class="avatar border-gray" src="{{ asset('/images/admin-placeholder.svg') }}" alt="...">
                        <h5 class="title">{{ $user->full_name }}</h5>
                    </a>
                    <p class="description">
                        @if ($verified)
                        <img class="verified" src="{{ asset('/images/verified.svg') }}" alt="">Verified
                        @endif
                    </p>
                </div>
                <p class="description text-center">
                    {{ $user->primaryAddress() ? $user->primaryAddress()->full_address : '' }}
                </p>
            </div>
            <hr>
            <div class="button-container">
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                    <i class="fab fa-facebook-f"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                    <i class="fab fa-twitter"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                    <i class="fab fa-google-plus-g"></i>
                </button>
            </div>
        </div>
        @forelse ($user->bankAccounts as $bankAccount)
        <div class="card card-atm {{ strtolower($bankAccount->bank->abbr) }}">
            @php
                $numbers = str_split($bankAccount->account_no, 4);
            @endphp
            <div class="card-body">
                <div class="card-number">
                    @foreach($numbers as $number)
                    <span>{{ $number }}</span>
                    @endforeach
                </div>
                <h6 class="stats-title text-uppercase">{{ $bankAccount->account_name }}</h6>
            </div>
        </div>
        @empty
        @endforelse
    </div>
    <div class="col">
        <div class="row">
            <div class="col-4">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="statistics statistics-horizontal">
                            <div class="info info-horizontal">
                                <div class="row">
                                    <div class="col-3 d-flex">
                                        <div class="icon icon-primary my-auto">
                                            <i class="now-ui-icons business_money-coins"></i>
                                        </div>
                                    </div>
                                    <div class="col text-right">
                                        @if($user->getWallet(WalletType::PRIMARY) != null)
                                        <h4 class="info-title">{{ GlobalHelper::toCurrency($user->getWallet(WalletType::PRIMARY)->balance, 'Rp', null, 0) }}</h4>
                                        @endif
                                        <h6 class="stats-title">BALANCE</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="statistics statistics-horizontal">
                            <div class="info info-horizontal">
                                <div class="row">
                                    <div class="col-3 d-flex">
                                        <div class="icon icon-info my-auto">
                                            <i class="now-ui-icons objects_support-17"></i>
                                        </div>
                                    </div>
                                    <div class="col text-right">
                                        @if($user->getWallet(WalletType::PRIMARY) != null)
                                        <h4 class="info-title">{{ $user->getWallet(WalletType::POINT)->balance }}</h4>
                                        @endif
                                        <h6 class="stats-title">POINT</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="statistics statistics-horizontal">
                            <div class="info info-horizontal">
                                <div class="row">
                                    <div class="col-3 d-flex">
                                        <div class="icon icon-success my-auto">
                                            <i class="now-ui-icons gestures_tap-01"></i>
                                        </div>
                                    </div>
                                    <div class="col text-right">
                                        <h4 class="info-title">{{ $user->last_login ? $user->last_login : " - " }}</h4>
                                        <h6 class="stats-title">LAST LOGIN</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::model($user, ['route' => ['admin.users.user.update', $user], 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Information</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-2 col-form-label">First Name</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $user->first_name, array('class' => 'form-control', 'readonly', 'title' => $user->first_name.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Last Name</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $user->last_name, array('class' => 'form-control', 'readonly', 'title' => $user->last_name.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Email</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $user->email, array('class' => 'form-control', 'readonly', 'title' => $user->email.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Phone Number</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $user->phone_number, array('class' => 'form-control', 'readonly', 'title' => $user->phone_number.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Balance</label>
                        <div class="col-10">
                            <div class="form-group">
                            @if($user->getWallet(WalletType::PRIMARY) != null)
                                {{ Form::text('', GlobalHelper::toCurrency($user->getWallet(WalletType::PRIMARY)->balance, 'Rp', null, 0), array('class' => 'form-control', 'readonly', 'title' => GlobalHelper::toCurrency($user->getWallet(WalletType::PRIMARY)->balance, 'Rp', null, 0).' | Readonly')) }}
                            @else
                            No Wallet
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Point</label>
                        <div class="col-10">
                            <div class="form-group">
                            @if($user->getWallet(WalletType::PRIMARY) != null)
                                {{ Form::text('', $user->getWallet(WalletType::POINT)->balance, array('class' => 'form-control', 'readonly', 'title' => $user->getWallet(WalletType::POINT)->balance.' | Readonly')) }}
                            @else
                            No Wallet
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Packages</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::select('packages', $packageList, $user->packages, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    @if ($verified)
                    <div class="row">
                        <label class="col-2 col-form-label">Identification No.</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $user->getDocument(UserDocumentType::NRIC)->identification_number, array('class' => 'form-control', 'readonly', 'title' => $user->getDocument(UserDocumentType::NRIC)->identification_number.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label for="">NRIC Image</label>
                            <br>
                            <img class="img-document" src="{{ $user->getDocument(UserDocumentType::NRIC)->attachment_url }}">
                        </div>
                        <div class="col-6">
                            <label for="">Selfie Image</label>
                            <br>
                            <img class="img-document" src="{{ $user->getDocument(UserDocumentType::SELFIE)->attachment_url }}">
                        </div>
                    </div>
                    @endif
                </div>
                <div class="card-footer">
                    <div class="text-center">
                        <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('pageRelatedCss')
<link href="https://fonts.googleapis.com/css?family=Major+Mono+Display" rel="stylesheet">
@endpush