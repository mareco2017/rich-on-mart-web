@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    User List @parent
@endsection
@section('groupName', 'Users')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">User List</h4>
        {{-- <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.users.user.create') }}">Create user</a>
            </div>
        </div> --}}
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th class="d-none">First Name</th>
                    <th class="d-none">Last Name</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Balance</th>
                    <!-- <th>Package</th> -->
                    <!-- <th>Referral Code</th> -->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div class="card card-chart">
    <div class="card-body">
        <p>Total Saldo Users : Rp{{ number_format($floatingWallet, 2,',','.') }} </p>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterPackageEnum = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.users.user.ajax') !!}',
                data: function(d) { 
                    d.package_enum = filterPackageEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                
                { data: 'fullname', className: 'text-center', sortable: false, searchable: false },
                { data: 'first_name', name: 'first_name', visible: false },
                { data: 'last_name', name: 'last_name', visible: false },

                { data: 'phone_number', name: 'phone_number', className: 'text-center' },
                { data: 'email', name: 'email', className: 'text-center' },
                { data: 'balance', name: 'balance', className: 'text-center' },
                // { data: 'package', name: 'package', className: 'text-center' },
                // { data: 'referral_code', name: 'referral_code', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');

        $('#filterPackage').on('change', function(e) { 
            filterPackageEnum = e.currentTarget.value; 
            table.draw(); 
        });
	});
</script>
@endpush