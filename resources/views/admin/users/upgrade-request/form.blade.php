@extends('templates.admin.master')

@section('title', 'Upgrade Package Detail')
@section('groupName', 'Users')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Upgrade Package Detail</h3>
    </div>
    <div class="card-body">
        <form class="form-horizontal">
            <div class="row">
                <label for="id" class="col-2 col-form-label">Id</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('id', $upgradeRequest->id, ['class' => 'form-control', 'id' => 'id', 'readonly', 'placeholder' => 'Id']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="package" class="col-2 col-form-label">Package No</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('package', $upgradeRequest->package, ['class' => 'form-control', 'id' => 'package', 'readonly', 'placeholder' => 'Package']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="packageName" class="col-2 col-form-label">Package Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('package_name', PackageType::getString($upgradeRequest->package), ['class' => 'form-control', 'id' => 'packageName', 'readonly', 'placeholder' => 'Package Name']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="packageName" class="col-2 col-form-label">Price</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('price', 'Rp '.number_format($upgradeRequest->price,0,',','.'), ['class' => 'form-control', 'id' => 'price', 'readonly', 'placeholder' => 'Package Name']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="requester" class="col-2 col-form-label">Requester</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('price', $upgradeRequest->requestedBy->fullname, ['class' => 'form-control', 'id' => 'requester', 'readonly', 'placeholder' => 'Requester']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="requester" class="col-2 col-form-label">Date / Time</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('created_at', $upgradeRequest->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i'), ['class' => 'form-control', 'readonly', 'placeholder' => 'Created At']) }}
                    </div>
                </div>
            </div>
            <hr>
            <h4 class="card-title mt-2 mb-4">Destination Bank Account</h4>
            <div class="row">
                <label class="col-2 col-form-label">Bank</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $bankAccount ? $bankAccount->bank->name : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Account Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $bankAccount ? $bankAccount->account_name : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Account No</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $bankAccount ? $bankAccount->account_no : '', ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($upgradeRequest->reviewed_by_id === null && $upgradeRequest->isActive())
        <div class="text-center">
            <div class="d-inline-block mx-1">
                {!! Form::open(['route' => ['admin.users.upgrade-request.decline', $upgradeRequest], 'method' => 'post']) !!}
                    <button type="submit" class="btn btn-round btn-danger"><i class="fas fa-times-circle"></i> Decline</button>
                {!! Form::close() !!}
            </div>
            <div class="d-inline-block mx-1">
                {!! Form::open(['route' => ['admin.users.upgrade-request.approve', $upgradeRequest], 'method' => 'post']) !!}
                    <button type="submit" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Approve</button>
                {!! Form::close() !!}
            </div>
        </div>
        @else
        <div class="text-center">
            <div class="d-inline-block mx-1">
                @if ($upgradeRequest->status === UpgradeRequestStatus::ACCEPTED)
                <button type="button" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Approved</button>
                @elseif ($upgradeRequest->status === UpgradeRequestStatus::DECLINED)
                <button type="button" class="btn btn-round btn-danger"><i class="fas fa-times-circle"></i> Declined</button>
                @else
                @endif
            </div>
        </div>
        @endif
    </div>
</div>
@endsection