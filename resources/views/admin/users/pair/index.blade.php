@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Pair')
@section('groupName', 'Users')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Pair</h3>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Dates</label>
                    {{ Form::text('dates', '', ['class' => 'form-control', 'id' => 'filterDates', 'placeholder' => 'Select Dates']) }}
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Package</label>
                    {{ Form::select('package', $packageList, '', ['class' => 'form-control nullable', 'id' => 'filterPackage', 'placeholder' => 'Show All Package']) }}
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Claimed Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Show All Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-inside-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th rowspan="2">Date</th>
                    <th colspan="5" class="text-center">Leader</th>
                    <th colspan="4" class="text-center">Left Binary</th>
                    <th colspan="4" class="text-center">Right Binary</th>
                    <th rowspan="2">Total</th>
                    <th rowspan="2">Claimed</th>
                    <th rowspan="2">Action</th>
                </tr>
                <tr>
                    <th>Package</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="d-none">Leader First Name</th>
                    <th class="d-none">Leader Last Name</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="d-none">Left Binary First Name</th>
                    <th class="d-none">Left Binary Last Name</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="d-none">Right Binary First Name</th>
                    <th class="d-none">Right Binary Last Name</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="14" class="text-right">Total:</th>
                    <th colspan="3" class="text-left"></th>
                </tr>
            </tfoot>
            <!-- <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="non_searchable d-none"></td>
                    <td class="non_searchable d-none"></td>
                    <td class="non_searchable"></td>
                    <td></td>
                    <td class="non_searchable d-none"></td>
                    <td class="non_searchable d-none"></td>
                    <td></td>
                    <td class="non_searchable d-none"></td>
                    <td class="non_searchable d-none"></td>
                    <td class="non_searchable"></td>
                    <td class="non_searchable"></td>
                </tr>
            </tfoot> -->
            <tbody></tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-12 col-xl-5">
        <div class="card">
        {!! Form::open(['route' => 'admin.users.pair.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="card-header">
                <h4 class="card-title">New Pair</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <label for="leaderId" class="col-5 col-form-label">Leader User Id</label>
                    <div class="col-7">
                        <div class="form-group">
                            {{ Form::text('leader_user_id', '', ['class' => 'form-control', 'id' => 'leaderId', 'placeholder' => 'Leader User Id']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="leftBinaryId" class="col-5 col-form-label">Left Binary User Id</label>
                    <div class="col-7">
                        <div class="form-group">
                            {{ Form::text('left_binary_user_id', '', ['class' => 'form-control', 'id' => 'leftBinaryId', 'placeholder' => 'Left Binary User Id']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="rightBinaryId" class="col-5 col-form-label">Right Binary User Id</label>
                    <div class="col-7">
                        <div class="form-group">
                            {{ Form::text('right_binary_user_id', '', ['class' => 'form-control', 'id' => 'rightBinaryId', 'placeholder' => 'Right Binary User Id']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-center">
                    <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/daterangepicker.css') }}">
@endpush

@push('pageRelatedJs')
<script type="text/javascript" src="{{ asset('js/daterangepicker.js') }}"></script>
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;
        var filterPackageEnum = null;
        var filterDatesEnum = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.users.pair.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                    d.package_enum = filterPackageEnum;
                    d.dates = filterDatesEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'created_at', name: 'pairs.created_at', className: 'text-center' },

                { data: 'package', name: 'package', className: 'text-center', sortable: false, searchable: false },
                { data: 'leader_user.id', name: 'leaderUser.id', className: 'text-center', sortable: false },
                { data: 'leader_user.fullname', className: 'text-center', sortable: false, searchable: false },
                { data: 'leader_user.first_name', name: 'leaderUser.first_name', visible: false },
                { data: 'leader_user.last_name', name: 'leaderUser.last_name', visible: false },

                { data: 'left_binary_user.id', name: 'leftBinaryUser.id', className: 'text-center', sortable: false },
                { data: 'left_binary_user.fullname', className: 'text-center', sortable: false, searchable: false },
                { data: 'left_binary_user.first_name', name: 'leftBinaryUser.first_name', visible: false },
                { data: 'left_binary_user.last_name', name: 'leftBinaryUser.last_name', visible: false },

                { data: 'right_binary_user.id', name: 'rightBinaryUser.id', className: 'text-center', sortable: false },
                { data: 'right_binary_user.fullname', className: 'text-center', sortable: false, searchable: false },
                { data: 'right_binary_user.first_name', name: 'rightBinaryUser.first_name', visible: false },
                { data: 'right_binary_user.last_name', name: 'rightBinaryUser.last_name', visible: false },

                { data: 'total', className: 'text-center', sortable: false, searchable: false },
                { data: 'claimed', className: 'text-center', sortable: false, searchable: false },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[.\s]+/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var rp = function convertToRupiah(angka) {
                    var rupiah = '';		
                    var angkarev = angka.toString().split('').reverse().join('');
                    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                    return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');
                }
    
                pageTotal = api
                    .column( 14, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                $(api.column( 0 ).footer()).removeClass('text-center');
                $(api.column( 14 ).footer()).removeClass('text-center');
                $( api.column( 14 ).footer() ).html(
                    rp(pageTotal)
                );
            }
            // initComplete: function () {
            //     this.api().columns().every(function () {
            //         var column = this;
            //         var columnClass = column.footer().className;
            //         var input = $("<input class='form-control' placeholder='Aa...'>");
            //         if (columnClass.indexOf('non_searchable') === -1){
            //             input.appendTo($(column.footer()).empty()).on('input', function () {
            //                 column.search($(this).val(), false, false, true).draw();
            //             });
            //         }
            //     });
            // }
		});
		$(table.table().container()).removeClass('form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 

        $('#filterPackage').on('change', function(e) { 
            filterPackageEnum = e.currentTarget.value; 
            table.draw(); 
        });

        $('#filterDates').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                format: "DD-MMM-YYYY"
            }
        });

        $('#filterDates').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MMM-YYYY') + ' - ' + picker.endDate.format('DD-MMM-YYYY'));
            filterDatesEnum = [picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD')]; 
            table.draw(); 
        });
	});
</script>
@endpush