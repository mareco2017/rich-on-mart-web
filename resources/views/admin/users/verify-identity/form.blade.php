@extends('templates.admin.master')

@section('title')
    User Verify Identity @parent
@endsection
@section('groupName', 'Users')

@section('content')
{{-- <div class="card">
    <div class="card-header">
        <h4 class="card-title">Binary Tree</h4>
    </div>
    <div class="card-body">
        @if (count($binaries) > 0)
        <div class="tree-container" id="tree-container"></div>
        @else
        <div class="mb-5">
            <p class="fs-7 f-light text-grey mb-0 text-center">No binary found</p>
        </div>
        @endif
    </div>
</div> --}}
{!! Form::model($user, ['url' => '/', 'method' => 'post', 'class' => 'form-horizontal']) !!}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Verify Identity Detail</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="id" class="col-2 col-form-label">Id</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('id', $user->id, ['class' => 'form-control', 'id' => 'id', 'disabled', 'placeholder' => 'Id']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="firstName" class="col-2 col-form-label">First Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'id' => 'firstName', 'placeholder' => 'First Name']) }}
                        @if($errors->has('first_name'))
                        <p class="status-text">{{ $errors->first('first_name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="lastName" class="col-2 col-form-label">Last Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'id' => 'lastName', 'placeholder' => 'Last Name']) }}
                        @if($errors->has('last_name'))
                        <p class="status-text">{{ $errors->first('last_name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="email" class="col-2 col-form-label">Email</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('email', $user->email, ['class' => 'form-control', 'id' => 'email', 'disabled', 'placeholder' => 'Email']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="phoneNumber" class="col-2 col-form-label">Phone Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('phone_number', $user->phone_number, ['class' => 'form-control', 'id' => 'phoneNumber', 'disabled', 'placeholder' => 'Phone Number']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="identificationNumber" class="col-2 col-form-label">Identification Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('identification_number', old('identification_number'), ['class' => 'form-control', 'id' => 'identificationNumber', 'placeholder' => 'Identification Number', 'required']) }}
                        @if($errors->has('identification_number'))
                        <p class="status-text">{{ $errors->first('identification_number') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">NRIC Image</label>
                <div class="col-10">
                    <div class="form-group">
                        <img class="" src="{{ $user->documents()->type(UserDocumentType::NRIC)->first()->attachment_url }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Selfie Image</label>
                <div class="col-10">
                    <div class="form-group">
                        <img class="" src="{{ $user->documents()->type(UserDocumentType::SELFIE)->first()->attachment_url }}" alt="">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <label for="desc" class="col-2 col-form-label">Description<br><small>(Optional)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'desc', 'placeholder' => 'Any description before Approving / Declining']) }}
                        @if($errors->has('description'))
                        <p class="status-text">{{ $errors->first('description') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            @if ($user->documents_status['nric'] == UserDocumentStatus::PENDING)
            <div class="text-center">
                <div class="d-inline-block mx-1">
                    <button type="button" data-action="{{ route('admin.users.verify-identity.decline', $user) }}" class="btn btn-round btn-danger"><i class="fas fa-times-circle"></i> Decline</button>
                </div>
                <div class="d-inline-block mx-1">
                    <button type="button" data-action="{{ route('admin.users.verify-identity.approve', $user) }}" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Approve</button>
                </div>
            </div>
            @else
            <div class="text-center">
                <div class="d-inline-block mx-1">
                    @if ($user->documents_status['nric'] == UserDocumentStatus::VERIFIED)
                    <button type="button" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Approved</button>
                    @elseif ($user->documents_status['nric'] == UserDocumentStatus::DECLINED)
                    <button type="button" class="btn btn-round btn-danger"><i class="fas fa-times-circle"></i> Declined</button>
                    @else
                    @endif
                </div>
            </div>
            @endif
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedCss')
<!-- <link rel="stylesheet" href="{{ asset('css/Treant.css') }}" type="text/css"/> -->
@endpush

@push('pageRelatedJs')
{{-- <script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script type="text/javascript">
    var binaries = {!! json_encode($binaries) !!};
	
	var config = {
		container: "#tree-container",
		connectors: {
			type: 'bCurve',
			style: {
				stroke: '#000',
				'stroke-width': 2,
				'stroke-opacity': '.7',
			}
		},
		siblingSeparation: 15,
		rootOrientation: 'NORTH',
		node: {
			collapsable: true,
		}
	};

    var nodes = [];

    	binaries.forEach(function(binary, i) {
		if (binary.level == 0) {
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				id: binary.id,
				text: {
					name: binary.fullname,
					desc: 'ID: ' + binary.user_id
				},
                HTMLclass: packageClass,
			});
		} else {
			var parent = nodes.find(function(element) {
				return element.id == binary.leader_binary_id;
			});
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				parent: parent,
				id: binary.id,
				text: {
					name: binary.fullname,
					desc: 'ID: ' + binary.user_id
				},
                HTMLclass: packageClass,
			});
		}
	});

	var temp = [
		config
	];

	var simple_chart_config = temp.concat(nodes);
	var my_chart = new Treant(simple_chart_config);
</script> --}}
<script>
$(document).ready(function(){
    $('button[data-action]').on('click', function() {
        let form = $('form');
        form.attr('action', $(this).data('action'));

        form.submit();
    });
    
    // $('#tree-container').perfectScrollbar();
});
</script>
@endpush