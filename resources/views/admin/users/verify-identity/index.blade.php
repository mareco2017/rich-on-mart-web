@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    User Verify Identity @parent
@endsection
@section('groupName', 'Users')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">User Verify Identity</h4>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th class="d-none">First Name</th>
                    <th class="d-none">Last Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.users.verify-identity.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                
                { data: 'fullname', className: 'text-center', sortable: false, searchable: false },
                { data: 'first_name', name: 'first_name', visible: false },
                { data: 'last_name', name: 'last_name', visible: false },
                
                { data: 'email', name: 'email', className: 'text-center' },
                { data: 'phone_number', name: 'phone_number', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass('form-inline');
	});
</script>
@endpush