@extends('templates.admin.master')

@section('title')
    {{ $category->id ? 'Edit' : 'Create' }} Category @parent
@endsection

@section('groupName', 'Categories')

@section('content')
@if (!$category->id)
{!! Form::open(['route' => 'admin.categories.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($category, ['route' => ['admin.categories.update', $category], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $category->id ? 'Edit' : 'Create' }} Categories</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="title" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title']) }}
                        @if($errors->has('title'))
                        <p class="status-text">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="commission" class="col-2 col-form-label">Commission<br><small>(In Amount)</small></label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('commission', old('commission'), ['class' => 'form-control', 'id' => 'commission', 'placeholder' => 'Commission']) }}
                        @if($errors->has('commission'))
                        <p class="status-text">{{ $errors->first('commission') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="discount" class="col-2 col-form-label">Discount</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('discount', old('discount'), ['class' => 'form-control', 'id' => 'discount', 'placeholder' => 'Discount']) }}
                        @if($errors->has('discount'))
                        <p class="status-text">{{ $errors->first('discount') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Cover</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ $category->id && $category->cover_url ? $category->cover_url : asset('/images/img-placeholder.png') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                            <div>
                                <span class="btn btn-primary btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                                    {{ Form::file('cover') }}
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                            </div>
                        </div>
                        @if($errors->has('cover'))
                        <p class="status-text">{{ $errors->first('cover') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
@endpush