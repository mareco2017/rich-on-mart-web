@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Category @parent
@endsection

@section('groupName', 'Categories')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Category</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.categories.create') }}">Create category</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control', 'id' => 'filterStatus', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Commission</th>
                    <th>Discount</th>
                    <th>Cover</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.categories.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'title', name: 'title', className: 'text-center' },
                { data: 'commission', name: 'commission', className: 'text-center', render: function (data) {
                    return 'Rp'+data;
                }},
                { data: 'discount', name: 'discount', className: 'text-center', render: function (data) {
                    return 'Rp'+data;
                }},
                { data: 'cover', name: 'cover', className: 'text-center', orderable: false, searchable: false, render: function (data, type, full, meta) {
                    if (!data) return '';
                    var str = '<a href="'+ data +'" target="_blank"><img id="previewImage" class="sm" src="'+ data +'"/></a>';
                    return str;
                }},
                { data: 'status', name: 'status', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false  }
			]
		});
		$(table.table().container()).removeClass( 'form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush