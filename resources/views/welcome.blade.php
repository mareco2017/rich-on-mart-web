@extends('structure')

@section('body')
<body class="dashboard">
    <section class="welcome">
        <div class="floating-objects container">
            <img class="fish" src="{{ asset('/images/products/fish.png') }}" alt="">
            <img class="salad" src="{{ asset('/images/products/salad.png') }}" alt="">
            <img class="tomato" src="{{ asset('/images/products/tomato.png') }}" alt="">
            <img class="chicken" src="{{ asset('/images/products/chicken.png') }}" alt="">
            <img class="cabbage" src="{{ asset('/images/products/cabbage.png') }}" alt="">
            <img class="meat" src="{{ asset('/images/products/meat.png') }}" alt="">
        </div>

        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('/icons/logo.png') }}" width="184" alt="">
                </a>
                <div class="ml-auto">
                    <div class="language-dropdown">
                        <div class="language-list">
                            <span class="flag-name">Indonesia</span>
                            <i class="fas fa-caret-down caret-down"></i>
                            <div class="flag">
                                <img src="{{ asset('/icons/flags/indonesia.svg') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container position-relative">
            <div class="text-center text-wrapper">
                <h4 class="unique-text font-weight-bold">Belanja puas, harga pantas,<br>tak perlu panas-panas!</h4>
                <p class="text-grey install-text">Belanja Online kebutuhan sehari-hari hanya di RichOnMart Indonesia.<br>Download & Install Aplikasinya Sekarang Juga!</p>
                <ul class="nav rom-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="sayuran-dan-buah-tab" data-toggle="tab" href="#sayuran-dan-buah" role="tab" aria-controls="sayuran-dan-buah" aria-selected="true">Sayuran dan Buah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="produk-daging-tab" data-toggle="tab" href="#produk-daging" role="tab" aria-controls="produk-daging" aria-selected="false">Produk Daging</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="olahan-susu-dan-telur-tab" data-toggle="tab" href="#olahan-susu-dan-telur" role="tab" aria-controls="olahan-susu-dan-telur" aria-selected="false">Olahan Susu dan Telur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="keperluan-dapur-tab" data-toggle="tab" href="#keperluan-dapur" role="tab" aria-controls="keperluan-dapur" aria-selected="false">Keperluan Dapur</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="sayuran-dan-buah" role="tabpanel" aria-labelledby="sayuran-dan-buah-tab">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/apple.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Fuji Apel Premium</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/broccoli.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Broccoli Organic Petani Lokal</p>
                                    </div>
                                    <p class="product-price">Rp 25.000</p>
                                    <p class="product-price-lined">Rp 27.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/daun-bawang.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Daun Bawang Organic</p>
                                    </div>
                                    <p class="product-price">Rp 7.000</p>
                                    <p class="product-price-lined">Rp 12.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/banana.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Sunpride Cavendish Banana</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/tomato.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Organic Fresh Tomato</p>
                                    </div>
                                    <p class="product-price">Rp 12.000</p>
                                    <p class="product-price-lined">Rp 15.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="produk-daging" role="tabpanel" aria-labelledby="produk-daging-tab">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/apple.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Fuji Apel Premium</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/broccoli.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Broccoli Organic Petani Lokal</p>
                                    </div>
                                    <p class="product-price">Rp 25.000</p>
                                    <p class="product-price-lined">Rp 27.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/daun-bawang.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Daun Bawang Organic</p>
                                    </div>
                                    <p class="product-price">Rp 7.000</p>
                                    <p class="product-price-lined">Rp 12.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/banana.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Sunpride Cavendish Banana</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/tomato.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Organic Fresh Tomato</p>
                                    </div>
                                    <p class="product-price">Rp 12.000</p>
                                    <p class="product-price-lined">Rp 15.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="olahan-susu-dan-telur" role="tabpanel" aria-labelledby="olahan-susu-dan-telur-tab">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/apple.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Fuji Apel Premium</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/broccoli.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Broccoli Organic Petani Lokal</p>
                                    </div>
                                    <p class="product-price">Rp 25.000</p>
                                    <p class="product-price-lined">Rp 27.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/daun-bawang.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Daun Bawang Organic</p>
                                    </div>
                                    <p class="product-price">Rp 7.000</p>
                                    <p class="product-price-lined">Rp 12.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/banana.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Sunpride Cavendish Banana</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/tomato.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Organic Fresh Tomato</p>
                                    </div>
                                    <p class="product-price">Rp 12.000</p>
                                    <p class="product-price-lined">Rp 15.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="keperluan-dapur" role="tabpanel" aria-labelledby="keperluan-dapur-tab">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/apple.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Fuji Apel Premium</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/broccoli.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Broccoli Organic Petani Lokal</p>
                                    </div>
                                    <p class="product-price">Rp 25.000</p>
                                    <p class="product-price-lined">Rp 27.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/daun-bawang.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Daun Bawang Organic</p>
                                    </div>
                                    <p class="product-price">Rp 7.000</p>
                                    <p class="product-price-lined">Rp 12.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/banana.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Sunpride Cavendish Banana</p>
                                    </div>
                                    <p class="product-price">Rp 5.000</p>
                                    <p class="product-price-lined">Rp 7.000</p>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                    <img src="{{ asset('/images/products/tomato.jpg') }}" alt="">
                                </div>
                                <div class="product-desc">
                                    <div class="product-name">
                                        <p>Organic Fresh Tomato</p>
                                    </div>
                                    <p class="product-price">Rp 12.000</p>
                                    <p class="product-price-lined">Rp 15.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-app">
        <div class="container">
            <div class="why-use-rom">
                <div class="orange-card"></div>
                <div class="row no-gutters">
                    <div class="col features-info-wrapper">
                        <h4 class="text-white font-weight-bold">Mengapa menggunakan ROM?</h4>
                        <p class="info-text">Semua keperluan yang anda belanja meggunakan ROM akan dipilih secara profesional oleh Shopper & dijamin kesegarannya!</p>
                    </div>
                    <div class="col features-wrapper">
                        <div class="features">
                            <div class="feature col">
                                <div class="content">
                                    <img class="feature-icon" src="{{ asset('/icons/shop.svg') }}" alt="">
                                    <p class="feature-title">Personal Shopper</p>
                                    <p class="feature-desc">Setiap pembelian Anda ditangani oleh Shopper khusus untuk melayanin Anda</p>
                                </div>
                            </div>
                            <div class="feature col">
                                <div class="content">
                                    <img class="feature-icon" src="{{ asset('/icons/time.svg') }}" alt="">
                                    <p class="feature-title">Waktu Fleksibel</p>
                                    <p class="feature-desc">Waktu pengantaran fleksibel, diantar kapanpun Anda sesuai permintaan Anda</p>
                                </div>
                            </div>
                            <div class="feature col">
                                <div class="content">
                                    <img class="feature-icon" src="{{ asset('/icons/quality.svg') }}" alt="">
                                    <p class="feature-title">Kualitas Terjamin</p>
                                    <p class="feature-desc">Semua barang diseleksi ketat oleh Shopper kami untuk menjamin kualitasnya</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="download-now">
                <h4 class="font-weight-bold">Download Aplikasi ROM Sekarang Juga</h4>
                <div class="">
                    <img class="download-at-store" src="{{ asset('/images/google-play.png') }}" alt="">
                    <img class="download-at-store" src="{{ asset('/images/app-store.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="logo-wrapper">
                <img src="{{ asset('/icons/logo.png') }}" width="184" alt="">
            </div>
            <div class="info-wrapper">
                <div class="row">
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Customer Service</p>
                        <p class="item"><i class="fas fa-phone mr-3"></i>+6282288827775</p>
                        <p class="item"><i class="fas fa-envelope mr-3"></i>official.richonmart@gmail.com</p>
                    </div>
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Dukungan</p>
                        <a href="#">
                            <p class="item">Kebijakan Privasi</p>
                        </a>
                        <a href="#">
                            <p class="item">Syarat &amp; Ketentuan</p>
                        </a>
                    </div>
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Social Media</p>
                        <a href="#" class="item-icon">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#" class="item-icon">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#" class="item-icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <p class="copyright">© {{ date("Y") }} PT.ROP Group Indonesia | RICH ON PAY - All Rights Reserved</p>
        </div>
    </footer>
</body>
@endsection

@push('pageRelatedJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script src="{{ asset('/js/animation.gsap.min.js') }}"></script>
<script>
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onLeave", duration: "100%"}});

	new ScrollMagic.Scene({triggerElement: ".floating-objects"})
					.setTween(".floating-objects > img", {y: "-20%"})
					.addTo(controller);
</script>
@endpush