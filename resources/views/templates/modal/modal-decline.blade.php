<div class="modal fade" id="modalDecline" tabindex="-1" role="dialog" aria-labelledby="myModalDeclineLabel" aria-hidden="true">
    <form action="/">
        <div class="modal-dialog">
            <div class="modal-content modal-decline">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title" id="myModalDeclineLabel">Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p class="mb-1" id="modalDeclineText"></p>
                    <label for="remarks" class="text-grey mb-0 fs-0">Your Remarks / Comments</label>
                    <textarea id="remarks" class="form-control pt-0" name="msg"></textarea>
                    @if (isset($choices) && $choices !== null)
                        <div class="mt-3">
                            <p class="text-grey mb-1 fs-0">Reason List - Click to select</p>
                            <div class="border bg-light p-3 modal-decline-reason">
                                @foreach ($choices as $key => $choice)
                                <div class="choices bg-white mb-2 py-2 px-3">
                                    <p class="mb-0"><b>{{ $key }}</b></p>
                                    <p class="mb-0 content">{{ $choice }}</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <div class="mx-auto">
                        <button type="button" class="btn btn-round btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                        <button type="submit" class="btn btn-round btn-info"><i class="fas fa-check-circle"></i> Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

@push('pageRelatedJs')
<script>
$(document).ready(function(){
    var loadingHTML = '<span class="fas fa-circle-notch fa-spin"></span>';

    $('#modalDecline').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        // var body = $(e.relatedTarget).data('body');
        var json = $(e.relatedTarget).data('json');
        // var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title') ? $(e.relatedTarget).data('title') : 'Confirmation';
        var message = $(e.relatedTarget).data('message') ? $(e.relatedTarget).data('message') : 'Are you sure to decline this data?';
        var action = $(e.relatedTarget).data('url');
        var redirect = $(e.relatedTarget).data('redirect') != undefined ? $(e.relatedTarget).data('redirect') : null;
        var method = $(e.relatedTarget).data('method') == undefined ? 'POST' : $(e.relatedTarget).data('method');
        var notifMessage = $(e.relatedTarget).data('notif-message') == undefined ? '' : $(e.relatedTarget).data('notif-message');
        $('#modalDecline .modal-header #myModalDeclineLabel').text(title);
        $('#modalDecline .modal-body #modalDeclineText').html(message);
        $('#modalDecline form').attr('action', action).on("submit", function(e){
            let submitBtn = $('button[type=submit]');
            let oriHTML = submitBtn.html();
            let data = json ? $.param(json) : $(this).serialize();
            submitBtn.html(loadingHTML);
            submitBtn.prop('disabled', true);
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: action,
                type: method,
                data: data,
                success: function(res) {
                    if (typeof redirect === "string") {
                        window.location = redirect;
                    }
                    $.notify({
                        icon: "now-ui-icons ui-1_bell-53",
                        message: notifMessage ? notifMessage : res.message
                    });
                },
                error: function (err) {
                    var errorMessage = "Process failed.";
                    if (err.responseJSON && err.responseJSON.message) {
                        errorMessage = err.responseJSON.message;
                    } else if (err.responseText) {
                        errorMessage = err.responseText;
                    }
                    $.notify({
                        icon: "now-ui-icons objects_spaceship",
                        message: errorMessage ? errorMessage : 'Something went wrong'
                    }, {
                        type: 'danger'
                    });
                },
                complete: function() {
                    $("#modalDecline").modal('hide')
                    submitBtn.html(oriHTML);
                    submitBtn.prop('disabled', false);
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modalDecline .modal-header #myModalDeclineLabel').text('');
        $('#modalDecline .modal-body #modalDeclineText').text('');
        $('#modalDecline form').attr('action','/').off('submit');
    });

    
    var choices = $('.choices');
    var textarea = $('textarea[name="msg"]');
    choices.on('click', function() {
        textarea.val($(this).find('.content').html());
    });

    $('.modal-decline-reason').perfectScrollbar();
});
</script>
@endpush