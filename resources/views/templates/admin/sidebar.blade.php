<?php
$currentRoute = Route::getCurrentRoute();
$routeCollection = Route::getRoutes();
$routes = [
    (object) [
        'title' => 'Dashboard',
        'icon' => 'now-ui-icons design_app',
        'href' => route('admin.dashboard'),
        'collection' => $routeCollection->getRoutesByName()['admin.dashboard']
    ],
    (object) [
        'title' => 'Order',
        'icon' => 'now-ui-icons business_money-coins',
        'href' => route('admin.order.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.order.index']
    ],
    (object) [
        'title' => 'Admins',
        'icon' => 'now-ui-icons clothes_tie-bow',
        'href' => route('admin.admins.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.admins.index']
    ],
    (object) [
        'title' => 'Users',
        'icon' => 'now-ui-icons users_circle-08',
        'child' => [
            (object) [
                'title' => 'User List',
                'icon' => 'UL',
                'href' => route('admin.users.user.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.user.index']
            ],
            // (object) [
            //     'title' => 'Upgrade Package',
            //     'icon' => 'UP',
            //     'href' => route('admin.users.upgrade-request.index'),
            //     'collection' => $routeCollection->getRoutesByName()['admin.users.upgrade-request.index']
            // ],
            (object) [
                'title' => 'Verify Identity',
                'icon' => 'VI',
                'href' => route('admin.users.verify-identity.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.verify-identity.index']
            ],
            // (object) [
            //     'title' => 'Pair',
            //     'icon' => 'P',
            //     'href' => route('admin.users.pair.index'),
            //     'collection' => $routeCollection->getRoutesByName()['admin.users.pair.index']
            // ],
        ]
    ],
    (object) [
        'title' => 'Bank',
        'icon' => 'now-ui-icons business_bank',
        'href' => route('admin.bank.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.bank.index']
    ],
    (object) [
        'title' => 'Bank Account',
        'icon' => 'now-ui-icons business_badge',
        'href' => route('admin.bank-account.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.bank-account.index']
    ],
    (object) [
        'title' => 'Banner',
        'icon' => 'now-ui-icons design_image',
        'href' => route('admin.promos.banner.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.promos.banner.index']
    ],
    // (object) [
    //     'title' => 'Promos',
    //     'icon' => 'now-ui-icons media-1_album',
    //     'child' => [
    //         (object) [
    //             'title' => 'Banner',
    //             'icon' => 'B',
    //             'href' => route('admin.promos.banner.index'),
    //             'collection' => $routeCollection->getRoutesByName()['admin.promos.banner.index']
    //         ],
    //     ]
    // ],
    (object) [
        'title' => 'Categories',
        'icon' => 'now-ui-icons business_chart-pie-36',
        'href' => route('admin.categories.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.categories.index']
    ],
    (object) [
        'title' => 'Products',
        'icon' => 'now-ui-icons shopping_bag-16',
        'href' => route('admin.products.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.products.index']
    ],
    (object) [
        'title' => 'Payments',
        'icon' => 'now-ui-icons shopping_bag-16',
        'href' => route('admin.payments.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.payments.index']
    ],
    // (object) [
    //     'title' => 'Top Up',
    //     'icon' => 'now-ui-icons arrows-1_cloud-upload-94',
    //     'href' => route('admin.top-up.index'),
    //     'collection' => $routeCollection->getRoutesByName()['admin.top-up.index']
    // ],
    (object) [
        'title' => 'System Config',
        'icon' => 'now-ui-icons ui-2_settings-90',
        'href' => route('admin.config.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.config.index']
    ],
]
?>
<div class="sidebar-wrapper">
    {{-- <div class="user">
        <div class="photo">
            <img src="{{ asset('/images/img-placeholder.png') }}" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <span>
                    {{ Auth::user()->fullname }}
                    <b class="caret"></b>
                </span>
            </a>
            <div class="clearfix"></div>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li>
                        {!! Form::open(['route' => 'admin.logout', 'method' => 'POST', 'name' => 'logoutForm']) !!}
                        {!! Form::close() !!}
                        <a href="" onclick="document.forms['logoutForm'].submit(); return false;">
                            <span class="sidebar-mini-icon">LO</span>
                            <span class="sidebar-normal">Log Out</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}
    <ul class="nav">
        @foreach($routes as $key => $route)
            <?php
                if (isset($route->collection) && $route->collection) {
                    $active = $route->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                } else if (isset($route->child) && $route->child) {
                    $hasAccess = false;
                    $active = false;
                    $open = false;
                    foreach ($route->child as $childRoute) {
                        if (isset($childRoute->collection) && $childRoute->collection) {
                            if ($childRoute->collection->getPrefix() == $currentRoute->getPrefix()) {
                                $active = 'active';
                                $open = true;
                            }
                        }
                    }
                }
            ?>
            @if(isset($route->child))
                <li class="{{ $active }}">
                    <a data-toggle="collapse" href="#{{ 'dropdown'.$key }}" aria-expanded="{{ $open ? 'true' : 'false' }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse {{ !$open ?: 'show' }}" id="{{ 'dropdown'.$key }}">
                        <ul class="nav mt-0">
                            @foreach($route->child as $child)
                            <?php
                                if (isset($child->collection) && $child->collection) {
                                    $childActive = $child->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                                }
                            ?>
                            <li class="{{ $childActive }}">
                                <a href="{{ $child->href }}">
                                    <span class="sidebar-mini-icon">{{ $child->icon }}</span>
                                    <span class="sidebar-normal">{{ $child->title }}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @else
                <li class="{{ $active }}">
                    <a href="{{ $route->href }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}</p>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</div>