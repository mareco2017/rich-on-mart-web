@extends('structure')

@section('title', '|')

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ mix('/css/now-ui-dashboard.css') }}">
@endpush

@section('body')
<body class="sidebar-mini">
    <div class="wrapper">
        <div class="sidebar" data-color="orange">
        <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
        -->

            <div class="logo">
                <a href="#" class="simple-text logo-mini">
                    ROM
                </a>

                <a href="#" class="simple-text logo-normal">
                    Rich On Mart
                </a>
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-simple btn-icon btn-neutral btn-round">
                        <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                        <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
                    </button>
                </div>
            </div>

            @include('templates.admin.sidebar')
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand">@yield('groupName')</a>
                    </div>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <form>
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="now-ui-icons ui-1_zoom-bold"></i>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="now-ui-icons media-2_sound-wave"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Stats</span>
                                    </p>
                                </a>
                            </li> -->

                            <!-- <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="now-ui-icons location_world"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">World</span>
                                    </p>
                                </a>
                            </li> -->
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="now-ui-icons users_single-02"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Account</span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <!-- <a class="dropdown-item" href="#">Action</a> -->
                                    <!-- <a class="dropdown-item" href="#">Another action</a> -->
                                    {!! Form::open(['route' => 'admin.logout', 'method' => 'POST', 'name' => 'logoutForm']) !!}
                                    {!! Form::close() !!}
                                    <a class="dropdown-item" href="" onclick="document.forms['logoutForm'].submit(); return false;">Log out</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="panel-header panel-header-sm @yield('panel-class')">
                @yield('panel')
            </div>

            <div class="content">
                @yield('content')
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <nav>
                    <ul>
                        <li>
                            <a href="https://mareco.id">
                                Powered by mareco
                            </a>
                        </li>
                    </ul>
                    </nav>
                    <div class="copyright">
                        &copy; <script>document.write(new Date().getFullYear())</script>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
@endsection

@push('pageRelatedJs')
<script type="text/javascript" src="{{ asset('/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/chartjs.min.js') }}"></script>
<script type="text/javascript" src="{{ mix('/js/now-ui-dashboard.js') }}"></script>
<script>
$.notifyDefaults({
    type: 'info',
    timer: 6000,
    placement: {
        from: "top",
        align: "right"
    },
    mouse_over: 'pause'
});
@if (Session::has('success'))
    $.notify({
        icon: "now-ui-icons ui-1_bell-53",
        message: "{!! Session::get('success') !!}"
    });
@endif
@if (Session::has('info'))
    $.notify({
        icon: "now-ui-icons ui-1_bell-53",
        message: "{!! Session::get('info') !!}"
    });
@endif
@if (Session::has('warning'))
    $.notify({
        icon: "now-ui-icons ui-2_time-alarm",
        message: "{!! Session::get('warning') !!}"
    }, {
        type: 'warning'
    });
@endif
@if (Session::has('error'))
    $.notify({
        icon: "now-ui-icons objects_spaceship",
        message: `{!! Session::get('error') !!}`
    }, {
        type: 'danger'
    });
@endif

var alreadyShow = false;
var titleUpdated = false;

function notConnectedNotif() {
    if (!alreadyShow) {
        $.notify({
            icon: "now-ui-icons media-2_sound-wave",
            title: "Socket is Not Connected",
            message: "Real-Time Order Notification won't active until Socket Reconnected",
        }, {
            type: "danger",
            placement: {
                from: "bottom",
                align: "right"
            },
            delay: 0
        });
    }
    alreadyShow = true;
}

function connectedNotif() {
    if (alreadyShow) {
        $.notify({
            icon: "now-ui-icons media-2_sound-wave",
            title: "Socket Connected",
            message: "Real-Time Order Notification is now active",
        }, {
            type: "info",
            placement: {
                from: "bottom",
                align: "right"
            },
            delay: 0
        });

        alreadyShow = false;
    }
}

$(document).ready(function() {
    if (io) {
        Echo.channel('admin.channel')
            .listen('.order.created', (e) => {
                console.log(window.location.hostname);
                let order = e.order || null;
                let url = order ? `/admin/order/${order.id}/detail` : '/admin/order';
                
                $.notify({
                    icon: "now-ui-icons ui-1_bell-53",
                    title: "New Order",
                    message: "An order just created, Click to view detail",
                    url: url,
                    target: '_self'
                }, {
                    delay: 0
                });
                
                if (!titleUpdated) {
                    document.title = '(You have new Order) ' + document.title;
                    titleUpdated = true;
                }
            });

        Echo.connector.socket.on('connect', function(socket) { // When client connects
            console.log(`connected`, socket);
            connectedNotif();
            if (typeof window.onSocketConnected === "function") { 
                window.onSocketConnected(socket);
            }
        });

        Echo.connector.socket.on('reconnecting', function(attemptNumber){
            console.log('reconnecting', attemptNumber);
            notConnectedNotif();
            if (typeof window.onSocketReconnecting === "function") {
                window.onSocketReconnecting(attemptNumber);
            }
        });
        
        Echo.connector.socket.on('disconnect', function(socket) { // When client disconnects
            console.log('disconnected', socket);
            if (typeof window.onSocketDisconnected === "function") { 
                window.onSocketDisconnected(socket);
            }
        });
    }
});
</script>
@endpush