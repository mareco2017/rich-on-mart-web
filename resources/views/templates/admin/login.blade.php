@extends('structure')

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ mix('/css/now-ui-kit.css') }}">
@endpush

@section('body')
<body class="login-page sidebar-collapse cf-inspector-body">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent" color-on-scroll="400">
        <div class="container">
            
            <!-- <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-header">Dropdown header</a>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">One more separated link</a>
                </div>
            </div> -->
            

            <div class="navbar-translate">
                <a class="navbar-brand">
                    Rich On Mart
                </a>
                <button class="navbar-toggler navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar top-bar"></span>
                    <span class="navbar-toggler-bar middle-bar"></span>
                    <span class="navbar-toggler-bar bottom-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse justify-content-end collapse" id="navigation" style="" data-nav-image="../assets/img/blurred-image-1.jpg">
                <ul class="navbar-nav">
                
                    <li class="nav-item">
                        <a class="nav-link" href="/">Back to Dashboard</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="https://github.com/creativetimofficial/now-ui-kit/issues">Have an issue?</a>
                    </li>

                

                
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
                            <i class="fab fa-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
                            <i class="fab fa-facebook-square"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
                            <i class="fab fa-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li> -->
                
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" style="background-image:url(/images/auth/login-bg.jpg)"></div>
        <div class="content">
            <div class="container">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="card card-login card-plain">
                        {!! Form::open(['route' => 'admin.login', 'method' => 'POST', 'class' => 'form']) !!}
                            <div class="card-header text-center">
                                <div class="logo-container">
                                    <img src="../assets/img/now-logo.png" alt="">
                                </div>
                                @if (Session::has('error'))
                                    <p class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </p>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="input-group no-border input-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        </div>
                                    <input type="text" name="username" class="form-control" placeholder="Email">
                                </div>

                                <div class="input-group no-border input-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" placeholder="Password" class="form-control">
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Login now</button>
                                <!-- <a href="#pablo" class="btn btn-primary btn-round btn-lg btn-block">Login now</a> -->

                                <div class="pull-left">
                                    <h6><a href="#" class="link">Create Account</a></h6>
                                </div>

                                <div class="pull-right">
                                    <h6><a href="#" class="link">Need Help?</a></h6>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                            About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                            Blog
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright" id="copyright">
                    © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
                </div>
            </div>
        </footer>
    </div>
</body>
@endsection

@push('pageRelatedJs')
<script type="text/javascript" src="{{ asset('/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ mix('/js/now-ui-kit.js') }}"></script>
<script>
    $.notifyDefaults({
        type: 'info',
        timer: 6000,
        placement: {
            from: "top",
            align: "right"
        },
        mouse_over: 'pause'
    });
    @if (Session::has('success'))
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: "{{ Session::get('success') }}"
        });
    @endif
    @if (Session::has('info'))
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: "{{ Session::get('info') }}"
        });
    @endif
    @if (Session::has('warning'))
        $.notify({
            icon: "now-ui-icons ui-2_time-alarm",
            message: "{{ Session::get('warning') }}"
        }, {
            type: 'warning'
        });
    @endif
    @if (Session::has('error'))
        $.notify({
            icon: "now-ui-icons objects_spaceship",
            message: `{{ Session::get('error') }}`
        }, {
            type: 'danger'
        });
    @endif
</script>
@endpush