<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		body {
      color: #464646 !important;
			margin: 0 !important;
      width: 100% !important;
      min-width: 100% !important;
    }
    
    a {
      color: #464646;
      text-decoration: unset;
    }

		.body {
			font-family: 'Helvetica','Arial',sans-serif;
			line-height: 1.5;
			font-size: 14px;
			background: #f1f1f1;
      color: #464646 !important;
      width: 100% !important;
      min-width: 100% !important;
      padding: 48px;
    }

    p {
      margin-top: 0;
      margin-bottom: 16px;
    }

		.container {
			width: 100% !important;
      max-width: 600px;
      background: #f1f1f1;
      padding: 0 12px;
    }

    .contain-header,
    .contain-body,
    .contain-footer {
      padding: 24px 28px;
    }

    .contain-header {
      padding-top: 24px;
      padding-bottom: 24px;
    }

    .contain-body {
      background: #ffffff;
      border: 1px solid rgba(0, 0, 0, 0.06);
    }
    
    .logo {
      width: 180px;
      height: auto;
    }

		img {
			display: block;
    }
    
    .copyright {
      margin-bottom: 0;
    }

		/* ul.social-icon-container {
			list-style-type: none;
			padding: 8px 0;
      margin: 0;
      line-height: 1;
		} */

		/* li.social-icon-container {
			display: inline-block;
			padding-right: 4px;
      margin: 0;
		} */

		/* .social-icon {
			width: 22px;
			height: 22px;
		} */

    .text-right {
      text-align: right;
    }

    .label-grey-0 {
      color: #95989a;
      margin-bottom: 0;
    }

    .label-grey-1 {
      color: #95989a;
      margin-bottom: 4px;
    }

    .label-grey-2 {
      color: #95989a;
      margin-bottom: 16px;
    }

    .hi-text {
      font-size: 20px;
      font-weight: bold;
      color: #0596D5;
    }

    .td-width-fit-content {
      width: 1%;
      min-width: min-content;
      white-space: nowrap;
      max-width: 100%;
    }

    table.contain-footer * {
      color: #95989a;
    }

    .text-center {
      text-align: center;
    }

    .mb-0 {
      margin-bottom: 0 !important;
    }

    .mb-1 {
      margin-bottom: 4px !important;
    }

    .mb-3 {
      margin-bottom: 16px !important;
    }

		.f-bold {
			font-weight: bold;
		}

    .text-grey {
      color: #95989a !important;
    }

    hr {
      height: 0;
      overflow: visible;
      margin-top: 16px;
      margin-bottom: 16px;
      border: 0;
      border-top: 1px solid rgba(0, 0, 0, 0.1);
    }
    
    @media only screen and (max-device-width: 480px) {
      .body {
        padding: 0;
      }

      .contain-header,
      .contain-body,
      .contain-footer {
        padding: 24px 24px;
      }
    }
	</style>
  @yield('style')
</head>
<body>
	<table class="body" cellpadding="0" cellspacing="0" border="0">
		<tbody>
			<tr>
				<td>
					<table class="container" cellpadding="0" cellspacing="0" border="0" align="center">
						<tbody>
							<tr>
								<td>
                  <table class="contain-header" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                      <td>
                        <img class="logo" src="{{ asset('/icons/logo.png') }}" />
                      </td>
                    </tr>
                  </table>
                  <table class="contain-body" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                      <td>
                        <!-- CONTENT HERE -->
                        <p class="hi-text">Hi{{ isset($user) && $user->fullname ? ' '.$user->fullname : '' }},</p>
                        @yield('content')
                        <!-- CONTENT END -->
                      </td>
                    </tr>
                  </table>
									<table class="contain-footer" cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td>
                        <p class="copyright">© {{ date('Y') }} <a href="/">ROM</a></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>