<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style>
    body {
        margin: 0;
        width: 100%;
    }
    
    div.body {
        width: 100%;
        font-size: 16px;
        background: #fff;
    }

    table {
        text-align: center;
        border-collapse: collapse;
        border-spacing: 0;
    }

    p {
        margin-top: 0;
        line-height: 1.3;
        color: #000 !important;
    }

    .logo {
        width: 250px;
        margin: 60px 0 40px;
    }

    .card,
    .second-card {
        border-radius: 13px;
        max-width: 358px;
        text-align: left;
        margin: auto;
        padding: 16px;
        box-shadow: 0 3px 6px 0px rgba(0, 0, 0, 0.16);
        border-style: solid;
        border-width: thin;
        border-color: #dadce0;
        background: #fff;
        margin-bottom: 16px;
    }

    .second-card {
        text-align: center;
        margin-bottom: 10px;
    }

    .text-header {
        text-align: center;
        font-size: 18px;
        font-family: Avenir,Helvetica,sans-serif;
    }

    .green-line {
        background-color: #00C324;
        min-height: 2px;
        max-height: 2px;
        margin: 18px 16px;
    }

    .reset-button {
        border-radius: 5px;
        text-align: center;
        background-color: #FF4B01;
        color: #fff !important;
        font-size: 18px;
        font-weight: 500;
        padding: 8px;
        margin: 18px 0;
        text-decoration: none;
        display: block;
    }

    a.contact-us {
        color: #FF4B01;
        font-weight: 500;
        text-decoration: none;
    }

    .no-margin-bottom {
        margin-bottom: 0;
    }

    .text-grey {
        color: #8E8E93 !important;
    }

    a.text-link {
        color: #FF3B30 !important;
        margin-bottom: 0;
        text-decoration: none;
    }
    </style>
</head>
<body>
    <div class="body">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img class="logo" src="{{ asset('/icons/logo.png') }}" alt="logo">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="card">
                        <p class="text-header"><b>Atur Ulang Kata Sandi</b></p>
                        <div class="green-line"></div>
                        <p>Hi, {{ isset($user) ? $user->fullname : 'User' }}.</p>
                        <p>Kami menerima permohonan atur ulang kata sandi akun RichOnMart Anda. Silakan Klik tombol dibawah ini untuk melanjutkan</p>
                        <a href="{{ route('webview.reset.password.page', isset($token) ? $token : '') }}" class="reset-button">Reset Password</a>
                        <p>Abaikan email ini jika Anda tidak mengajukan permohonan untuk mengatur ulang kata sandi akun Anda.</p>
                        <p>Jika Anda memerlukan bantuan silahkan <a href="#" class="contact-us">Hubungi Customer Service Kami</a></p>
                        <p class="no-margin-bottom">Regards<br>RichOnMart</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="second-card">
                        <p class="text-grey">Komplek Golden Gate Block G No 2, Batu<br>Selicin, Lubuk Baja. Kota Batam.<br>Kepulauan Riau 29444<br>Contact No: (+62) 0778-4882222</p>
                        <a href="www.richonmart.com" class="text-link">www.richonmart.com</a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="text-grey">Copyright &copy; {{ date('Y') }} RichOnMart. All Rights Reserved</p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>