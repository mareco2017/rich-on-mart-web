@extends('templates.mails.mail-template')

@section('style')
<style>
table.container {
  max-width: 1200px;
}

.hi-text {
  display: none;
}

table.struk {
  width: 100%;
  background: #f1f1f1;
  border: solid 2px #dedede;
  padding: 10px 40px;
}

.h2 {
  text-align: center;
  margin-top: 0;
}

.w-50 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding: 0 8px 0 0;
}

.w-50 + .w-50 {
  padding: 0 0 0 8px;
}

.d-1,
.d-2 {
  display: inline-block;    
  white-space: pre-wrap;
  word-break: break-all;
  vertical-align: top;
}

.d-1 {
  width: 30%;
}

.d-2 {
  width: 70%;
}

.h3 {
  margin: 0;
}

.dummy-text {
  text-align: center;
  margin-bottom: 0;
}
</style>
@endsection
@section('content')
<table class="struk">
  <tr>
    <td colspan="2">
      <h2 class="h2">STRUK PEMBAYARAN TAGIHAN LISTRIK</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">ID PEL</div><div class="d-2">: {{ $options->payment->subscriberID }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">TARIF/DAYA</div><div class="d-2">: {{ $options->payment->tarif.'/'.$options->payment->daya }} VA</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">NAMA</div><div class="d-2">: {{ $options->payment->nama }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">REF</div><div class="d-2">: {{ $options->payment->refnumber }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">LEMBAR TAGIHAN</div><div class="d-2">: {{ $options->payment->lembarTagihan }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td><br></td>
  </tr>
  @foreach($options->payment->detilTagihan as $tagihan)
    @php
      $year = substr($tagihan->periode, 0, 4);
      $month = substr($tagihan->periode, 4, 2);
      $date = Carbon\Carbon::createFromDate($year, $month)->startOfMonth();
    @endphp
    <tr>
      <td>
        <h3>----- LEMBAR TAGIHAN {{ $loop->iteration }} -----</h3>
      </td>
    </tr>
    <tr>
      <td class="w-50">
        <div class="d-1">BL/TH</div><div class="d-2">: {{ $date->format('M y') }}</div>
      </td>
      <td class="w-50">
        <div class="d-1"></div><div class="d-2"></div>
      </td>
    </tr>
    <tr>
      <td class="w-50">
        <div class="d-1">STAND METER</div><div class="d-2">: {{ $tagihan->meterAwal.'-'.$tagihan->meterAkhir }}</div>
      </td>
      <td class="w-50">
        <div class="d-1"></div><div class="d-2"></div>
      </td>
    </tr>
    <tr>
      <td class="w-50">
        <div class="d-1">RP TAG PLN</div><div class="d-2">: {{ GlobalHelper::toCurrency($tagihan->nilaiTagihan) }}</div>
      </td>
      <td class="w-50">
        <div class="d-1"></div><div class="d-2"></div>
      </td>
    </tr>
    <tr>
      <td class="w-50">
        <div class="d-1">ADMIN</div><div class="d-2">: {{ GlobalHelper::toCurrency($tagihan->admin) }}</div>
      </td>
      <td class="w-50">
        <div class="d-1"></div><div class="d-2"></div>
      </td>
    </tr>
    <tr>
      <td class="w-50">
        <div class="d-1">TOTAL</div><div class="d-2">: {{ GlobalHelper::toCurrency($tagihan->total) }}</div>
      </td>
      <td class="w-50">
        <div class="d-1"></div><div class="d-2"></div>
      </td>
    </tr>
  @endforeach
  <tr>
    <td colspan="2">
      <h2 class="text-center">PLN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TOTAL BAYAR</div><div class="d-2">: {{ GlobalHelper::toCurrency($options->payment->totalTagihan) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="mb-3">
        <p class="dummy-text">Terima Kasih</p>
        <p class="dummy-text">Rincian Tagihan dapat diakses di <a href="www.pln.co.id">www.pln.co.id</a></p>
        <p class="dummy-text">Informasi Hubungi Call Center : 123</p>
        <p class="dummy-text">Atau Hub. PLN Terdekat : 53873</p>
      </div>
    </td>
  </tr>
</table>
@endsection