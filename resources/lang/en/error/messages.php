<?php

return [
	"no_user" => "No user binded.",
	"invalid_credential" => "Invalid credential.",
    "create_user_email_used" => "Email Address already used.",
    "create_user_number_used" => "Phone Number already used.",
    "went_wrong" => "Something went wrong.",
    "went_wrong_pls_contact" => "Something went wrong, please contact customer support.",
    "upgrade_first" => "Upgrade your account before you can access this feature",
    "insufficient_balance" => "insufficient balance.",
    "order_cannot_cancel" => "Only order with status :object can be cancelled.",

    //use object
    "not_verified" => ":object not verified.",
    "not_found" => ":object not found.",
    "failed_to" => "failed in :object",

    //prefixes
    "error_while" => "Something went wrong while ",
    "failed" => "failed in ",

    //not found
    "user_not_active" => "User has been banned. Please",
    "wallet_not_found" => "Wallet not found",

    //progress
    "create_user" => "membuat akun.",
    "update_user" => "memperbaharui akun.",
    "set_pin" => "set PIN.",
    "reset-pin" => "resetting PIN.",
    "update_pin" => "changing PIN.",
    "check_pin" => "checking PIN.",
    "update_phone" => "changing phone number.",
    "create_session" => "memuat sesi.",
    "logout_user" => "logging out user.",
    "create_order" => "create order.",
    "update_order" => "update order.",

    "event_request" => "request",
    "event_joined" => "You already joined this event",
    "cannot_join_event" => "Hanya pengguna yang sudah upgrade akun yang dapat mengikuti event ini",
    "referral_code_not_found" => "Referral Code not found",

    "min_topup" => "Minimum Top Up is :object",
    "topup_exceed" => "Upgrade your account to get more wallet limit",
    "topup_exceed_up" => "You cannot exceed your wallet limit anymore",
    "still_active" => "There is still active :object",

    "user_credential_not_found" => "You haven't register yet.",
    "user_phone_not_found" => "User with phone number :object not found.",
    "user_not_found" => "User not found.",
    "user_not_active" => "Your account has been deactivated, please contact us for further information.",
    "error_while_create_user" => "Something went wrong while creating user",
    "failed_create_user" => "Failed to create user.",
    "error_while_update_user" => "Something went wrong while update user",
    "failed_update_user" => "Failed to update user",
    "update_user_no_user" => "Failed to update user. No user binded.",
    "error_while_update_phone" => "Something went wrong while update phone number.",
    "set_pin_no_user" => "Failed to set PIN. No user binded.",
    "check_password_no_user" => "Failed to check Password. Account not found.",
    "update_password_no_user" => "Failed to change Password. No user binded.",
    "pin_already_set" => "PIN already set.",
    "error_while_set_pin" => "Something went wrong while set PIN.",
    "incorrect_password" => "Incorrect Password",
    "incorret_old_pin" => "Incorrect Old PIN",
    "same_new_pin" => "Old PIN and new PIN shouldn't be similar",
    "error_while_update_pin" => "Something went wrong while change PIN",
    "forgot_pin_field_required" => "Field name is required if NRIC is verified",
    "wrong_answer" => "Wrong Answer.",
    "otp_error" => "Invalid or expired PIN.",
    "create_session_no_user" => "Failed to create session. No user binded.",
    "failed_create_session" => "Failed to create session.",
    "error_while_logout_user" => "Something went wrong while logout.",

    "update_bank_failed" => "Failed to update bank." ,
    "update_bank_no_user" => "Failed to update/create bank account, no user binded.",
    "update_bank_acc_failed" => "Failed to update bank account.",
    "set_bank_no_user" => "Failed to set bank account as primary, no user binded.",
    "set_bank_not_author" => "Not authorized to set bank account as primary.",
    "delete_bank_not_author" => "Not authorized to delete bank account.",
    "delete_bank_in_use" => "Cannot delete bank account, bank account is currently in use.",
];