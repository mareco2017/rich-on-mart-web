<?php

return [
    "operation_time" => "Our Customer Service are ready to serve you everyday from :start_time WIB to :end_time WIB",
    'balance_limit' => 'Your maximum account balance is :balance'
];