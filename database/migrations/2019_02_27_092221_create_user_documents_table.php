<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableMorphs('verifiable');
            $table->dateTime('verified_date')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('attachment_id')->nullable();
            $table->json('options')->nullable();
            $table->string('description')->nullable()->default('');
            $table->string('identification_number')->nullable()->default('');
            $table->tinyInteger('status')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('attachment_id')->references('id')->on('attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_documents');
    }
}
