<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_name', 250)->default('');
            $table->string('account_no')->default('');
            $table->morphs('ownable');
            $table->unsignedInteger('bank_id')->nullable();
            $table->string('branch', 250)->default('');
            $table->unsignedInteger('is_primary')->default(0);
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
