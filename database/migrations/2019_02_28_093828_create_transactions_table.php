<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('related_transaction_id')->nullable();
            $table->unsignedInteger('debit_wallet_id')->nullable();
            $table->unsignedInteger('credit_wallet_id')->nullable();
            $table->string('reference_number', 50);
            $table->string('description', 500)->nullable();
            $table->double('total_amount')->default(0);
            $table->double('debit_current_balance')->default(0);
            $table->double('credit_current_balance')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('related_transaction_id')->references('id')->on('transactions');
            $table->foreign('debit_wallet_id')->references('id')->on('wallets');
            $table->foreign('credit_wallet_id')->references('id')->on('wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
