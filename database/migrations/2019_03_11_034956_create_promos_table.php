<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('authorized_by_id')->nullable();
            $table->unsignedInteger('business_id')->nullable();
            $table->nullableMorphs('creatable');
            $table->string('title', 200)->default('');
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->tinyInteger('category'); // Voucher / Business
            $table->tinyInteger('urut')->nullable();
            $table->tinyInteger('type')->nullable(); // Cashback / Diskon
            $table->string('code', 50)->nullable()->unique();
            $table->unsignedInteger('cover_id')->nullable();
            $table->tinyInteger('sub_type')->nullable(); // Percentage / Amount
            $table->double('amount')->default(0);
            $table->mediumInteger('quantity')->default(0); // Max 1.000.000
            $table->double('minimum_purchase')->default(0);
            $table->double('maximum_discount')->nullable();
            $table->longText('tnc')->nullable();
            $table->string('route_name')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('authorized_by_id')->references('id')->on('admins');
            // $table->foreign('business_id')->references('id')->on('businesses');
            $table->foreign('cover_id')->references('id')->on('attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
