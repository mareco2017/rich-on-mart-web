<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::firstOrCreate([
    		'name' => 'PT. BCA (Bank Central Asia) TBK / BCA',
    		'logo' => 'assets/bank/bca.png',
        	'code' => '0140397',
    		'abbr' => 'BCA',
        ]);
        Bank::firstOrCreate([
            'name' => 'PT. BANK MANDIRI',
            'logo' => 'assets/bank/mandiri.png',
        	'code' => '0140397',
            'abbr' => 'MANDIRI',
        ]);
    }
}
