<?php

use Illuminate\Database\Seeder;
use App\Models\AppConfig;
use App\Models\Admin;

class AppConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wallet = Admin::where('email', 'wallet@gmail.com')->first();

        AppConfig::updateOrCreate([
            'key' => 'wallet_account_id'
        ], [
            'value' => $wallet ? $wallet->id : null
        ]);

        AppConfig::updateOrCreate([
            'key' => 'package_registration_number'
        ], [
            'value' => '+6285335566778'
        ]);
    }
}
