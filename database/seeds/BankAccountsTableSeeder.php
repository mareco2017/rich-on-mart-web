<?php

use Illuminate\Database\Seeder;
use App\Models\AppConfig;
use App\Models\Admin;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = AppConfig::where('key', 'wallet_account_id')->value('value');
        $walletAdmin = Admin::find($id);

        if ($walletAdmin) {
            $walletAdmin->bankAccounts()->updateOrCreate([
            	'account_no' => '8322221212'
            ], [
        		'account_name' => 'PT. HALE',
        		'bank_id' => 1,
        		'is_primary' => 1
        	]);
        	$walletAdmin->bankAccounts()->updateOrCreate([
        		'account_no' => '213213213213'
        	], [
        		'account_name' => 'PT. HALE',
        		'bank_id' => 2,
        		'is_primary' => 1
        	]);
        }
    }
}
