<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Helpers\Managers\WalletManager;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin1 = Admin::updateOrCreate([
            'email' => 'wallet@gmail.com',
            'phone_number' => '+6285335566888'
        ], [
            'first_name' => 'Rich On Mart',
            'last_name' => 'Wallet',
            'email' => 'wallet@gmail.com',
            'phone_number' => '+6285335566888',
            'password' => bcrypt('wallet')
        ]);

        $admin2 = Admin::updateOrCreate([
            'email' => 'root@gmail.com',
            'phone_number' => '+6285335566778'
        ], [
            'first_name' => 'Root',
            'last_name' => '',
            'email' => 'root@gmail.com',
            'phone_number' => '+6285335566778',
            'password' => bcrypt('root')
        ]);

        $walletManager = new WalletManager;
        $walletManager->initializeWallets($admin1);
        $walletManager->initializeWallets($admin2);
    }
}
