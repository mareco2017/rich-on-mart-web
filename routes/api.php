<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'as' => 'api.'], function() {
	Route::post('register', 'AuthController@register')->name('register');
	Route::post('refresh', 'AuthController@refresh')->name('refresh');
	Route::post('check-exists', 'AuthController@checkExists')->name('check-exists');
	Route::post('forgot-password', 'ForgotPasswordController@getResetToken')->name('forgot-password');

	// Guest
	Route::group(['middleware' => 'guest:api', 'guard' => 'api'], function() {
		Route::post('login', 'AuthController@login')->name('login');
		Route::post('social-login/{type}', 'AuthController@socialLogin')->name('social-login');
	});

	// Home
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('{category}/products', 'HomeController@productByCategory')->name('product-by-category');
	Route::get('search', 'HomeController@productSearch')->name('search');

	// Logged In
	Route::group(['middleware' => 'auth:api', 'guard' => 'api'], function() {
		Route::post('logout', 'AuthController@logout')->name('logout');
		Route::post('resend', 'AuthController@resend')->name('resend');
		Route::post('verify', 'AuthController@verify')->name('verify');
		
		// User
		Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
			Route::get('me', 'UserController@me')->name('me');
			Route::post('update-profile', 'UserController@updateProfile')->name('update-profile');
			Route::post('change-password', 'UserController@changePassword')->name('change-password');
			Route::post('upload-identity', 'UserController@uploadIdentity')->name('upload-identity');
			Route::post('initialize-wallets', 'UserController@initializeWallets')->name('initialize-wallets');
		});

		// User Address
		Route::group(['prefix' => 'address', 'as' => 'address.'], function() {
			Route::get('', 'UserAddressController@list')->name('list');
			Route::post('create', 'UserAddressController@create')->name('create');
			Route::post('{address}/remove', 'UserAddressController@remove')->name('remove');
			Route::post('{address}/set-primary', 'UserAddressController@setPrimary')->name('set-primary');
		});

		// Bank
		Route::group(['prefix' => 'bank', 'as' => 'bank.'], function() {
			Route::get('', 'BankController@list')->name('list');
		});

		// Bank Account
		Route::group(['prefix' => 'bank-account', 'as' => 'bank-account.'], function() {
			Route::get('', 'BankAccountController@list')->name('list');
			Route::get('payment', 'BankAccountController@listPayment')->name('list-payment');
			Route::post('create', 'BankAccountController@create')->name('create');
			Route::post('{bankAccount}/remove', 'BankAccountController@remove')->name('remove');
			Route::post('{bankAccount}/set-primary', 'BankAccountController@setPrimary')->name('set-primary');
		});

		// Balance
		Route::group(['prefix' => 'balance', 'as' => 'balance.'], function() {
			// Topup
			Route::get('topup-list', 'BalanceController@topupList')->name('topup-list');
			Route::post('topup', 'BalanceController@topup')->name('topup');
			Route::post('topup/{order}/paid', 'BalanceController@topupPaid')->name('topup.paid');
			Route::post('topup/{order}/cancel', 'BalanceController@topupCancel')->name('topup.cancel');

			// Withdraw
			// Route::post('withdraw', 'BalanceController@withdraw')->name('withdraw');
			// Route::post('bonus-withdraw', 'BalanceController@bonusWithdraw')->name('withdraw');
		});

		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
			Route::get('owned', 'OrderController@owned')->name('owned');
		});

		// Payment
		Route::group(['prefix' => 'payment', 'as' => 'payment.'], function() {
			Route::get('', 'PaymentController@list')->name('list');

			// Postpaid
			Route::post('pdam/inquiry', 'PaymentController@inquiryPDAM')->name('pdam.inquiry');
			Route::post('pdam/{order}/payment', 'PaymentController@paymentPDAM')->name('pdam.payment');
			Route::post('telkom/inquiry', 'PaymentController@inquiryTelkom')->name('telkom.inquiry');
			Route::post('telkom/{order}/payment', 'PaymentController@paymentTelkom')->name('telkom.payment');
			Route::post('telepon/inquiry', 'PaymentController@inquiryTelepon')->name('telepon.inquiry');
			Route::post('telepon/{order}/payment', 'PaymentController@paymentTelepon')->name('telepon.payment');
			Route::post('internet/inquiry', 'PaymentController@inquiryInternet')->name('internet.inquiry');
			Route::post('internet/{order}/payment', 'PaymentController@paymentInternet')->name('internet.payment');
			Route::post('listrik/inquiry', 'PaymentController@inquiryListrik')->name('listrik.inquiry');
			Route::post('listrik/{order}/payment', 'PaymentController@paymentListrik')->name('listrik.payment');

			// Prepaid
			Route::post('voucher-game/payment', 'PaymentController@paymentVoucherGame')->name('voucher-game.payment');
			Route::post('pulsa/payment', 'PaymentController@paymentPulsa')->name('pulsa.payment');
			Route::post('paket/payment', 'PaymentController@paymentPaket')->name('paket.payment');
		});

		// Category
		Route::group(['prefix' => 'category', 'as' => 'category.'], function() {
			Route::get('', 'CategoryController@list')->name('list');
		});

		// Product
		Route::group(['prefix' => 'product', 'as' => 'product.'], function() {
			Route::get('', 'ProductController@list')->name('list');
		});

		// Notification
		Route::get('notifications', 'NotificationController@list')->name('notifications');

		// Checkout
		Route::post('process-checkout', 'GroceryController@processCheckout')->name('process-checkout');
		Route::post('checkout', 'GroceryController@checkout')->name('checkout');
	});
});
