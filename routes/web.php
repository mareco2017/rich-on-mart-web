<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Webview
Route::group(['namespace' => 'Webview', 'prefix' => 'webview', 'as' => 'webview.'], function() {
	Route::get('password/reset/{token}', 'ResetPasswordController@resetPage')->name('reset.password.page');
	Route::post('password/reset', 'ResetPasswordController@reset')->name('reset.password');

	Route::get('terms-conditions', 'AboutAppController@termsConditions')->name('terms-conditions');
	Route::get('privacy-policy', 'AboutAppController@privacyPolicy')->name('privacy-policy');
	Route::get('about-us', 'AboutAppController@aboutUs')->name('about-us');
	Route::get('package-info', 'AboutAppController@packageInfo')->name('package-info');

	Route::get('/promo/{promo}', 'PromoController@detail')->name('promo-detail');
	Route::get('cara-isi-saldo', function() {
		return view('webview.cara-isi-saldo');
	});

	// Logged in
	Route::group(['middleware' => 'auth.params'], function() {
		Route::get('binary', 'BinaryController@show')->name('binary');
	});
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    // Guest
	Route::group(['middleware' => 'guest:web-admin', 'guard' => 'web-admin'], function() {
		Route::get('login', 'AuthController@loginPage')->name('login.page');
		Route::post('login', 'AuthController@login')->name('login');
	});

	// Logged In
	Route::group(['middleware' => 'auth:web-admin', 'guard' => 'web-admin'], function() {
	    Route::post('logout', 'AuthController@logout')->name('logout');
		Route::get('', 'DashboardController@index')->name('dashboard');
		
		// Admin
		Route::group(['prefix' => 'admins', 'as' => 'admins.'], function() {
			Route::get('/', 'AdminController@index')->name('index');
			Route::post('/', 'AdminController@ajax')->name('ajax');
			
			Route::get('/new', 'AdminController@edit')->name('create');
			Route::get('/{admin}/edit', 'AdminController@edit')->name('edit');
			Route::post('/store', 'AdminController@update')->name('store');
			Route::post('/{admin}/update', 'AdminController@update')->name('update');
			Route::delete('/{admin}/delete', 'AdminController@delete')->name('delete');
		});
		// User
		Route::group(['namespace' => 'User', 'prefix' => 'users', 'as' => 'users.'], function() {
			// User List
			Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
				Route::get('/', 'UserController@index')->name('index');
				Route::post('/', 'UserController@ajax')->name('ajax');
				Route::get('/{user}/detail', 'UserController@detail')->name('detail');
				Route::get('/new', 'UserController@edit')->name('create');
				Route::get('/{user}/edit', 'UserController@edit')->name('edit');
				Route::post('/store', 'UserController@update')->name('store');
				Route::post('/{user}/update', 'UserController@update')->name('update');
				Route::delete('/{user}/delete', 'UserController@delete')->name('delete');
			});
			// User upgrade request
			Route::group(['prefix' => 'upgrade-request', 'as' => 'upgrade-request.'], function() {
				Route::get('/', 'UpgradeRequestController@index')->name('index');
				Route::post('/', 'UpgradeRequestController@ajax')->name('ajax');
				
				Route::get('/{upgradeRequest}/edit', 'UpgradeRequestController@edit')->name('edit');
				Route::post('/{upgradeRequest}/approve', 'UpgradeRequestController@approve')->name('approve');
				Route::post('/{upgradeRequest}/decline', 'UpgradeRequestController@decline')->name('decline');
				Route::delete('/{upgradeRequest}/delete', 'UpgradeRequestController@delete')->name('delete');
			});
			// User verify identity
			Route::group(['prefix' => 'verify-identity', 'as' => 'verify-identity.'], function() {
				Route::get('/', 'VerifyIdentityController@index')->name('index');
				Route::post('/', 'VerifyIdentityController@ajax')->name('ajax');
				
				Route::get('/{user}/edit', 'VerifyIdentityController@edit')->name('edit');
				Route::post('/{user}/approve', 'VerifyIdentityController@approve')->name('approve');
				Route::post('/{user}/decline', 'VerifyIdentityController@decline')->name('decline');
				Route::delete('/{user}/delete', 'VerifyIdentityController@delete')->name('delete');
			});
			// Route::group(['prefix' => 'pair', 'as' => 'pair.'], function() {
			// 	Route::get('/', 'PairController@index')->name('index');
			// 	Route::post('/', 'PairController@ajax')->name('ajax');
				
			// 	Route::post('/store', 'PairController@update')->name('store');
			// 	Route::post('/{pair}/claim', 'PairController@claim')->name('claim');
			// 	Route::delete('/{pair}/delete', 'PairController@delete')->name('delete');
			// });
		});
		// Bank
		Route::group(['prefix' => 'bank', 'as' => 'bank.'], function() {
			Route::get('/', 'BankController@index')->name('index');
			Route::post('/', 'BankController@ajax')->name('ajax');
			Route::get('/new', 'BankController@edit')->name('create');
			Route::get('/{bank}/edit', 'BankController@edit')->name('edit');
			Route::post('/store', 'BankController@update')->name('store');
			Route::post('/{bank}/update', 'BankController@update')->name('update');
			Route::delete('/{bank}/delete', 'BankController@delete')->name('delete');
		});
		// Bank Account
		Route::group(['prefix' => 'bank-account', 'as' => 'bank-account.'], function() {
			Route::get('/', 'BankAccountController@index')->name('index');
			Route::post('/', 'BankAccountController@ajax')->name('ajax');
			Route::get('/new', 'BankAccountController@edit')->name('create');
			Route::get('/{bankAccount}/edit', 'BankAccountController@edit')->name('edit');
			Route::post('/store', 'BankAccountController@update')->name('store');
			Route::post('/{bankAccount}/update', 'BankAccountController@update')->name('update');
			Route::delete('/{bankAccount}/delete', 'BankAccountController@delete')->name('delete');
		});
		// Promo
		Route::group(['namespace' => 'Promo', 'prefix' => 'promos', 'as' => 'promos.'], function() {
			// Voucher
			// Route::group(['prefix' => 'voucher', 'as' => 'voucher.'], function(){
			// 	Route::get('/', 'VoucherController@index')->name('index');
			// 	Route::get('/new', 'VoucherController@edit')->name('create');
			// 	Route::get('/{promo}/push-notif', 'VoucherController@pushNotif')->name('push-notif');
			// 	Route::get('/{promo}/authorize', 'VoucherController@authorizeVoucher')->name('authorize');
			// 	Route::get('/{promo}/deauthorize', 'VoucherController@deauthorizeVoucher')->name('deauthorize');
			// 	Route::post('/store', 'VoucherController@update')->name('store');
			// 	Route::get('/{promo}/edit', 'VoucherController@edit')->name('edit');
			// 	Route::post('/{promo}/save', 'VoucherController@update')->name('update');
			// 	Route::post('/{promo}/push-notif', 'VoucherController@postPushNotif')->name('post-push-notif');
			// 	Route::post('/list', 'VoucherController@ajax')->name('ajax');
			// 	Route::delete('/{promo}/delete', 'VoucherController@delete')->name('delete');
			// });
			// Banner
			Route::group(['prefix' => 'banner', 'as' => 'banner.'], function(){
				Route::get('/', 'BannerController@index')->name('index');
				Route::get('/new', 'BannerController@edit')->name('create');
				Route::post('/store', 'BannerController@update')->name('store');
				// Route::get('/{promo}/push-notif', 'BannerController@pushNotif')->name('push-notif');
				Route::get('/{promo}/edit', 'BannerController@edit')->name('edit');
				Route::post('/{promo}/save', 'BannerController@update')->name('update');
				// Route::post('/{promo}/push-notif', 'BannerController@postPushNotif')->name('post-push-notif');
				Route::post('/list', 'BannerController@ajax')->name('ajax');
				Route::delete('/{promo}/delete', 'BannerController@deletePromo')->name('delete');
			});
		});
		// Categories
		Route::group(['prefix' => 'categories', 'as' => 'categories.'], function() {
			Route::get('/', 'CategoryController@index')->name('index');
			Route::post('/', 'CategoryController@ajax')->name('ajax');
			Route::get('/new', 'CategoryController@edit')->name('create');
			Route::get('/{category}/edit', 'CategoryController@edit')->name('edit');
			Route::post('/store', 'CategoryController@update')->name('store');
			Route::post('/{category}/update', 'CategoryController@update')->name('update');
			Route::delete('/{category}/delete', 'CategoryController@delete')->name('delete');
		});
		// Products
		Route::group(['prefix' => 'products', 'as' => 'products.'], function() {
			Route::get('/', 'ProductController@index')->name('index');
			Route::post('/', 'ProductController@ajax')->name('ajax');
			Route::get('/new', 'ProductController@edit')->name('create');
			Route::get('/{product}/edit', 'ProductController@edit')->name('edit');
			Route::post('/store', 'ProductController@update')->name('store');
			Route::post('/{product}/update', 'ProductController@update')->name('update');
			Route::delete('/{product}/delete', 'ProductController@delete')->name('delete');
		});
		// Payment
		Route::group(['prefix' => 'payments', 'as' => 'payments.'], function() {
			Route::get('/', 'PaymentController@index')->name('index');
			Route::post('/', 'PaymentController@ajax')->name('ajax');
			Route::get('/external', 'PaymentController@external')->name('external');
			Route::post('/external', 'PaymentController@externalAjax')->name('external-ajax');
			Route::get('/new', 'PaymentController@edit')->name('create');
			Route::get('/{payment}/edit', 'PaymentController@edit')->name('edit');
			Route::post('/store', 'PaymentController@update')->name('store');
			Route::post('/{payment}/update', 'PaymentController@update')->name('update');
			Route::delete('/{payment}/delete', 'PaymentController@delete')->name('delete');
		});
		// Top Up
		Route::group(['prefix' => 'top-up', 'as' => 'top-up.'], function() {
			Route::get('/', 'TopUpController@index')->name('index');
			Route::post('/', 'TopUpController@ajax')->name('ajax');
			Route::get('/new', 'TopUpController@edit')->name('create');
			Route::get('/{order}/edit', 'TopUpController@edit')->name('edit');
			Route::post('/store', 'TopUpController@store')->name('store');
			Route::get('/history', 'TopUpController@history')->name('history');
			Route::post('/history', 'TopUpController@historyAjax')->name('history-ajax');
			Route::get('/{order}/history', 'TopUpController@historyView')->name('history-view');
			Route::get('/{order}/confirmation', 'TopUpController@confirmation')->name('confirmation');
			Route::post('/{order}/update', 'TopUpController@update')->name('update');
			Route::post('/{order}/approve', 'TopUpController@approve')->name('approve');
			Route::post('/{order}/recheck', 'TopUpController@recheck')->name('recheck');
			Route::post('/{order}/decline', 'TopUpController@decline')->name('decline');
			Route::get('/{order}/print/{size}', 'TopUpController@print')->name('print');
		});
		// AppConfig
		Route::group(['prefix' => 'config', 'as' => 'config.'], function() {
			Route::get('/', 'AppConfigController@index')->name('index');
			Route::post('/', 'AppConfigController@ajax')->name('ajax');
			Route::get('/new', 'AppConfigController@edit')->name('create');
			Route::get('/{appConfig}/edit', 'AppConfigController@edit')->name('edit');
			Route::post('/store', 'AppConfigController@update')->name('store');
			Route::post('/{appConfig}/update', 'AppConfigController@update')->name('update');
			Route::delete('/{appConfig}/delete', 'AppConfigController@delete')->name('delete');
		});
		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
			Route::get('/', 'OrderController@index')->name('index');
			Route::post('/', 'OrderController@ajax')->name('ajax');
			Route::get('/{order}/detail', 'OrderController@detail')->name('detail');
			Route::get('/{order}/set-status/{status}', 'OrderController@setStatus')->name('set-status');
			// Route::post('/{order}/update', 'OrderController@update')->name('update');
			// Route::delete('/{order}/delete', 'OrderController@delete')->name('delete');
		});
	});
});