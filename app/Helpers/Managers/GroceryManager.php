<?php

namespace App\Helpers\Managers;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\TransactionType;
use App\Enums\WalletType;
use App\Facades\HistoryLogger;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Unique;
use App\Models\Admin;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;

class GroceryManager extends OrderManager
{
    protected $user;
    protected $order;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($referable, $deliveryType, $userAddressId, $products)
    {
        $order = new Order;
        $order->type = OrderType::GROCERY;
        $order->reference_number = $this->generateReferenceNumber(OrderType::GROCERY);
        $order->status = OrderStatus::WAITING_FOR_PAYMENT;
        $order->expired_at = Carbon::now()->addMinutes(60);
        $order->referable()->associate($referable);
        $order->ownable()->associate($this->user);
        $order->options = [
            'delivery_type' => $deliveryType,
            'user_address_id' => $userAddressId,
            'products' => $products
        ];

        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . 'Failed to create order!', 500);
        }

        foreach ($products as $product) {
            $item = Product::find($product['product_id']);
            if (!$item) throw new Exception(__('error/messages.failed') . 'Failed to create order!, Product not found', 400);

            $orderDetail = new OrderDetail;
            $orderDetail->qty = $product['qty'];
            $orderDetail->price = $product['price'];
            $orderDetail->total = $product['qty']*$product['price'];
            $orderDetail->discount = 0;
            $orderDetail->productable()->associate($item);

            $order->orderDetails()->save($orderDetail);
        } 
        return $order;
    }
}
