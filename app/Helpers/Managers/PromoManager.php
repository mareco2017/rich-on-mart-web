<?php

namespace App\Helpers\Managers;

use Exception;
use Carbon\Carbon;
use App\Models\Admin;
use App\Models\User;
use App\Models\Promo;
// use App\Models\Business;
// use App\Models\Voucher;
// use App\Models\Attachment;
// use App\Helpers\Enums\PromoIssuerType;
use App\Enums\PromoCategory;
// use App\Helpers\Enums\ActiveStatus;
// use Illuminate\Support\Facades\Mail;
use App\Helpers\Managers\AttachmentManager;

class PromoManager
{
    protected $disk = 'public';
    protected $user;
    protected $business;
    protected $maxBanner = 10;
    protected $routeName = 'promo.detail';

    public function __construct($user = null, $business = null)
    {
        $this->user = $user;
        $this->business = $business;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function update(Promo $promo, $data)
    {
        // if ($data['category'] == PromoCategory::VOUCHER) $this->updateVoucherPromo($promo, $data);

        $this->storeData($promo, $data);
        if (!$promo->save()) {
            throw new Exception('Failed to update promo!', 500);
        }

        $this->updateCover($promo, $data);

        return $promo;
    }

    public function updateVoucherPromo(Promo $promo, $data)
    {
        $promo->code = strtoupper($data['code']);
        $promo->quantity = $data['quantity'];
        $promo->type = $data['type'];
        $promo->sub_type = $data['sub_type'];
        $promo->amount = $data['amount'];
        $promo->minimum_purchase = isset($data['minimum_purchase']) ? $data['minimum_purchase'] : 0;
        $promo->maximum_discount = isset($data['maximum_discount']) ? $data['maximum_discount'] : null;
        $promo->tnc = isset($data['tnc']) && $data['tnc'] > 0 ? serialize($data['tnc']) : null;
        $promo->options = ['issuer' => PromoIssuerType::BUSINESS];
        if (array_key_exists('issuer', $data)) {
            $promo->options = ['issuer' => isset($data['issuer']) ? $data['issuer'] : PromoIssuerType::BUSINESS];
        }
    }

    public function storeData($promo, $data) {
        $promo->title = $data['title'];
        // $promo->business()->associate(array_key_exists('owner', $data) ? Business::find($data['owner']) : $this->business);
        $promo->description = isset($data['description']) ? $data['description'] : null;
        $promo->status = isset($data['status']) ? $data['status'] : 0;
        $promo->start_date = $data['start_date'];
        $promo->end_date = $data['end_date'];
        $promo->tnc = isset($data['tnc']) ? $data['tnc'] : null;
        // $promo->route_name = $this->routeName;
        $promo->category = $data['category'];
        // if (!$promo->id) {
        //     $promo->creatable()->associate($this->user);
        // }
    }

    public function updateCover($promo, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $promo->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->setAttachable($promo)->create($data['cover']);
            $promo->cover()->associate($attachment);

            if (!$promo->save()) {
                throw new Exception('Failed to update cover promo!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function generateUrut(Promo $promo)
    {
        $lastUrut = Promo::max('urut');
        $promo->urut = $lastUrut + 1;
        return $promo;
    }

    public function delete(Promo $promo)
    {
        $attachmentManager = new AttachmentManager($this->disk);
        if ($promo->cover) {
            $attachment = $attachmentManager->delete($promo->cover);
        }
        $promo->delete();
    }

    public function setAuthorize(Promo $promo, $authorize = false, $autosave = false)
    {
        if (!$promo) throw new Exception('Failed to set Authorize, Promo not found!', 500);

        if (!$authorize) {
            $promo->authorizedBy()->dissociate();
        } else if ($authorize) {
            $promo->authorizedBy()->associate($this->user);
        }

        if ($autosave) $promo->save();

        return $this;
    }

    public function setStatus(Promo $promo, $status)
    {
        if (!$promo->id) throw new Exception('Failed to set Status, Promo not found!', 500);
        if (!$promo->authorized_by_id) throw new Exception('Failed to update Status, Promo not Authorized!', 500);

        $promo->status = $status;
        $promo->save();

        return $this;
    }

    // API ============================================================================


    public function claim(Promo $promo)
    {   
        if (!$this->user) throw new Exception('Failed to claim voucher, User not binded!', 500);
        if (!$promo) throw new Exception('Failed to claim voucher, Promo not found!', 500);
        $voucher = new Voucher;
        if ($promo->quantity < 1) throw new Exception("Failed to claim voucher! Vouchers already run out!", 400);

        $voucher->ownable()->associate($this->user);
        $voucher->quantity = 1;
        $voucher->promo()->associate($promo);
        $voucher->end_date = $promo->end_date;
        $voucher->status = $promo->status;
        $voucher->category = $promo->category;
        $voucher->type = $promo->type;
        $voucher->code = $promo->code;
        $voucher->cover_id = $promo->cover_id;
        // get url
        $voucher->sub_type = $promo->sub_type;
        $voucher->amount = $promo->amount;
        $voucher->minimum_purchase = $promo->minimum_purchase;
        $voucher->tnc = $promo->tnc ? $promo->tnc : null;

        if (!$voucher->save()) {
            throw new Exception('Failed to update claim voucher!', 400);
        }
        $promo->quantity = $promo->quantity-1;
        if (!$promo->save()) {
            throw new Exception('Something went wrong! Please try again or contact the support.', 400);
        }
        return 'success';
    }

    public function checkVoucher(User $user)
    {
        $vouchers = $user->vouchers->where('business_id', $request->id);
        return $vouchers;
    }
    public function getVouchersList($vendor = null, $ownedVoucher = [], $keyword = null, $limit = 10, $offset = 0)
    {
        if(isset($vendor)){
            $vouchers = Promo::active()->where('business_id',$vendor);
        } else {
            $vouchers = Promo::active();
        }
        if (!empty($keyword)){
            $vouchers = $vouchers->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('code', 'like', '%' . $keyword . '%');
        }
        $vouchers = $vouchers->where('quantity', '>', 0)
                ->voucher()
                ->orderBy('end_date', 'desc')
                ->skip($offset)
                ->take($limit)
                ->get();
        foreach ($vouchers as $key => $voucher) {
            if(in_array($voucher['id'], $ownedVoucher))
            {
                $voucher['owned'] = true;
            } else $voucher['owned'] = false;
        }
        return $vouchers;
    }
   
}
