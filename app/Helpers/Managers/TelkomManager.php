<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Facades\HistoryLogger;
use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\TransactionType;
use App\Enums\WalletType;
use App\Models\Admin;
use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Unique;

class TelkomManager extends OrderManager
{
    protected $user;
    protected $order;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($referable, $inq, Payment $payment = null)
    {
        $order = new Order;
        $order->type = OrderType::PAYMENT_TELKOM;
        $order->reference_number = $this->generateReferenceNumber(OrderType::PAYMENT_TELKOM);
        $order->status = OrderStatus::WAITING_FOR_PAYMENT;
        $order->expired_at = Carbon::now()->addMinutes(30);
        $order->referable()->associate($referable);
        $order->ownable()->associate($this->user);
        $order->options = [
            'inquiry' => $inq
        ];

        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . 'Failed to create order!', 500);
        }

        for ($i=0; $i<(int)$inq->jumlahTagihan; $i++) {
            $tag = $inq->tagihan[$i];
            if (!is_object($tag)) continue;
            $qty = 1;
            $detail = [
                'productable_type' => $payment ? $payment->getMorphClass() : null,
                'productable_id' => $payment ? $payment->id : null,
                'qty' => $qty,
                'price' => $tag->total,
                'total' => $tag->total * $qty,
                'options' => $tag,
                'discount' => 0
            ];

            $order->orderDetails()->create($detail);
        }
        return $order;
    }

    public function setOrder(Order $order)
    {
        if (!in_array($order->type, [OrderType::PAYMENT_TELKOM])) throw new Exception('Order type is not '. OrderType::getString(OrderType::PAYMENT_TELKOM) .'!', 400);
        $this->order = $order;
        return $this;
    }

    public function paid($payment)
    {
        if (!$this->user) throw new Exception('Failed to paid '. OrderType::getString($this->order->type) .', no user binded!', 500);
        if (!$this->order->ownedBy($this->user)) throw new Exception('Not authorized to paid '. OrderType::getString($this->order->type) .'!', 400);
        if (!in_array($this->order->status, [OrderStatus::WAITING_FOR_PAYMENT])) throw new Exception('Order status is not '. OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT) .'!', 400);

        // TODO: Logic payment
        
        $update = $this->updateStatus($this->order, OrderStatus::COMPLETED, ['payment' => $payment]);
        return $update;
    }

    public function cancel()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);

        $this->updateStatus($this->order, OrderStatus::CANCELLED);
        
        return $this->order;
    }
}
