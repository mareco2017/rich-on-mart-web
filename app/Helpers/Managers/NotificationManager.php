<?php

namespace App\Helpers\Managers;

use App\Enums\NotificationCategory;
use App\Enums\NotificationSubType;
use App\Enums\NotificationType;
use App\Helpers\FcmHelper;
use App\Jobs\SendReceipt;
use App\Mail\BonusReferralNotification;
use App\Mail\CashbackNotification;
use App\Mail\ExternalMarketingNotification;
use App\Mail\IdentityDeclinedNotification;
use App\Mail\IdentityNotification;
use App\Mail\PaymentNotification;
use App\Mail\RefundNotification;
use App\Mail\TopUpCreditNotification;
use App\Mail\TopUpDeclinedNotification;
use App\Mail\TopUpNotification;
use App\Mail\TransferNotification;
use App\Mail\WithdrawDeclinedNotification;
use App\Mail\WithdrawNotification;
use App\Mail\PrepaidPLNReceipt;
use App\Mail\PDAMReceipt;
use App\Mail\PostpaidPLNReceipt;
use App\Mail\PhonePostpaidReceipt;
use App\Models\Admin;
use App\Models\Notification;
use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;
use DB;
use Exception;
use GlobalHelper;
use SMS;

class NotificationManager
{
    protected $user;
    protected $notification;
    protected $notificationType;

    public function __construct($user = null, $notification = null)
    {
        // if ($user instanceof User) {
            $this->user = $user;
        // }

        if ($notification instanceof Notification) {
            $this->notification = $notification;
        }
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setNotification(Notification $notification)
    {
        $this->notification = $notification;
        return $this;
    }

    public function setType($type)
    {
        $this->notificationType = $type;
        return $this;
    }

    public function update($receivable = null, $options = [], $subType = null)
    {
        $create = false;
        if (!$this->user) throw new Exception('Failed to create notification, no user binded!', 500);
        if (!$this->notification) {
            $create = true;
            $this->notification = new Notification;
        }

        if ($create) {
            $this->notification->read = 0;
            $this->notification->status = 0;
            if (!in_array($this->notificationType, NotificationType::getList())) throw new Exception('Failed to create notification, type not set!', 500);
        }

        $this->notification->type = $this->notificationType ?? 0;
        if (!$options) {
            $options = [
            'category' => NotificationType::getCategory(NotificationType::IDENTITY)
            ];
        };

        if ($subType) {
            $options = array_merge($options, ['subType' => $subType]);
        }

        $this->notification->options = $options;
        
        $detail = $this->generateText($this->notification, $subType);
        $this->notification->title = $detail['title'];
        $this->notification->description = $detail['description'];

        $this->notification->sendable()->associate($this->user);
        $this->notification->receivable()->associate($receivable);

        if (!$this->notification->save()) {
            throw new Exception('Failed to create/update notification', 500);
        }
        $this->sendnotification($receivable, $this->notification->options['category'], $subType);
        return $this->notification;
    }

    public function read($notificationIds = [])
    {
        if (!$this->user) throw new Exception('No user binded!', 500);
        $updated = [];
        foreach ($notificationIds as $id) {
            $notification = Notification::find($id);
            // if ($notification && $notification->receivable_id == $this->sendable->id) {
                $notification->read = 1;
                if (!$notification->save()) {
                    throw new Exception('Failed to update notification!', 500);
                }
                $updated[] = $notification;
            // }
        }
        return $updated;
    }

    public function sendnotification($user, $category = [], $subType)
    {
        if ($this->notification) {
            // if (in_array(0, $category)){  // APP Notification
            //     $this->appNotification($user, $this->notification);
            // }
            if (in_array(NotificationCategory::PUSH, $category)){  // PUSH Notification
                $this->pushNotification($user, $this->notification, $subType);
            }
            // if (in_array(NotificationCategory::SMS, $category)){  // SMS Notification
            //     $this->smsNotification($user, $this->notification, $subType);
            // }
            if (in_array(NotificationCategory::EMAIL, $category)){  // EMAIL Notification
                $this->emailNotification($user, $this->notification, $subType);
            }
        }
        return $this->notification;
    }

    public function pushNotification($user, $notification, $subType)
    {
        $options = [
            'title' => config('app.name'),
            'body' => $this->generateText($notification, $subType)['description'],
            'data' => [
                'type' => $notification->type,
                'type_string' => (string) $notification->type,
                'order_id' => array_key_exists('order_id', $notification->options) ? $notification->options['order_id'] : null
            ],
        ];
        $tokens = [];
        if ($user instanceof User) {
            $tokens = $user->sessionTokens()->active()->whereNotNull('device_token')->pluck('device_token')->unique()->toArray();
        }
        $push = FcmHelper::pushNotification($tokens, $options);

        return $push;
    }

    public function smsNotification($user, $notification, $subType)
    {
        $message = $this->generateMessage($notification, $subType);
        if (substr($user->phone_number, 0, 3) == "+62") {
            $sms = SMS::send($user->phone_number, $message);
        }

        return $sms;
    }

    public function emailNotification($user, $notification, $subType)
    {
        $sent = false;
        $slug = NotificationType::getSlug($this->notificationType);
        $setting = 1;
        if ($user instanceof User)
        {
            $userOptions = is_object($user->options) ? (array) $user->optionsn : $user->options;
            if (isset($userOptions['settings'])) {
                if (isset($userOptions['settings'][$slug])) {
                    $setting = $userOptions['settings'][$slug];
                }
            }
            if ($setting == 1) {
                $mail = $this->generateEmail($notification, $subType);
                if ($mail) {
                    SendReceipt::dispatch($user, $mail);
                }
            }
        } else if ($user instanceof Admin) {
            if (isset($user->email)) {
                $mail = $this->generateEmail($notification, $subType);
                if ($mail) {
                    SendReceipt::dispatch($user, $mail);
                }
            }
        }
        return $sent;
    }

    public function getInfo()
    {
        switch ($this->notification->type) {
            case NotificationType::TOPUP:
                $info = GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0);
                break;
            case NotificationType::TOPUP_DECLINED:
                $info = GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0);
                break;
            case NotificationType::WITHDRAW:
                $info = GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0);
                break;
            case NotificationType::WITHDRAW_DECLINED:
                $info = GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0);
                break;
            case NotificationType::TRANSFER:
                $info = $this->user->fullname;
                break;
            case NotificationType::CASHBACK:
                $info = number_format($this->notification->options['amount'], 2, ',', '.');
                break;
            case NotificationType::REFUND:
                $info = $this->notification->options['related_reference_number'];
                break;
            case NotificationType::PAY:
                $info = "";
                break;
            default:
                $info = "";
                break;
        }
        return $info;
    }

    public function generateText($notification, $subType = null, $info = "")
    {
        switch ($notification->type) {
            case NotificationType::IDENTITY:
                switch ($subType) {
                    case NotificationSubType::UPGRADED:
                        // $message = "Congrats, Your account has been upgraded";
                        $title = "Selamat, Akun Anda Berhasil Diupgrade!";
                        $description = "Aplikasi Mareco Anda makin komplit, sekarang Anda dapat menikmati semua fitur khusus upgraded user"; 
                        break;
                    case NotificationSubType::UPGRADED_DECLINED:
                        $title = "Akun Anda gagal Diupgrade!";
                        $description = "Mohon upgrade ulang akun Anda"; 
                        break;
                    case NotificationSubType::VERIFIED:
                        // $description = "Congrats, Your ". $info ." has been verified";
                        $title = "Verifikasi ". $this->notification->options['changed'] ." Anda Berhasil";
                        $description = "Email Anda ". $this->notification->options['change_to'] ." telah berhasil diverifikasi, kini akun Anda semakin aman!";
                        break;
                    case NotificationSubType::EMAIL_CHANGED:
                        // $description = "Congrats, Your ". $this->notification->options['changed'] ." has been changed";
                        $title = "Pengantian ". $this->notification->options['changed'] ." Anda Berhasil";
                        $description = "Email Anda telah berhasil diubah menjadi ". $this->notification->options['change_to'];
                        break;
                    case NotificationSubType::PHONE_CHANGED:
                        // $description = "Congrats, Your ". $this->notification->options['changed'] ." has been changed";
                        $title = "Pengantian ". $this->notification->options['changed'] ." Anda Berhasil";
                        $description = "No. Handphone Anda telah berhasil diubah menjadi ". $this->notification->options['change_to'];
                        break;
                    case NotificationSubType::EVENT_EXT_MARKETING_APPROVE:
                        $title = "Selamat! Permintaan Anda telah diterima";
                        $description = "Anda telah terdaftar menjadi Eksternal Marketing Mareco. Kode Referral Anda : ". $this->notification->options['referral_code'];
                        $description .= "\n" . "Ayo! Ajak toko terdekatmu bergabung dengan Mareco hari ini!";
                        break;
                    default:
                        $description = "Congrats";
                        break;
                }
                break;
            case NotificationType::PROMO:
                $title = $this->notification->title;
                $description = $this->notification->message;
                break;
            case NotificationType::VOUCHER:
                $title = $this->notification->title;
                $description = $this->notification->message;
                break;
            case NotificationType::TOPUP:
                switch ($subType) {
                    case NotificationSubType::BANK_TRANSFER:
                        $title = "Top-up " . $this->getInfo() . " berhasil";
                        $description = "Bank Transfer - " . $this->notification->options['bank_destination'];
                        $description .= "\n" . $this->notification->options['reference_number'];
                        break;
                    case NotificationSubType::MERCHANT:
                        $title = "Top-up " . $this->getInfo() . " berhasil";
                        $description = "Top Up Merchant - " . $this->notification->options['sender_name'];
                        $description .= "\n" . $this->notification->options['reference_number'];
                        break;
                    default:
                        $title = "Top-up " . $this->getInfo() . " berhasil";
                        $description = "Top up berhasil";
                        break;
                }
                break;
            case NotificationType::TOPUP_DECLINED:
                $title = "Top-up Anda telah ditolak";
                $description = $this->notification->options['message'];
                break;
            case NotificationType::WITHDRAW_DECLINED:
                $title = "Withdraw Anda telah ditolak";
                $description = $this->notification->options['message'];
                break;
            case NotificationType::WITHDRAW:
                $title = "Withdraw " . $this->getInfo() . " berhasil";
                $description = $this->notification->options['bank_destination'];
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::TRANSFER:
                $title = "Transfer from " . $this->getInfo() . " berhasil";
                $description = "+ " . GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0) . " from " . $this->user['phone_number'];
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::CASHBACK:
                $title = "Cashback " . $this->getInfo() . " Point";
                $description = $this->notification->options['sender_name'] . " - " . $this->notification->options['related_reference_number'];
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::REFUND:
                $title = "Refund " . $this->getInfo() . " berhasil";
                $description = $this->notification->options['sender_name'] . " - " . $this->notification->options['related_reference_number'];
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::PAY:
                $title = "";
                $description = "+ " . GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0) . " from " . $this->user->phone_number;
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::BONUS_REFERRAL:
                $title = "Referral Anda telah disetujui";
                $description = "+ " . GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0) . " from Mareco";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::TOPUP_CREDIT:
                $title = "Top-up credit berhasil";
                $description = "+ " . GlobalHelper::toCurrency($this->notification->options['amount'], 'Rp', null, 0) . " from Mareco";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::PLN_PREPAID:
                $title = "Pembelian Token Listrik berhasil";
                $description = "";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::PLN_POSTPAID:
                $title = "Pembayaran Tagihan PLN berhasil";
                $description = "";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::PDAM:
                $title = "Pembayaran Tagihan Air berhasil";
                $description = "";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            case NotificationType::PHONE_POSTPAID:
                $title = "Pembayaran Tagihan Telepon berhasil";
                $description = "";
                $description .= "\n" . $this->notification->options['reference_number'];
                break;
            default:
                $description = "";
                break;
        }
        return ['title' => $title, 'description' => $description];
    }

    public function generateMessage($notification,  $subType = null)
    {
        // Note: Generate message according type, with: amount, reference user/business
        switch ($notification->type) {
            case NotificationType::IDENTITY:
                switch ($subType) {
                    case NotificationSubType::UPGRADED:
                        $message = "Selamat, akun Anda berhasil diupgrade!";
                        $message .= "Aplikasi Mareco Anda makin komplit, sekarang Anda dapat menikmati semua fitur khusus upgraded user"; 
                        break;
                    case NotificationSubType::UPGRADED_DECLINED:
                        $message = "Akun Anda gagal diupgrade!";
                        $message .= "Mohon upgrade ulang akun Anda"; 
                        break;
                    case NotificationSubType::VERIFIED:
                        $message = "Verifikasi ". $this->notification->options['changed'] ." Anda berhasil";
                        $message .= "Email Anda ". $this->notification->options['change_to'] ." telah berhasil diverifikasi, kini akun Anda semakin aman!";
                        break;
                    case NotificationSubType::EMAIL_CHANGED:
                        $message = "Pengantian ". $this->notification->options['changed'] ." Anda berhasil";
                        $message .= "Email Anda telah berhasil diubah menjadi ". $this->notification->options['change_to'];
                        break;
                    case NotificationSubType::PHONE_CHANGED:
                        $message = "Pengantian ". $this->notification->options['changed'] ." Anda berhasil";
                        $message .= "No. Handphone Anda telah berhasil diubah menjadi ". $this->notification->options['change_to'];
                        break;
                    case NotificationSubType::EVENT_EXT_MARKETING_APPROVE:
                        $message = "Selamat! Permintaan Anda telah diterima";
                        $message .= "Anda telah terdaftar menjadi Eksternal Marketing Mareco. Kode Referral Anda : ". $this->notification->options['referral_code'];
                        $message .= "\n" . "Ayo! Ajak toko terdekatmu bergabung dengan Mareco hari ini!";
                        break;
                    default:
                        $message .= "Selamat, pembaharuan akun Anda berhasil";
                        break;
                }
                break;
            case NotificationType::PROMO:
                $message = "Promo...";
                break;
            case NotificationType::VOUCHER:
                $message = "Voucher...";
                break;
            case NotificationType::TOPUP:
                $message = "Top-up saldo Mareco sebesar " . $this->getInfo() . " berhasil. Kode transaksi : " . $this->notification->options['reference_number'];
                break;
            case NotificationType::WITHDRAW:
                $message = "Withdraw saldo Mareco sebesar " . $this->getInfo() . " ke rekening " . $this->notification->options['bank_destination'] ." berhasil. Kode transaksi : " . $this->notification->options['reference_number'];
                break;
            case NotificationType::TRANSFER:
                $message = "Transfer...";
                break;
            case NotificationType::CASHBACK:
                $message = "Selamat, Anda mendapatkan cashback dari " . $this->notification->options['sender_name'] . " untuk transaksi " . $this->notification->options['related_reference_number'] . " sebesar " . $this->getInfo();
                break;
            case NotificationType::REFUND:
                $message = "Anda mendapatkan refund dari " . $this->notification->options['sender_name'] . " untuk transaksi " . $this->notification->options['related_reference_number'] . " sebesar " . $this->getInfo();
                break;
            case NotificationType::PAY:
                $message = "Pay";
                break;
            case NotificationType::TOPUP_DECLINED:
                $message = "Topup Anda telah ditolak. " . $this->notification->options['message'];
                break;
            case NotificationType::WITHDRAW_DECLINED:
                $message = "Withdraw Anda telah ditolak. " . $this->notification->options['message'];
                break;
            case NotificationType::NEWLETTER:
                $message = "Newsletter";
                break;
            case NotificationType::TOPUP_CREDIT:
                $message = "Top Up Credit sebesar " . $this->getInfo() . "Anda berhasil";
                break;
            default:
                $message = "";
                break;
        }

        return $message;
    }

    public function generateEmail($notification, $subType = null)
    {
        switch ($notification->type) {
            case NotificationType::IDENTITY:
                // Notif for user when user upgrade request approved
                switch ($subType) {
                    case NotificationSubType::UPGRADED:
                        $mail = new IdentityNotification($notification);
                        break;
                    case NotificationSubType::UPGRADED_DECLINED:
                        $mail = new IdentityDeclinedNotification($notification);
                        break;
                    case NotificationSubType::VERIFIED:
                        // $description = "Congrats, Your ". $info ." has been verified";
                        $mail = null;
                        break;
                    case NotificationSubType::EMAIL_CHANGED:
                        $mail = null;
                        break;
                    case NotificationSubType::PHONE_CHANGED:
                        $mail = null;
                        break;
                    case NotificationSubType::EVENT_EXT_MARKETING_APPROVE:
                        $mail = new ExternalMarketingNotification($notification);
                        break;
                    default:
                        $mail = null;
                        break;
                }
                break;
            case NotificationType::PROMO:
                $mail = new IdentityNotification($notification);
                break;
            case NotificationType::VOUCHER:
                $mail = new PaymentNotification($notification);
                break;
            case NotificationType::TOPUP:
                // Notif user when top up successfully
                $mail = new TopUpNotification($notification);
                break;
            case NotificationType::TOPUP_DECLINED:
                // Notif user when top up declined
                $mail = new TopUpDeclinedNotification($notification);
                break;
            case NotificationType::WITHDRAW_DECLINED:
                // Notif user when top up declined
                $mail = new WithdrawDeclinedNotification($notification);
                break;
            case NotificationType::WITHDRAW:
                // Notif user when withdraw successfully
                $mail = new WithdrawNotification($notification);
                break;
            case NotificationType::TRANSFER:
                // Notif for user when user got money from other user
                $mail = new TransferNotification($notification);
                break;
            case NotificationType::CASHBACK:
                // Notif for user when user got cashback after transaction
                $mail = new CashbackNotification($notification);
                break;
            case NotificationType::REFUND:
                // Notif for user when user got refund from store
                $mail = new RefundNotification($notification);
                break;
            case NotificationType::PAY:
                // Notif for user when pay success
                $mail = new PaymentNotification($notification);
                break;
            case NotificationType::BONUS_REFERRAL:
                // Notif for user when their successfully invite a merchant
                $mail = new BonusReferralNotification($notification);
                break;
            case NotificationType::TOPUP_CREDIT:
                $mail = new TopUpCreditNotification($notification);
                break;
            case NotificationType::PLN_PREPAID:
                // Notif for uer when 
                $mail = new PrepaidPLNReceipt($notification);
                break;
            case NotificationType::PLN_POSTPAID:
                $mail = new PostpaidPLNReceipt($notification);
                break;
            case NotificationType::PDAM:
                $mail = new PDAMReceipt($notification);
                break;
            case NotificationType::PHONE_POSTPAID:
                $mail = new PhonePostpaidReceipt($notification);
                break;
            default:
                $mail = null;
                break;
        }
        return $mail;
    }
}
