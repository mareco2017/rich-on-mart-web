<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Category;
use App\Helpers\Managers\AttachmentManager;

class CategoryManager
{
    protected $disk = 'public';

    public function update(Category $category, $data)
	{
		$category->title = $data['title'];
		$category->commission = $data['commission'];
		$category->discount = isset($data['discount']) ? $data['discount'] : 0;
        $category->status = $data['status'];
		$this->updateCover($category, $data);
		
        if (!$category->save()) {
            throw new Exception('Failed to update category!', 500);
        }
        return $category;
	}

    public function updateCover(Category $category, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $category->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->setAttachable($category)->create($data['cover']);
            $category->cover()->associate($attachment);

            if (!$category->save()) {
                throw new Exception('Failed to update cover!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(Category $category)
    {
        $attachmentManager = new AttachmentManager($this->disk);
        if ($category->cover) {
            $attachment = $attachmentManager->delete($category->cover);
        }
        $category->delete();
    }
}
