<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Facades\HistoryLogger;
use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\TransactionType;
use App\Enums\WalletType;
use App\Models\Admin;
use App\Models\User;
use App\Models\Order;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Unique;

class TopupManager extends OrderManager
{
    protected $user;
    protected $order;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($referable, $amount, $destBankAccountId)
    {
        $order = new Order;
        $order->type = OrderType::TOPUP;
        $order->reference_number = $this->generateReferenceNumber(OrderType::TOPUP);
        $order->status = OrderStatus::WAITING_FOR_PAYMENT;
        $order->expired_at = Carbon::now()->addMinutes(30);
        $order->referable()->associate($referable);
        $order->ownable()->associate($this->user);
        $order->options = [
            'dest_bank_account_id' => $destBankAccountId
        ];

        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . 'Failed to create order!', 500);
        }

        $qty = 1;
        $detail = [
            'productable_type' => null,
            'productable_id' => null,
            'qty' => $qty,
            'price' => $amount,
            'total' => $amount * $qty,
            'discount' => Unique::generateTopupUniqueNumber(3, true)
        ];

        $order->orderDetails()->create($detail);
        return $order;
    }

    public function setOrder(Order $order)
    {
        if (!in_array($order->type, [OrderType::TOPUP])) throw new Exception('Order type is not '. OrderType::getString(OrderType::TOPUP) .'!', 400);
        $this->order = $order;
        return $this;
    }

    public function paid()
    {
        if (!$this->user) throw new Exception('Failed to paid '. OrderType::getString($this->order->type) .', no user binded!', 500);
        if (!$this->order->ownedBy($this->user)) throw new Exception('Not authorized to paid '. OrderType::getString($this->order->type) .'!', 400);
        if (!in_array($this->order->status, [OrderStatus::WAITING_FOR_PAYMENT])) throw new Exception('Order status is not '. OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT) .'!', 400);
        
        $update = $this->updateStatus($this->order, OrderStatus::ONGOING);
        return $update;
    }

    public function cancel()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);

        $this->updateStatus($this->order, OrderStatus::CANCELLED);
        
        return $this->order;
    }

    public function approve()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);
        if (!$this->order->isActive()) throw new Exception("Order is not active anymore!", 500);
        
        $debitWallet = $this->order->ownable ? $this->order->ownable->getWallet(WalletType::PRIMARY) : null;
        $creditWallet = $this->order->referable ? $this->order->referable->getWallet(WalletType::PRIMARY) : null;

        $transactionManager = new TransactionManager($this->user);
        $transactionManager->setOrder($this->order);

        $transaction = $transactionManager->create($debitWallet, $creditWallet, $this->order->total, TransactionType::TOPUP);
        $bonusTransaction = $transactionManager->setRelatedTransaction($transaction)->create($debitWallet, $creditWallet, $this->order->total_discount, TransactionType::BONUS);

        $options = [];
        if ($this->user) {
            $options['approvable_type'] = $this->user->getMorphClass();
            $options['approvable_id'] = $this->user->id;
            $options['approvable_date'] = Carbon::now();
        }

        $this->updateStatus($this->order, OrderStatus::COMPLETED, $options);

        return $this->order;
    }
     public function decline()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);
        if (!$this->order->isActive()) throw new Exception("Order is not active anymore!", 500);
        $this->updateStatus($this->order, OrderStatus::DECLINED);
        
        return $this->order;
    }
}
