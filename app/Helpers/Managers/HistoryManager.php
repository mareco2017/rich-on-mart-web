<?php

namespace App\Helpers\Managers;

use Exception;
use Carbon\Carbon;
use App\Models\History;

class HistoryManager
{
    protected $user;
    protected $client;
    protected $number;

    public function __construct()
    {
    	// 
    }

    public function create($historiable, $creatable = null, $status, $remarks = null)
    {
    	$history = new History;
    	$history->status = $status;
    	$history->remarks = $remarks;
    	$history->historiable()->associate($historiable);
    	if ($creatable) {
    		$history->creatable()->associate($creatable);
    	}

    	if (!$history->save()) {
            throw new Exception('Failed to create history!', 500);
    	}

    	return $history;
    }
}
