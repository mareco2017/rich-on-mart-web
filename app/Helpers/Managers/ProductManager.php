<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Product;
use App\Helpers\Managers\AttachmentManager;

class ProductManager
{
    protected $disk= 'public';

	public function update(Product $product, $data)
	{
		$product->title = $data['title'];
		$product->price = isset($data['price']) ? $data['price'] : 0;
		$product->description = isset($data['description']) ? $data['description'] : '';
		$product->discount = isset($data['discount']) ? $data['discount'] : 0;
        $product->unit = $data['unit'];
        $product->category()->associate($data['category']);
        $product->status = $data['status'];
		$this->updateCover($product, $data);
		
        if (!$product->save()) {
            throw new Exception('Failed to update product!', 500);
        }
        return $product;
	}

    public function updateCover(Product $product, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $product->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->setAttachable($product)->create($data['cover']);
            $product->cover()->associate($attachment);

            if (!$product->save()) {
                throw new Exception('Failed to update cover!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(Product $product)
    {
        $attachmentManager = new AttachmentManager($this->disk);
        if ($product->cover) {
            $attachment = $attachmentManager->delete($product->cover);
        }
        $product->delete();
    }
}
