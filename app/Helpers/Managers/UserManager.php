<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\User;
use App\Models\Admin;
use App\Models\Role;
use App\Enums\UserDocumentType;
use App\Enums\UserDocumentStatus;
use App\Helpers\Managers\DocumentManager;
use App\Helpers\Managers\BinaryManager;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Support\Facades\Hash;

class UserManager
{
    protected $user;
    protected $countryCode;

    public function __construct($user = null)
    {
        $this->user = $user;
        $this->countryCode = 'ID';
    }

    public function create(User $user, Role $role, $data)
    {
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        
        $user->phone_number = PhoneNumber::make($data['phone_number'], $this->countryCode);
        if (isset($data['password'])) {
            $user->password = bcrypt($data['password']);
        }
        $user->email = isset($data['email']) ? $data['email'] : null;
        $user->role()->associate($role);
        
        // if (array_key_exists('referral_code', $data) && $data['referral_code'] != null) {
        //     $eventManager = new EventManager(); 
        //     $user->options = [
        //         'referral_user_id' => $eventManager->findReferral($data['referral_code']),
        //         'verify_referral' => VerificationStatus::PENDING
        //     ];
        // }

        if (!$user->save()) {
            throw new Exception('Failed create user!', 500);
        }

        // $createWallet = $this->initializeWallets($admin);

        return $admin;
    }

    public function delete(User $user)
    {
        $user->delete();
        return $user;
    }

    public function approveIdentity($data, Admin $admin)
    {
        $document = $this->verifyUserData();

        $documentManager = new DocumentManager($admin);
        $nricVerified = $documentManager->verify($document['nric'], $data);
        $selfieVerified = $documentManager->verify($document['selfie'], $data);

        $this->storeUpgradeData($data);
        // $this->createBinary(isset($data['leader_id']) ? $data['leader_id'] : null);
        return $this->user;
    }

    public function declineIdentity($data, Admin $admin)
    {
        $document = $this->verifyUserData();

        $documentManager = new DocumentManager($admin);
        $nricVerified = $documentManager->decline($document['nric'], $data);
        $selfieVerified = $documentManager->decline($document['selfie'], $data);

        return $this->user;
    }

    public function verifyUserData()
    {
        $nric = $this->user->documents()->type(UserDocumentType::NRIC)->first();
        $selfie = $this->user->documents()->type(UserDocumentType::SELFIE)->first();
        if (!$nric) throw new Exception("User has not uploaded NRIC or not found!", 400);
        if (!$selfie) throw new Exception("User has not uploaded Selfie or not found!", 400);
        if ($this->user->documents_status['nric'] != UserDocumentStatus::PENDING) throw new Exception("User already reviewed!", 400);

        return ['nric' => $nric, 'selfie' => $selfie];
    }

    public function storeUpgradeData($data)
    {
        $user = $this->user;
        $user->first_name = $data['first_name'];
        $user->last_name = isset($data['last_name']) ? $data['last_name'] : '';
        
        if (!$user->save()) throw new Exception(__('error/messages.failed_update_user'), 500);
        $this->user = $user;
    }

    public function createBinary($leaderId = null)
    {
        $binaryManager = new BinaryManager($this->user);
        $updatedBinary = $binaryManager->create($leaderId);
    }

    public function checkPassword($password)
    {
        if (!$this->user) throw new Exception(_('error/
            messages.check_password_no_user'), 500);

        if (!Hash::check($password, $this->user->password)) {
            throw new Exception(_('error/messages.incorrect_password'), 400);
        }
        return $this->user;
    }
}
