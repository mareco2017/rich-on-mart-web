<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\Order;
use App\Models\User;
use App\Models\Business;
use App\Models\Promo;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\Config as AppConfig;
use App\Enums\OrderType;
use App\Enums\OrderStatus;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\WalletType;
use App\Facades\HistoryLogger;
use WalletManager;

class OrderManager
{
    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    protected function log(Order $order, $user, $status)
    {
        $history = HistoryLogger::create($order, $user, $status);
        return $history;
    }

    public function updateStatus(Order $order, $status, $options = [])
    {
        $order->status = $status;
        $currentOptions = [];
        if (is_array((array)$order->options)) {
            (array) $currentOptions = $order->options;
        }
        $mergedOptions = array_merge((array)$currentOptions, $options);
        $order->options = empty($mergedOptions) ? null : $mergedOptions;
        
        if (!$order->save()) {
            throw new Exception('Failed to update order status!', 500);
        }
        $history = HistoryLogger::create($order, $this->user, $order->status);
        return $order;
    }

    protected function updateOptions(Order $order, $options = [])
    {
        $currentOptions = [];
        if (is_array($order->options)) {
            $currentOptions = $order->options;
        }
        $mergedOptions = array_merge($currentOptions, $options);
        $order->options = empty($mergedOptions) ? null : $mergedOptions;
        if ($this->date) {
            $order->updated_at = $this->date;
            $order->timestamps = false;
        }
        if (!$order->save()) {
            throw new Exception('Failed to update order options!', 500);
        }
        
        return $order;
    }

    protected function generateReferenceNumber($type)
    {
        $code = OrderType::getCode($type);
        $reference = $code;
        $minLength = 3;

        $latest = Order::where('reference_number', 'like', $reference.'%')->max('reference_number');
        if (!$latest) {
            $referenceNumber = $reference.str_pad('1', $minLength, "0", STR_PAD_LEFT).chr(rand(65,90));
        } else {
            // dd($latest);
            $number = (int) substr($latest, strlen($reference));
            $number++;
            if (strlen((string) $number) > $minLength) {
                $minLength = strlen((string) $number);
            }
            $referenceNumber = $reference.str_pad($number, $minLength, "0", STR_PAD_LEFT).chr(rand(65,90));
        }
        
        return $referenceNumber;
    }
}
