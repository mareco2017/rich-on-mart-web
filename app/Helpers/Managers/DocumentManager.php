<?php

namespace App\Helpers\Managers;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\UserDocument;
use App\Enums\UserDocumentType;
use App\Enums\UserDocumentStatus;
use App\Helpers\Managers\AttachmentManager;

class DocumentManager
{
    protected $user;
    protected $disk;

    public function __construct($user = null)
    {
        $this->user = $user;
        $this->disk = 'public';
    }

    public function create($type, $data)
    {
        if (!$this->user) throw new Exception("Failed to create ". UserDocumentType::getString($type) .", no user binded!", 500);
        $document = $this->user->documents()->firstOrNew(['type' => $type]);
        $document->description = isset($data['description']) ? $data['description'] : '';
        $document->identification_number = isset($data['identification_number']) ? $data['identification_number'] : '';
        $document->options = isset($data['options']) ? $data['options'] : null;

        if ($document->attachment) {
            $oldAttachment = $document->attachment;
        }

        if (isset($data['attachment'])) {
            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->setAttachable($document)->create($data['attachment']);
            $document->attachment()->associate($attachment);
        }

        if (!$document->save()) {
            throw new Exception('Failed to create document!', 500);
        }

        if (isset($oldAttachment)) {
            $attachmentManager = new AttachmentManager($this->disk);
            $deteleAttachment = $attachmentManager->delete($oldAttachment, true);
        }

        return $document;
    }

    public function verify(UserDocument $document, $data = [])
    {
        if (!$this->user) throw new Exception("Failed to verify ". UserDocumentType::getString($type) .", no user binded!", 500);

        $document->identification_number = $data['identification_number'];
        $document->description = isset($data['description']) ? $data['description'] : '';
        $document->verified_date = Carbon::now();
        $document->status = UserDocumentStatus::VERIFIED;
        if (!$document->save()) throw new Exception('Failed to update document!', 500);
        
        return $document;
    }

    public function decline(UserDocument $document, $data = [])
    {
        if (!$this->user) throw new Exception("Failed to decline ". UserDocumentType::getString($type) .", no user binded!", 500);
        
        // $document->options = array_merge((array) $document->options, ['remarks' => $remarks]);
        $document->description = isset($data['description']) ? $data['description'] : '';
        $document->verified_date = Carbon::now();
        $document->status = UserDocumentStatus::REJECTED;
        if (!$document->save()) throw new Exception('Failed to update document!', 500);
        
        return $document;
    }
}
