<?php

namespace App\Helpers\Managers;

use App\Models\Bank;
use App\Models\BankAccount;
use App\Helpers\Managers\AttachmentManager;
use Exception;

class BankManager
{
    protected $user;
    protected $disk;

    public function __construct($user = null)
    {
        $this->user = $user;
        $this->disk = 's3';
    }

    public function update(Bank $bank, $data)
    {
        $bank->name = $data['name'];
        $bank->code = $data['code'];
        $bank->abbr = $data['abbr'];
        // if ($data['logo']) {
        //     $upload = AwsHelper::uploadFile($data['logo'], 'logo', 'images/', 'jpeg,bmp,png');
        //     $bank->logo = $upload->key;
        // }

        if (isset($data['logo'])) {
            $this->updateCover($bank, $data);
            // $attachmentManager = new AttachmentManager($this->disk);
            // $attachment = $attachmentManager->create($data['logo']);
            // $bank->cover()->associate($attachment);
        }
        if (!$bank->save()) {
            throw new Exception(__('error/messages.update_bank_failed'), 500);
        }

        return $bank;
    }
       
    public function updateCover($bank, $data)
    {
        if (array_key_exists('logo', $data) && isset($data['logo'])) {
            $oldCover = $bank->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->create($data['logo']);
            $bank->cover()->associate($attachment);

            if (!$bank->save()) {
                throw new Exception('Failed to update bank cover!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(Bank $bank)
    {
        $bank->delete();

        return $bank;
    }

    public function updateBankAccount(BankAccount $bankAccount, $data)
    {
        if (!$this->user) throw new Exception(__('error/messages.update_bank_no_user'), 500);

        $bankAccount->account_name = $data['account_name'];
        $bankAccount->account_no = $data['account_no'];
        $bankAccount->is_primary = $data['is_primary'];
        $bankAccount->ownable()->associate($this->user);
        $bankAccount->bank()->associate($data['bank_id']);
        
        if (count($this->user->bankAccounts) < 1 && !$bankAccount->is_primary) {
            $bankAccount->is_primary = 1;
        }

        if (!$bankAccount->save()) {
            throw new Exception(__('error/messages.update_bank_acc_failed'), 500);
        }

        if (isset($data['is_primary']) && (bool) $data['is_primary']) {
            $this->setBankAccountAsPrimary($bankAccount);
        }

        return $bankAccount;
    }

    public function setBankAccountAsPrimary(BankAccount $bankAccount)
    {
        if (!$this->user) throw new Exception(__('error/messages.set_bank_no_user'), 500);
        if (!$bankAccount->ownedBy($this->user)) throw new Exception(__('error/messages.set_bank_not_author'), 400);
        
        $this->user->bankAccounts()->whereNotIn('id', [$bankAccount->id])->update(['is_primary' => 0]);
        
        $bankAccount->is_primary = 1;

        if (!$bankAccount->save()) {
            throw new Exception('Failed to set as primary!', 500);
        }

        return $bankAccount;
    }

    public function deleteBankAccount(BankAccount $bankAccount)
    {
        if (!$bankAccount->ownedBy($this->user)) throw new Exception(__('error/messages.delete_bank_not_author'), 400);
        
        // $isPrimary = $bankAccount->is_primary;
        // $activeOrder = Order::active()->type([OrderType::WITHDRAW, OrderType::WITHDRAW_CREDIT, OrderType::TOPUP, OrderType::TOPUP_CREDIT])
        //     ->where('options->bank_account_id', (string)$bankAccount->id)
        //     ->orWhere('options->bank_account_id', $bankAccount->id)
        //     ->orWhere('options->dest_bank_account_id', (string)$bankAccount->id)
        //     ->orWhere('options->dest_bank_account_id', $bankAccount->id)
        //     ->first();
        // if ($activeOrder) throw new Exception(__('error/messages.delete_bank_in_use'), 400);
        $bankAccount->delete();

        // if ($isPrimary) {
        //     $anotherAccount = $this->user->bankAccounts()->first();
        //     if ($anotherAccount) $this->setBankAccountAsPrimary($anotherAccount);
        // }

        return $bankAccount;
    }
}
