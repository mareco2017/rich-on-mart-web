<?php

namespace App\Helpers\External;

use SoapClient;;

class PulsaHelper 
{
	const BBUZ_PP_HOST	= 'http://117.102.64.238:1212/pulsaPrabayar.php?wsdl';
	const SOAP_TIMEOUT	= 60;
	const SOAP_RESPONSE_TIMEOUT = 240;
	
	private $apiKey		= 'bd8446eaa574ab00ab72249f8b06a759';	// api key
	private $secretKey	= 'a3355920444fae8eb84b1f6304bbe1d9';	// secret key
	private $soap;
	
	public function __construct() 
	{
		$options = [
            // 'location' => self::BBUZ_PP_HOST,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'timeout' => self::SOAP_TIMEOUT,
            'response_timeout' => self::SOAP_RESPONSE_TIMEOUT,
            'login' => $this->apiKey,
            'password' => $this->secretKey
        ];

		$this->soap = new SoapClient(
			self::BBUZ_PP_HOST,
			$options
			// $wsdl = true, 
			// $proxyhost = false, 
			// $proxyport = false, 
			// $proxyusername = false, 
			// $proxypassword = false, 
			// $timeout = self::SOAP_TIMEOUT, 
			// $response_timeout = self::SOAP_RESPONSE_TIMEOUT, 
			// $portName = ''
		);
	}

	public function productList($productId = '')
	{
		$params = array(
			'product_id' => $productId
		);
		
		return $this->soap->__soapCall('productList', $params);
	}

	public function productIdByCode($msisdn, $code)
	{
		$params = array(
			'msisdn' => $msisdn,
			'code' => $code
		);
		
		return $this->soap->__soapCall('productIdByCode', $params);
	}
	
	public function inquiry($msisdn) 
	{		
		$params = array(
			'msisdn' => $msisdn
		);
		
		return $this->soap->__soapCall('inquiry', $params);
	}
	
	public function payment($productId, $msisdn, $purchaseAmount)
	{		
		$params = array(
			'product_id' => $productId,
			'msisdn' => $msisdn,
			'purchase_amount' => $purchaseAmount
		);
		
		return $this->soap->__soapCall('payment', $params);
	}

	public function checkStatus($msisdn, $trxID)
	{
		$params = array(
			'msisdn' => $msisdn,
			'trxID' => $trxID
		);
		
		return $this->soap->__soapCall('checkStatus', $params);
	}
	
	public function mitraInfo() 
	{
		return $this->soap->__soapCall('mitraInfo', array());
	}

}

?>