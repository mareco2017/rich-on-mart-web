<?php

namespace App\Helpers\External;

use SoapClient;

// require_once ("lib/nusoap.php");

class VoucherGameHelper 
{
	const BBUZ_PP_HOST	= 'http://117.102.64.238:1212/voucherGame.php?wsdl';
	const SOAP_TIMEOUT	= 60;
	const SOAP_RESPONSE_TIMEOUT = 180;
	
	// key and secret for development
	private $apiKey		= 'bd8446eaa574ab00ab72249f8b06a759';	// api key
	private $secretKey	= 'a3355920444fae8eb84b1f6304bbe1d9';	// secret key
	private $soap;
	
	public function __construct() 
	{
		$options = [
            // 'location' => self::BBUZ_PP_HOST,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'timeout' => self::SOAP_TIMEOUT,
            'response_timeout' => self::SOAP_RESPONSE_TIMEOUT,
            'login' => $this->apiKey,
            'password' => $this->secretKey
        ];

		$this->soap = new SoapClient(
			self::BBUZ_PP_HOST,
			$options
			// $wsdl = true, 
			// $proxyhost = false, 
			// $proxyport = false, 
			// $proxyusername = false, 
			// $proxypassword = false, 
			// $timeout = self::SOAP_TIMEOUT, 
			// $response_timeout = self::SOAP_RESPONSE_TIMEOUT, 
			// $portName = ''
		);
		
		// $this->soap->setCredentials($this->apiKey, $this->secretKey, 'basic');
	}

	public function mitraInfo() {
		return $this->soap->__soapCall('mitraInfo', array());
	}

	public function productList($productID = '') {
		$params = array ('productID' => $productID);
		return $this->soap->__soapCall('productListGame', $params);
	}

	public function payment($productID, $amount, $uniqueID) {
		$params = array ('productID' => $productID, 'amount' => $amount, 'uniqueID' => $uniqueID);
		return $this->soap->__soapCall('paymentVoucherGame', $params);
	}

	public function status($msisdn, $trxID) {
		$params = array ('msisdn' => $msisdn, 'trxID' => $trxID);
		return $this->soap->__soapCall('voucherGameStatus', $params);
	}
}