<?php

namespace App\Helpers\External;

use SoapClient;;

/*
	Telkom Group
	Kode Produk Telkom Group Bakoelbuzz
	TELKOMPSTN, TELKOMSPEEDY, TELKOMFLEXI, TELKOMSELLHALO
	
	No. Tester :
	0313282370 : 1 lembar tagihan
	0751675732	: 3 bulan tagihan
	HALLO
	08122739194
*/

class TelkomHelper 
{
	const BBUZ_PP_HOST	= 'http://117.102.64.238:1212/pp/index.php?wsdl';
	const SOAP_TIMEOUT	= 60;
	const SOAP_RESPONSE_TIMEOUT = 240;
	
	private $apiKey		= 'bd8446eaa574ab00ab72249f8b06a759';	// api key
	private $secretKey	= 'a3355920444fae8eb84b1f6304bbe1d9';	// secret key
	private $soap;
	
	public function __construct() 
	{
		$options = [
            // 'location' => self::BBUZ_PP_HOST,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'timeout' => self::SOAP_TIMEOUT,
            'response_timeout' => self::SOAP_RESPONSE_TIMEOUT,
            'login' => $this->apiKey,
            'password' => $this->secretKey
        ];

		$this->soap = new SoapClient(
			self::BBUZ_PP_HOST,
			$options
			// $wsdl = true, 
			// $proxyhost = false, 
			// $proxyport = false, 
			// $proxyusername = false, 
			// $proxypassword = false, 
			// $timeout = self::SOAP_TIMEOUT, 
			// $response_timeout = self::SOAP_RESPONSE_TIMEOUT, 
			// $portName = ''
		);
	}
	
	public function inquiry($productCode, $idPel, $idPel2 = '', $miscData= '') 
	{		
		$params = array(
			'productCode' => $productCode,
			'idPel'		  => $idPel,
			'idPel2'	  => $idPel2,
			'miscData'	  => $miscData
		);
		
		return $this->soap->__soapCall('ppInquiry', $params);
	}
	
	public function payment($productCode, $refID, $nominal)
	{		
		$params = array(
			'productCode' => $productCode,
			'refID'		  => $refID,
			'nominal'	  => $nominal
		);
		
		return $this->soap->__soapCall('ppPayment', $params);
	}
	
	public function mitraInfo() 
	{
		return $this->soap->__soapCall('ppMitraInfo', array());
	}

}

?>