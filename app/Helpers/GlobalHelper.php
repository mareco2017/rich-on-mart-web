<?php

namespace App\Helpers;

final class GlobalHelper {

    public static function toCurrency($amount = 0, $symbolLeft = 'Rp', $symbolRight = null, $decimal = 2)
    {
        $str = $symbolLeft . ' ' . number_format($amount, $decimal, ',', '.') . ' ' . $symbolRight;
        return $str;
    }
}

?>