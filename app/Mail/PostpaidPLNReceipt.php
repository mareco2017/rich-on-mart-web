<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Order;

class PostPaidPLNReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $options;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->options = $order->options;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@richonmart')
                ->subject('Pembayaran Tagihan PLN Berhasil')
                ->view('templates.mails.pln-pascabayar-receipt');
    }
}
