<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Managers\AppConfigManager;
use App\Models\AppConfig;
use Yajra\Datatables\Datatables;
use DB;
use Exception;

class AppConfigController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.config.index');
    }

    public function edit(Request $request, AppConfig $appConfig)
    {
        return view('admin.config.form')
            ->with('appConfig', $appConfig);
    }

    public function ajax(Request $request)
    {
    	$data = AppConfig::query();
    	return Datatables::of($data)
                ->addColumn('action', function ($model) {
                    $str = '<div class="btn-group btn-group-circle">';
                    $str .= '<a href='. route('admin.config.edit', ['appConfig' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                    $str .= '<a data-url="'. route('admin.config.delete', ['appConfig' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table" data-message="Are you sure want to delete this Config? This may cause a system <b>ERROR</b> or <b>FAILURE</b> towards this app"><span class="far fa-trash-alt"></span></a>';
                    $str .= '</div>';
                    return $str;
                })
                ->escapeColumns([])
				->make(true);
    }

    public function update(Request $request, AppConfig $appConfig)
    {
        $messageType = $appConfig->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'key' => 'required',
            'value' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $appConfigManager = new AppConfigManager;
            $updateConfig = $appConfigManager->update($appConfig, $request->all());
            DB::commit();
            return redirect()->route('admin.config.index')->with('success', 'Config successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, AppConfig $appConfig)
    {
        try {
            DB::beginTransaction();
            $appConfigManager = new AppConfigManager($request->user());
            $updatedConfig = $appConfigManager->delete($appConfig);
            DB::commit();
            return response()->json(['message' => 'Config Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}
