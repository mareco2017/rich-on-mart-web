<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;
use Validator;
use Exception;
use App\Models\Payment;
use App\Enums\PaymentType;
use GlobalHelper;
use App\Helpers\Managers\AttachmentManager;
use App\Helpers\External\PulsaHelper;

class PaymentController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = 'public';
    }

    public function index(Request $request)
    {
        return view('admin.payment.index')
            ->with('types', PaymentType::getArray());
    }

    public function external(Request $request)
    {
        return view('admin.payment.external')
            ->with('types', []);
    }

    public function ajax(Request $request)
    {
        $data = Payment::query();
        return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('type') && !empty($request->type)) {
                    $query->where('type', $request->type);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.payments.edit', ['payment' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.payments.delete', ['payment' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('icon', function ($model) {
                return $model->icon_url;
            })
            ->editColumn('type', function($model) {
                return PaymentType::getString($model->type);
            })
            ->editColumn('price', function($model) {
                return GlobalHelper::toCurrency($model->price);
            })
            ->editColumn('discount', function($model) {
                return GlobalHelper::toCurrency($model->discount);
            })
            ->editColumn('fee', function($model) {
                return GlobalHelper::toCurrency($model->fee);
            })
            ->editColumn('active', function($model) {
                if ($model->active) {
                    return '<span class="badge badge-success">Yes</span>';
                } else {
                    return '<span class="badge badge-danger">No</span>';
                }
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function externalAjax(Request $request)
    {
        $helper = new PulsaHelper;
        $data = $helper->productList();
        return Datatables::of($data)
            ->filter(function($query) use ($request) {
                // if ($request->has('type') && !empty($request->type)) {
                //     $query->where('type', $request->type);
                // }
            }, true)
            ->editColumn('harga', function($model) {
                return GlobalHelper::toCurrency($model->harga);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function edit(Request $request, Payment $payment)
    {
        return view('admin.payment.form')
            ->with('payment', $payment)
            ->with('types', PaymentType::getArray());
    }

    public function update(Request $request, Payment $payment)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'icon' => 'nullable|image',
            'code' => 'required|string',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'fee' => 'required|numeric',
            'type' => 'required|integer',
            'active' => 'nullable|boolean'
        ]);

        try {
            DB::beginTransaction();

            $payment->title = $request->title;
            $payment->code = $request->code;
            $payment->price = $request->price;
            $payment->discount = $request->discount;
            $payment->fee = $request->fee;
            $payment->type = $request->type;
            $payment->active = $request->has('active') ? $request->active : false;

            if ($request->has('icon')) {
                $oldIcon = $payment->icon;

                $attachmentManager = new AttachmentManager($this->disk);
                $attachment = $attachmentManager->create($request->icon);
                $payment->icon()->associate($attachment);
            }

            if (!$payment->save()) {
                throw new Exception('Failed while saving payment!', 500);
            }

            if (isset($oldIcon)) $attachmentManager->delete($oldIcon);

            DB::commit();
            return redirect()->route('admin.payments.index')->with('success', 'Payment successfully created/updated');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage)->withInput();
        }
    }

    public function delete(Request $request, Payment $payment)
    {
        try {
            DB::beginTransaction();

            $icon = $payment->icon;

            $payment->delete();

            if ($icon) {
                $attachmentManager = new AttachmentManager($this->disk);
                $attachmentManager->delete($icon);
            }

            DB::commit();
            if ($request->expectsJson()) {
                return response()->json(['message' => 'Payment successfully deleted'], 200);
            }
            return redirect()->route('admin.payments.index')->with('success', 'Payment successfully deleted');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            if ($request->expectsJson()) {
                return response()->json(['message' => $errorMessage], 500);
            }
            return redirect()->back()->with('error', $errorMessage)->withInput();
        }
    }
}