<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Category;
use App\Enums\ActiveStatus;
use App\Helpers\Managers\CategoryManager;
use Yajra\Datatables\Datatables;
use Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.category.index')
            ->with('statusList', $statusList);
    }

    public function edit(Request $request, Category $category)
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.category.form')
            ->with('category', $category)
            ->with('statusList', $statusList);
    }

    public function ajax(Request $request)
    {
    	$data = Category::query();
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.categories.edit', ['category' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.categories.delete', ['category' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->editColumn('status', function($model) {
                return ActiveStatus::getString($model->status);
            })
            ->make(true);
    }

    public function update(Request $request, Category $category)
    {
        $messageType = $category->id ? 'updated' : 'created';

        $rules = [
            'title' => 'required',
            'commission' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'status' => 'required|boolean',
            'cover' => 'file',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('cover', 'required', function($data) use ($category) {
            return !$category->id;
        });
        $validator->validate();

        try {
            DB::beginTransaction();
            $categoryManager = new CategoryManager;
            $updatedCategory = $categoryManager->update($category, $request->all());
            DB::commit();
            return redirect()->route('admin.categories.index')->with('success', 'Category successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage)->withInputs();
        }
    }

    public function delete(Request $request, Category $category)
    {
        try {
            DB::beginTransaction();
            $categoryManager = new CategoryManager;
            $deletedCategory = $categoryManager->delete($category);
            DB::commit();
            return response()->json(['message' => 'Category Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}