<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Order;
// use App\Models\User;
// use App\Models\Article;
// use App\Models\Passenger;
use Carbon\Carbon;
use App\Enums\OrderType;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $admin = $this->guard()->user();
        // $orders = Order::count();
        // $articles = Article::count();
        // $passengers = Passenger::count();
        // $users = User::count();

        $dates = [];
        $productOrders = [];
        $paymentOrders = [];

        $dateRange = new \DatePeriod(
            Carbon::today()->subWeek(2),
            new \DateInterval('P1D'),
            Carbon::tomorrow()
        );

        foreach ($dateRange as $value) {
            $dates[] = $value == Carbon::today() ? 'Today' : $value->format('j D');
            $productOrders[] = Order::type(OrderType::GROCERY)->whereDate('created_at', $value)->count();
            $paymentOrders[] = Order::type(OrderType::getPaymentList())->whereDate('created_at', $value)->count();
        }

        return view('admin.dashboard')->with([
            'admin' => $admin,
            // 'orders' => $orders,
            // 'articles' => $articles,
            // 'passengers' => $passengers,
            // 'users' => $users,
            'dates' => $dates,
            'productOrders' => $productOrders,
            'paymentOrders' => $paymentOrders,
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web-admin');
    }
}