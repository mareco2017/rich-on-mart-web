<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Product;
use App\Models\Category;
use App\Enums\ActiveStatus;
use App\Enums\ProductUnit;
use App\Helpers\Managers\ProductManager;
use Yajra\Datatables\Datatables;
use Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $statusList = ActiveStatus::getArray();
        $unitList = ProductUnit::getArray();
        $categories = Category::all()->pluck('title', 'id');

        return view('admin.product.index')
            ->with('statusList', $statusList)
            ->with('categories', $categories)
            ->with('unitList', $unitList);
    }

    public function edit(Request $request, Product $product)
    {
        try {
            $statusList = ActiveStatus::getArray();
            $unitList = ProductUnit::getArray();
            $categories = Category::all()->pluck('title', 'id');
            if ($categories->count() < 1) throw new Exception("Categories not found, Can't create product<br><a style='color: #ffffff !important;font-weight: 600;' href=".route('admin.categories.create').">Create Category Now</a>", 404);
            
            return view('admin.product.form')
                ->with('product', $product)
                ->with('statusList', $statusList)
                ->with('unitList', $unitList)
                ->with('categories', $categories);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function ajax(Request $request)
    {
    	$data = Product::with('category');
    	return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $query->where('status', $request->status_enum);
                }
                if ($request->has('unit') && $request->unit !== null) {
                    $query->where('unit', $request->unit);
                }
                if ($request->has('category') && $request->category !== null) {
                    $query->where('category_id', $request->category);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.products.edit', ['product' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.products.delete', ['product' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->editColumn('status', function($model) {
                return ActiveStatus::getString($model->status);
            })
            ->editColumn('unit', function($model) {
                return ProductUnit::getString($model->unit);
            })
            ->make(true);
    }

    public function update(Request $request, Product $product)
    {
        $messageType = $product->id ? 'updated' : 'created';

        $rules = [
            'title' => 'required',
            'description' => 'nullable|string',
            'price' => 'nullable|numeric',
            'discount' => 'nullable|numeric',
            'unit' => 'required',
            'category' => 'required|integer',
            'status' => 'required|boolean',
            'cover' => 'file',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('cover', 'required', function($data) use ($product) {
            return !$product->id;
        });
        $validator->validate();

        try {
            DB::beginTransaction();
            $productManager = new ProductManager;
            $updatedProduct = $productManager->update($product, $request->all());
            DB::commit();
            return redirect()->route('admin.products.index')->with('success', 'Product successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage)->withInputs();
        }
    }

    public function delete(Request $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $productManager = new ProductManager;
            $deletedProduct = $productManager->delete($product);
            DB::commit();
            return response()->json(['message' => 'Product Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}