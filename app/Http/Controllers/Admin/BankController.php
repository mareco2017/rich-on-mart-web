<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Bank;
use App\Helpers\Managers\BankManager;
use Yajra\Datatables\Datatables;
use Validator;

class BankController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.bank.index');
    }

    public function edit(Request $request, Bank $bank)
    {
        return view('admin.bank.form')
            ->with('bank', $bank);
    }

    public function ajax(Request $request)
    {
    	$data = Bank::query();
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.bank.edit', ['bank' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.bank.delete', ['bank' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Bank $bank)
    {
        $messageType = $bank->id ? 'updated' : 'created';
        
        $rules = [
            'name' => 'required',
            'code' => 'required',
            'abbr' => 'required',
            'logo' => 'nullable'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('logo', 'required', function($data) use ($bank) {
            return !$bank->id;
        });
        $validator->validate();
        try {
            DB::beginTransaction();
            $bankManager = new BankManager;
            $updatedBank = $bankManager->update($bank, $request->all());
            DB::commit();
            return redirect()->route('admin.bank.index')->with('success', 'Bank successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            dd($errorMessage);
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Bank $bank)
    {
        try {
            DB::beginTransaction();
            $bankManager = new BankManager;
            $updatedBank = $bankManager->delete($bank);
            DB::commit();
            return response()->json(['message' => 'Bank Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}