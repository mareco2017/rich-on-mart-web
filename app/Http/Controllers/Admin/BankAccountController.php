<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Bank;
use App\Models\Admin;
use App\Models\BankAccount;
use App\Helpers\Managers\BankManager;
use Yajra\Datatables\Datatables;

class BankAccountController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.bank-account.index');
    }

    public function edit(Request $request, BankAccount $bankAccount)
    {
        $bankList = Bank::all()->pluck('name', 'id');
        $adminList = Admin::all()->pluck('fullname', 'id');

        return view('admin.bank-account.form')
            ->with('bankList', $bankList)
            ->with('adminList', $adminList)
            ->with('bankAccount', $bankAccount);
    }

    public function ajax(Request $request)
    {
    	$data = BankAccount::with('bank');
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.bank-account.edit', ['bankAccount' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.bank-account.delete', ['bankAccount' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('owner', function($model) {
                return $model->ownable ? $model->ownable->fullname : '';
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, BankAccount $bankAccount)
    {
        $messageType = $bankAccount->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'account_name' => 'required',
            'account_no' => 'required|numeric',
            'bank_id' => 'required',
            'is_primary' => 'required|boolean',
            'admin_id' => 'nullable',
            'user_id' => 'nullable',
        ]);

        try {
            DB::beginTransaction();
            if ($request->admin_id) {
                $owner = Admin::find($request->admin_id);
            } else if ($request->user_id) {
                $owner = User::find($request->user_id);
            } else {
                throw new Exception('Oops something went wrong', 500);
            }

            $bankManager = new BankManager($owner);
            $updatedBank = $bankManager->updateBankAccount($bankAccount, $request->all());
            // dd($updatedBank);
            DB::commit();
            return redirect()->route('admin.bank-account.index')->with('success', 'Bank successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, BankAccount $bankAccount)
    {
        try {
            DB::beginTransaction();
            $bankManager = new BankManager($request->user());
            $deleteBankAccount = $bankManager->deleteBankAccount($bankAccount);
            DB::commit();
            return response()->json(['message' => 'Bank Account Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}