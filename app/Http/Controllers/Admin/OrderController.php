<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Enums\OrderType;
use App\Enums\OrderStatus;
use App\Helpers\Managers\OrderManager;
use Yajra\Datatables\Datatables;
use DB;
use Exception;
use Carbon\Carbon;
use App\Helpers\GlobalHelper;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $statusList = OrderStatus::getArray();
        $typeList = OrderType::getArray();

        return view('admin.order.index')
            ->with('statusList', $statusList)
            ->with('typeList', $typeList);
    }

    public function edit(Request $request, Order $order)
    {
        return view('admin.order.form')
            ->with('order', $order);
    }

    public function ajax(Request $request)
    {
    	$data = Order::query();
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('start_date') || $request->has('end_date')) {
                    $date1 = $request->start_date ? Carbon::parse($request->start_date)->timezone('Asia/Jakarta') : null;
                    $date2 = $request->end_date ? Carbon::parse($request->end_date)->timezone('Asia/Jakarta')->endOfDay() : null;
                    
                    if ($date1 && $date2) {
                        $model->whereBetween('created_at', [$date1->setTimezone('UTC'), $date2->setTimezone('UTC')]);
                    } else if ($date1 && !$date2) {
                        $model->whereDate('created_at', '>=', $date1->setTimezone('UTC'));
                    } else if (!$date1 && $date2) {
                        $model->where('created_at', '<=', $date2->setTimezone('UTC'));
                    }
                }
                if ($request->has('type') && $request->type !== null) {
                    $model->where('type', $request->type);
                }
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                }
                if ($request->has('show_active') && $request->show_active == 'true') {
                    $s = [
                        OrderStatus::PENDING,
                        OrderStatus::WAITING_FOR_PAYMENT,
                        OrderStatus::ONGOING,
                        OrderStatus::PAYMENT_CONFIRMED,
                        OrderStatus::PURCHASING_ITEM,
                        OrderStatus::ON_THE_WAY,
                    ];
                    $model->whereIn('status', $s);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.order.detail', $model) .' class="action"><span class="far fa-eye"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->editColumn('total', function ($model) {
                return GlobalHelper::toCurrency($model->total);
            })
            ->editColumn('type', function ($model) {
                return OrderType::getString($model->type);
            })
            ->editColumn('status', function ($model) {
                return OrderStatus::getString($model->status);
            })
            ->editColumn('created_at', function ($model) {
                return $model->created_at->setTimezone('Asia/Jakarta')->format('l, F d, Y H:i A');
            })
            ->addColumn('owner', function ($model) {
                return $model->ownable ? $model->ownable->fullname : '-';
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function detail(Request $request, Order $order)
    {
        $statusList = OrderStatus::getArray();

        return view('admin.order.detail')
            ->with('statusList', $statusList)
            ->with('order', $order);
    }

    public function update(Request $request, Order $order)
    {
        $messageType = $order->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'key' => 'required',
            'value' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $orderManager = new orderManager;
            $updatedOrder = $orderManager->update($order, $request->all());
            DB::commit();
            return redirect()->route('admin.order.index')->with('success', 'Order successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function setStatus(Request $request, Order $order, $status)
    {
        try {
            DB::beginTransaction();
            $orderManager = new orderManager;
            $updatedOrder = $orderManager->updateStatus($order, $status);
            DB::commit();
            return redirect()->route('admin.order.detail', $order)->with('success', 'Status Updated');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Order $order)
    {
        try {
            DB::beginTransaction();
            $orderManager = new orderManager($request->user());
            $updatedOrder = $orderManager->delete($order);
            DB::commit();
            return response()->json(['message' => 'Order Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}
