<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Admin;
// use App\Helpers\Managers\AdminManager;
use Yajra\Datatables\Datatables;
use Validator;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.admin.index');
    }

    public function edit(Request $request, Admin $admin)
    {
        // return view('admin.admins.admin.form')
        //     ->with('admin', $admin);
    }

    // public function detail(Request $request, User $user)
    // {

    // }

    public function ajax(Request $request)
    {
    	$data = Admin::query();
    	return Datatables::of($data)
            // ->addColumn('action', function ($model) {
            //     $str = '<div class="btn-group btn-group-circle">';
            //     $str .= '<a href='. route('admin.admins.admin.edit', ['user' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
            //     $str .= '<a data-url="'. route('admin.admins.admin.delete', ['user' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
            //     $str .= '</div>';
            //     return $str;
            // })
            // ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Admin $admin)
    {
        // $messageType = $admin->id ? 'updated' : 'created';

        // $rules = [
        //     'first_name' => 'required',
        //     'last_name' => 'nullable',
        //     'phone_number' => 'required|phone:AUTO,ID|unique:admins,phone_number,'. $admin->id,
        //     'email' => 'required|email|unique:admins,email,'. $admin->id,
        //     'password' => 'nullable',
        // ];
        // $validator = Validator::make($request->all(), $rules);
        // $validator->sometimes('password', 'required', function($data) use ($admin) {
        //     return !$admin->id;
        // });
        // $validator->validate();

        // try {
        //     DB::beginTransaction();
        //     $adminManager = new AdminManager($request->user());
        //     if (!$role = Role::find(3)) throw new Exception("Role not found", 500);
        //     $updatedAdmin = $adminManager->create($admin, $role, $request->all());
        //     DB::commit();
        //     return redirect()->route('admin.admins.admin.index')->with('success', 'Admin successfully '.$messageType);
        // } catch (Exception $e) {
        //     DB::rollBack();
        //     $errorMessage = $e->getMessage();
        //     return redirect()->back()->with('error', $errorMessage);
        // }
    }

    public function delete(Request $request, Admin $admin)
    {
        // try {
        //     DB::beginTransaction();
        //     $adminManager = new AdminManager($request->user());
        //     $updatedAdmin = $adminManager->delete($admin);
        //     DB::commit();
        //     return response()->json(['message' => 'Admin Deleted'], 200);
        // } catch (Exception $e) {
        //     DB::rollBack();
        //     $errorMessage = $e->getMessage();
        //     return response()->json(['message' => $errorMessage], 500);
        // }
    }
}