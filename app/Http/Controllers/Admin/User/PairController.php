<?php

namespace App\Http\Controllers\Admin\User;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Pair;
use App\Models\User;
use App\Helpers\Managers\PairManager;
use App\Helpers\Enums\PairClaim;
use App\Helpers\Enums\PackageType;
use DataTables;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class PairController extends Controller
{
    public function index(Request $request)
    {
        $statusList = PairClaim::getArray();
        $packageList = PackageType::getArray();

        return view('admin.users.pair.index')
            ->with('statusList', $statusList)
            ->with('packageList', $packageList);
    }

    public function ajax(Request $request)
    {
        $data = Pair::whereNull('claimed_at')->with('leaderUser', 'leftBinaryUser', 'rightBinaryUser')->select('pairs.*');
    	return DataTables::eloquent($data)
            ->filter(function($model) use ($request) {
                if ($request->has('dates') && $request->dates !== null) {
                    $date1 = $request->dates[0];
                    $date2 = new Carbon($request->dates[1].'23:59:59');
                    
                    $model->whereBetween('created_at', [$date1, $date2]);
                }
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('is_leader_claimed', $request->status_enum);
                }
                if ($request->has('package_enum') && $request->package_enum !== null) {
                    $model->where('leader_package_type', $request->package_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                if (!$model->is_leader_claimed) $str .= '<a data-url="'. route('admin.users.pair.claim', ['pair' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table" data-method="POST" data-message="Are you sure to <b>CLAIM</b> this pair?"><span class="fas fa-check"></span></a>';
                $str .= '<a data-url="'. route('admin.users.pair.delete', ['pair' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('package', function ($model) {
                return PackageType::getString($model->leader_package_type);
            })
            ->addColumn('claimed', function ($model) {
                $str = PairClaim::getString($model->is_leader_claimed);
                // if ($model->is_leader_claimed) $str .= ' ('.$model->claimed_at->format('Y-m-d').')';
                return $str;
            })
            ->addColumn('total', function ($model) {
                return number_format(PackageType::getPairingBonus($model->leader_package_type),0,',','.');
            })
            ->editColumn('created_at', function ($model) {
                return $model->created_at->format('j-M-Y');
            })
            ->make(true);
    }

    public function update(Request $request, Pair $pair)
    {
        $validatedData = $request->validate([
            'leader_user_id' => 'required|numeric',
            'left_binary_user_id' => 'required|numeric',
            'right_binary_user_id' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();
            $pairManager = new PairManager;
            $updatedPair = $pairManager->update($pair, $request->all());
            DB::commit();
            return redirect()->route('admin.users.pair.index')->with('success', 'Pair successfully created');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function claim(Request $request, Pair $pair)
    {
        try {
            DB::beginTransaction();
            $pairManager = new PairManager;
            $updatedPair = $pairManager->claim($request, $pair);
            DB::commit();
            return response()->json(['message' => 'Pair successfully Claimed'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }

    public function delete(Request $request, Pair $pair)
    {
        try {
            DB::beginTransaction();
            $pairManager = new PairManager($pair);
            $updatedPair = $pairManager->delete($pair);
            DB::commit();
            return response()->json(['message' => 'Pair successfully Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}