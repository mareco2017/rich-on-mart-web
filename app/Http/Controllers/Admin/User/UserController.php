<?php

namespace App\Http\Controllers\Admin\User;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Enums\WalletType;
use App\Helpers\GlobalHelper;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Role;
use App\Helpers\Managers\UserManager;
use Yajra\Datatables\Datatables;
use Validator;
use App\Enums\PackageType;

class UserController extends Controller
{
    public function index(Request $request)
    {
        // $packageList = PackageType::getArray();
        $floatingWallet = Wallet::where('ownable_type', 'users')->where('type', 0)->sum('balance');
        return view('admin.users.user.index')
            ->with('floatingWallet', $floatingWallet);
            // ->with('packageList', $packageList);
    }

    public function edit(Request $request, User $user)
    {
        return view('admin.users.user.form')
            ->with('user', $user);
    }

    // public function detail(Request $request, User $user)
    // {

    // }

    public function ajax(Request $request)
    {
    	$data = User::query();
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('package_enum') && $request->package_enum !== null) {
                    $model->where('packages', $request->package_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.users.user.detail', ['user' => $model->id]) .' class="action"><span class="far fa-eye"></span></a>';
                // $str .= '<a href='. route('admin.users.user.edit', ['user' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                // $str .= '<a data-url="'. route('admin.users.user.delete', ['user' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('balance', function($model) {
                return $model->getWallet(WalletType::PRIMARY) ? GlobalHelper::toCurrency($model->getWallet(WalletType::PRIMARY)->balance) : 'Not Initialized';
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, User $user)
    {
        $messageType = $user->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'packages' => 'required|numeric',
        ]);

        try {
            DB::beginTransaction();
            $user->packages = $request->packages;

            if (!$user->save()) {
                throw new Exception('Failed create user!', 500);
            }
            DB::commit();
            return redirect()->route('admin.users.user.index')->with('success', 'Admin successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, User $user)
    {
        try {
            DB::beginTransaction();
            $userManager = new UserManager($request->user());
            $updatedUser = $userManager->delete($user);
            DB::commit();
            return response()->json(['message' => 'User Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }

    public function detail(Request $request, User $user)
    {
        $packageList = PackageType::getArray();
        return view('admin.users.user.detail')
            ->with('user', $user)
            ->with('packageList', $packageList);
    }
}