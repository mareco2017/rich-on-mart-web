<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Promo;
use App\Traits\ApiResponse;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class HomeController extends Controller
{
    use ApiResponse;

    public function index(Request $request)
    {
    	$categories = Category::active()->orderBy('title','asc')->get();
    	$products = Product::active()->orderBy('created_at')->get();
        $banners = Promo::banner()->active()->orderBy('created_at')->get();
        return $this->jsonResponse("Success", array('banners' => $banners, 'products' => $products, 'categories' => $categories,'version' => '0.0.1'));
    }

    public function productByCategory(Request $request)
    {
    	$products = Product::where('category_id','=', $request->category)->active()->get();
        return $this->jsonResponse("Success", array('products' => $products));
    }
    
    public function productSearch(Request $request)
    {
        $name = $request->name;
        $category = $request->category;
        $offset = $request->offset;
        $limit  = $request->limit;
        $products = Product::where('title','LIKE', '%' . $name . '%')->active();
        if ($category) $products->where('category_id','=', $request->category);
        $products = $products->orderBy('title','asc')->get();
        return $this->jsonResponse("Success", array('products' => $products));
    }
}