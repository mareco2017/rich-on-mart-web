<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Helpers\GlobalHelper;
use App\Helpers\Managers\GroceryManager;
use App\Helpers\Unique;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\AppConfig;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Events\NewOrderEvent;

class GroceryController extends Controller
{
    use ApiResponse;

    public function processCheckout(Request $request)
    {
        $validatedData = $request->validate([
            'delivery_type' => 'required',
            'user_address_id' => 'required',
        ]);

        try {
            $user = $request->user();
            $deliveryType = $request->delivery_type;
            $userAddress = $request->user_address_id;
            $totalPayment = $request->total_payment;
            $products = $request->products;
            $activeCheckoutOrder = $user->orders()->active()->type(OrderType::GROCERY)->first();
            if ($activeCheckoutOrder) {
                $activeCheckoutOrder->status = OrderStatus::EXPIRED;
                $activeCheckoutOrder->save();
            }
            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            DB::beginTransaction();
            $groceryManager = new GroceryManager($user);
            $order = $groceryManager->create($referable, (int) $deliveryType, (int) $userAddress, $products);
            DB::commit();
            broadcast(new NewOrderEvent($order));

            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function checkout(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = $request->user();
            $order = Order::find($request->order_id);
            if (!$order) throw new Exception('No order binded! ', 400);
            if ($order->status != OrderStatus::WAITING_FOR_PAYMENT) throw new Exception('Orderan ini sudah selesai, silahkan ulang order kembali. ', 400);
            $order->status = OrderStatus::PAYMENT_CONFIRMED;
            $order->save();
            DB::commit();

            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}
