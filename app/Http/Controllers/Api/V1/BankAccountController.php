<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\AppConfig;
use App\Models\BankAccount;
use App\Models\Bank;
use App\Models\Admin;
use App\Helpers\Managers\BankManager;

class BankAccountController extends Controller
{
    use ApiResponse;

    public function list(Request $request)
    {
        try {
            $user = $request->user();
            $bankAccounts = $user->bankAccounts->load('bank') ?? [];

            return $this->jsonResponse("Success", array('bank_accounts' => $bankAccounts), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function listPayment(Request $request)
    {
        try {
            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $morphClass = (new Admin)->getMorphClass();
            // $wallet = Admin::find($id);
            $bankAccounts = BankAccount::where('ownable_type', $morphClass)->where('ownable_id', $id)->whereNotNull('bank_id')->with('bank')->get();

            return $this->jsonResponse("Success", array('bank_accounts' => $bankAccounts), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'bank_id' => 'required|exists:banks,id',
            'account_name' => 'required|string|alpha_spaces',
            'account_no' => 'required|numeric',
            'is_primary' => 'nullable|in:0,1'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $exists = $user->bankAccounts()->where('account_no', $request->account_no)->where('bank_id', $request->bank_id)->exists();
            if ($exists) {
                return $this->jsonResponse("Account number already exists!", [], 400);
            }
            $bankManager = new BankManager($user);
            $data = $request->only('bank_id', 'account_name', 'account_no', 'is_primary');
            $bankAccount = $bankManager->updateBankAccount(new BankAccount, $data);

            DB::commit();
            return $this->jsonResponse("Success", array('bank_account' => $bankAccount->fresh()), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function remove(Request $request, BankAccount $bankAccount)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();
            $bankManager = new BankManager($user);
            $deletedBankAccount = $bankManager->deleteBankAccount($bankAccount);

            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function setPrimary(Request $request, BankAccount $bankAccount)
    {
    	try {
    		$user = $request->user();

            DB::beginTransaction();
    		$bankManager = new BankManager($user);
    		$primaryBankAccount = $bankManager->setBankAccountAsPrimary($bankAccount);

            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
    	} catch (Exception $e) {
    		DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
    	}
    }
}