<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\Category;

class CategoryController extends Controller
{
    use ApiResponse;

    public function list(Request $request)
    {
        try {
            $categories = Category::active()->get();
            return $this->jsonResponse("Success", array('categories' => $categories), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }
}