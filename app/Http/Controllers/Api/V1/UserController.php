<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Models\UserToken;
use App\Enums\UserVerificationType;
use App\Enums\UserDocumentType;
use App\Helpers\Managers\WalletManager;
use App\Helpers\Managers\DocumentManager;

class UserController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = $this->guard()->user();
        $user->load('wallets');
        return $this->jsonResponse("Success", ['user' => $user]);
    }

    public function updateProfile(Request $request)
    {
        $user = $this->guard()->user();

        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'email' => 'required|email|unique:users,email,' . $user->id,
            'first_name' => 'required|string|alpha_spaces',
            'last_name' => 'nullable|string|alpha_spaces',
            'password' => 'required',
            // 'gender' => 'nullable',
            // 'dob' => 'nullable|date_format:Y-m-d'
        ]);

        try {
            DB::beginTransaction();
            
            $user->phone_number = $request->phone_number;
            $user->email = $request->email;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            // $user->gender = $request->gender;
            // $user->dob = $request->dob;

            if (!Hash::check($request->password, $user->password)) {
                throw new Exception("Wrong password!", 400);
            }

            if ($user->isDirty('phone_number')){
                $phoneNumberV = $user->verifications()->type(UserVerificationType::PHONE_NUMBER)->latest()->first();

                if ($phoneNumberV) {
                    $phoneNumberV->delete();
                    $user->phone_number_verified_at = null;
                }
            }

            if ($user->isDirty('email')){
                $emailV = $user->verifications()->type(UserVerificationType::EMAIL)->latest()->first();

                if ($emailV) {
                    $emailV->delete();
                    $user->email_verified_at = null;
                }
            }

            if (!$user->save()) {
                throw new Exception("Failed while updating profile!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Successfully updated profile!", array('user' => $user));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required',
            'new_password' => 'required|min:6|confirmed',
            'new_password_confirmation' => 'required',
        ]);

        try {
            $user = $this->guard()->user();

            if (!Hash::check($request->password, $user->password)) {
                throw new Exception("Wrong password!", 400);
            }
            
            DB::beginTransaction();
            
            $user->password = bcrypt($request->new_password);

            if (!$user->save()) {
                throw new Exception("Failed while changing password!", 500);
            }

            DB::commit();

            // note: need logout?

            return $this->jsonResponse("Successfully change password!");
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function uploadIdentity(Request $request)
    {
        $validatedData = $request->validate([
            'nric_picture' => 'required|image',
            'selfie_picture' => 'required|image'
        ]);

        try {
            $user = $request->user();

            $activeNRIC = $user->documents()->type(UserDocumentType::NRIC)->first();
            if ($activeNRIC && !$activeNRIC->is_declined) throw new Exception("You have uploaded your NRIC picture!", 400);

            $activeSelfie = $user->documents()->type(UserDocumentType::SELFIE)->first();
            if ($activeSelfie && !$activeSelfie->is_declined) throw new Exception("You have uploaded your Selfie picture!", 400);

            DB::beginTransaction();
            $documentManager = new DocumentManager($user);
            $nricData = [
                'attachment' => $request->nric_picture,
                'description' => ''
            ];

            $nric = $documentManager->create(UserDocumentType::NRIC, $nricData);
            $nric->setRelations([]);

            $selfieData = [
                'attachment' => $request->selfie_picture,
                'description' => ''
            ];

            $selfie = $documentManager->create(UserDocumentType::SELFIE, $selfieData);
            $selfie->setRelations([]);
            // $history = HistoryLogger::create($order, $user, $order->status);
            DB::commit();

            return $this->jsonResponse("Success", ['nric' => $nric, 'selfie' => $selfie], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function initializeWallets(Request $request)
    {
        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $walletManager = new WalletManager;
            $initWallets = $walletManager->initializeWallets($user);

            $user->load('wallets');

            DB::commit();

            return $this->jsonResponse("Success", ['user' => $user], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}