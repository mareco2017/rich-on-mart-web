<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\User;
use App\Models\UserAddress;

class UserAddressController extends Controller
{
    use ApiResponse;

    public function list(Request $request)
    {
        try {
            $user = $request->user();
            $addresses = $user->addresses ?? [];

            return $this->jsonResponse("Success", array('addresses' => $addresses), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'recipient_name' => 'required|string',
            'phone_number' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
            'sub_district' => 'required|string',
            'postal_code' => 'required|string',
            'full_address' => 'required|string',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
            'is_primary' => 'nullable|in:0,1'
        ]);

        try {
            $user = $request->user();
            $hasAddress = $user->addresses()->exists();

            DB::beginTransaction();

            $address = new UserAddress;
            $address->name = $request->name;
            $address->recipient_name = $request->recipient_name;
            $address->phone_number = $request->phone_number;
            $address->province = $request->province;
            $address->city = $request->city;
            $address->sub_district = $request->sub_district;
            $address->postal_code = $request->postal_code;
            $address->full_address = $request->full_address;
            $address->lat = $request->lat;
            $address->long = $request->long;
            $address->is_primary = $hasAddress ? false : true;
            $address->user()->associate($user);

            if (!$address->save()) {
                throw new Exception('Failed to create user address!', 500);
            }

            DB::commit();
            return $this->jsonResponse("Success", array('address' => $address), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function remove(Request $request, UserAddress $address)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();
            
            $address->delete();

            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function setPrimary(Request $request, UserAddress $address)
    {
    	try {
    		$user = $request->user();

            DB::beginTransaction();

    		$user->addresses()->where('is_primary', 1)->update(['is_primary' => 0]);
        
            $address->is_primary = 1;

            if (!$address->save()) {
                throw new Exception('Failed to set user address as primary!', 500);
            }

            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
    	} catch (Exception $e) {
    		DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
    	}
    }
}