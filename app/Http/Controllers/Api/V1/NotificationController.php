<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Helpers\Managers\NotificationManager;
use App\Models\Notification;

class NotificationController extends Controller
{
    use ApiResponse;

    public function list(Request $request)
    {
        try {
            $limit = $request->limit ?? 10;
            $user = $request->user();
            $query = Notification::own($user)->with('sendable', 'receivable');

            $unread = Notification::own($user)->unread()->count();

            if ($unread > $limit) {
                $limit = $unread;
            }
            $query = $query->take($limit);

            // $query->with('sendable');
            $notifications = $query->orderBy('created_at','desc')->get();
            foreach ($notifications as $key => $notification) {
                if ($notification['read'] == 0)
                {
                    $notification['read'] = false; 
                } else {
                    $notification['read'] = true;
                }
            }
            
            $notificationManager = new NotificationManager($user);
            $read = $notificationManager->read($notifications->pluck('id'));

            return $this->JsonResponse('Success', ['notifications' => $notifications], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}