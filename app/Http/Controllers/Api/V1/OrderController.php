<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\AppConfig;
use App\Models\Admin;
use App\Models\Payment;
use App\Models\Order;
use App\Enums\PaymentType;
use App\Enums\OrderType;

class OrderController extends Controller
{
    use ApiResponse;

    public function owned(Request $request)
    {
        $typeList = implode(',', OrderType::getList());

        $validatedData = $request->validate([
            'type' => 'nullable|string|regex:/^\d+(,\d+)*$/',
            'status' => 'nullable|string|regex:/^\d+(,\d+)*$/',
            'offset' => 'required|numeric',
            'limit' => 'required|numeric'
        ]);

        try {
            $user = $request->user();
            
            $types = explode(",", $request->type);
            $statuses = explode(",", $request->status);

            $query = $user->orders();

            if ($types) {
                $query->type($types);
            }
            if ($statuses) {
                $query->status($statuses);
            }

            $orders = $query->latest()
                            ->with('orderDetails.productable')
                            ->offset($request->offset)
                            ->limit($request->limit)
                            ->get();

            return $this->jsonResponse("Success", array('orders' => $orders), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }
}