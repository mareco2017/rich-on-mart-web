<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Carbon\Carbon;
use Exception;
use App\Models\User;
use App\Models\UserToken;
use App\Models\UserVerification;
use App\Enums\UserVerificationType;
use App\Enums\UserVerificationStatus;
use App\Helpers\Unique;

class AuthController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            // 'password' => 'required|min:6|confirmed',
            // 'password_confirmation' => 'required',
            'first_name' => 'required|string|alpha_spaces',
            'last_name' => 'nullable|string|alpha_spaces',
            // 'gender' => 'nullable',
            // 'dob' => 'nullable|date_format:Y-m-d'
        ]);

        try {
            DB::beginTransaction();
            $user = new User;
            $user->phone_number = $request->phone_number;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            // $user->gender = $request->gender;
            // $user->dob = $request->dob;

            if (!$user->save()) {
                throw new Exception("Failed while registering user!", 500);
            }
            DB::commit();

            $token = $this->loginAndCreateToken($request, $user);
            $loggedInUser = $this->guard()->user();
            $verification = $this->sendVerification($user, UserVerificationType::PHONE_NUMBER, $user->phone_number);

            return $this->jsonResponse("Successfully registering user!", ['token' => $token->token, 'user' => $loggedInUser]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function resend(Request $request)
    {
        $userVerificationTypes = implode(",", UserVerificationType::getList());
        $validatedData = $request->validate([
            'type' => 'required|numeric|in:' . $userVerificationTypes
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            if ($request->type == UserVerificationType::PHONE_NUMBER) {
                if ($user->phone_number_verified) throw new Exception("Phone number is verified!", 400);
            } else if ($request->type == UserVerificationType::EMAIL) {
                if ($user->email_verified) throw new Exception("Email is verified!", 400);
            }
            
            $olds = $user->verifications()->type($request->type)->pending()->get();

            UserVerification::destroy($olds->toArray());

            $v = $this->sendVerification($user, $request->type);

            DB::commit();

            return $this->jsonResponse("Success", ['user' => $user->fresh()]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function verify(Request $request)
    {
        $userVerificationTypes = implode(",", UserVerificationType::getList());
        $validatedData = $request->validate([
            'type' => 'required|numeric|in:' . $userVerificationTypes,
            'code' => 'required|string',
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();
            
            $v = $user->verifications()->type($request->type)->pending()->first();
            $now = Carbon::now();

            if (!$v) throw new Exception("Something went wrong, please try again.", 400);

            if ($now->greaterThan($v->expired_at)) {
                \Log::info($now);
                \Log::info($v->expired_at);
                throw new Exception("Invalid or expired code", 400);
            }

            if ($v->code != $request->code) {
                throw new Exception("Invalid or expired code", 400);
            }

            $v->status = UserVerificationStatus::VERIFIED;

            if (!$v->save()) {
                throw new Exception("Failed while updating user verification!", 500);
            }

            if ($v->type == UserVerificationType::PHONE_NUMBER) {
                $user->phone_number_verified_at = Carbon::now();
            } else if ($v->type == UserVerificationType::EMAIL) {
                $user->email_verified_at = Carbon::now();
            }

            if (!$user->save()) {
                throw new Exception("Failed while updating user!", 500);
            }

            DB::commit();

            return $this->jsonResponse("Successfully verifying!", ['user' => $user->fresh()]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        try {
            $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
            $request->merge([$field => $request->input('username')]);
            
            $credentials = $request->only($field, 'password');

            if (!$tokenString = $this->guard()->attempt($credentials)) {
                throw new Exception("Wrong username or password!", 400);
            }

            $loggedInUser = $this->guard()->user();
            $token = $this->loginAndCreateToken($request, $loggedInUser, $tokenString);
            
            return $this->jsonResponse("Successfully logged in!", ['token' => $token->token, 'user' => $loggedInUser]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function socialLogin(Request $request, $type)
    {
        $validatedData = $request->validate([
            'access_token' => 'required'
        ]);

        try {
            switch ($type) {
                case 'google':
                    $user = $this->loginWithGoogle($request);
                    break;
                case 'facebook':
                    $user = $this->loginWithFacebook($request);
                    break;
                default:
                    throw new Exception("Login type not supported yet.", 400);
                    break;
            }

            $token = $this->loginAndCreateToken($request, $user);
            $loggedInUser = $this->guard()->user();
            
            return $this->jsonResponse("Successfully logged in with {$type}!", ['token' => $token->token, 'user' => $loggedInUser]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function loginWithGoogle(Request $request)
    {
        try {
            $client = new \Google_Client(['client_id' => config('social.google.client_id')]);
            $payload = $client->verifyIdToken($request->access_token);
            if ($payload) {
                $user = User::where('google->sub', $payload['sub'])->first();
                if (!$user) {
                    // Check duplicate email
                    if (isset($payload['email']) && $this->checkEmailAvailability($payload['email'])) {
                        throw new Exception("Email already used!", 400);
                    }
                    DB::beginTransaction();
                    $user = new User;
                    $user->phone_number = null;
                    $user->email = isset($payload['email']) ? $payload['email'] : null;
                    $user->first_name = isset($payload['given_name']) ? $payload['given_name'] : '';
                    $user->last_name = isset($payload['family_name']) ? $payload['family_name'] : null;
                    // $user->gender = null;
                    // $user->dob = null;
                    $user->google = $payload;

                    if (!$user->save()) {
                        throw new Exception("Failed while registering user!", 500);
                    }

                    DB::commit();
                } else {
                    $user->google = $payload;

                    if (!$user->save()) {
                        throw new Exception("Failed while updating user!", 500);
                    }
                }
                
                // If request specified a G Suite domain:
                // $domain = $payload['hd'];
            } else {
                throw new Exception("Invalid google id token!", 500);
                // Invalid ID token
            }
            
            return $user;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    public function loginWithFacebook(Request $request)
    {
        try {
            $fb = new \Facebook\Facebook([
                'app_id' => config('social.facebook.app_id'),
                'app_secret' => config('social.facebook.app_secret'),
                'default_graph_version' => 'v3.1',
                //'default_access_token' => '{access-token}', // optional
            ]);

            // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
            //   $helper = $fb->getRedirectLoginHelper();
            //   $helper = $fb->getJavaScriptHelper();
            //   $helper = $fb->getCanvasHelper();
            //   $helper = $fb->getPageTabHelper();

            try {
                // Get the \Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
                $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $request->access_token);
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                throw new Exception('Graph returned an error: ' . $e->getMessage(), 500);
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                throw new Exception('Facebook SDK returned an error: ' . $e->getMessage(), 500);
                exit;
            }

            $me = $response->getGraphUser();
            $graphNode = $response->getGraphNode();
            if ($me) {
                $user = User::where('facebook->id', $me->getId())->first();
                if (!$user) {
                    // Check duplicate email
                    if ($this->checkEmailAvailability($me->getEmail())) {
                        throw new Exception("Email already used!", 400);
                    }
                    DB::beginTransaction();
                    $user = new User;
                    $user->phone_number = null;
                    $user->email = $me->getEmail();
                    $user->first_name = $me->getFirstName();
                    $user->last_name = $me->getLastName();
                    // $user->gender = null;
                    // $user->dob = $me->getBirthday();
                    $user->facebook = (object) $graphNode->asArray();

                    if (!$user->save()) {
                        throw new Exception("Failed while registering user!", 500);
                    }

                    DB::commit();
                } else {
                    $user->facebook = (object) $graphNode->asArray();

                    if (!$user->save()) {
                        throw new Exception("Failed while updating user!", 500);
                    }
                }
                
                // If request specified a G Suite domain:
                // $domain = $payload['hd'];
            } else {
                throw new Exception("Invalid facebook access token!", 500);
                // Invalid ID token
            }
            
            return $user;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    public function loginAndCreateToken(Request $request, $user, $tokenString = null)
    {
        try {
            DB::beginTransaction();
            if (!$tokenString) {
                $tokenString = $this->guard()->login($user);
            }
            $payload = $this->guard()->payload();
            $exp = $payload->get('exp');
            $expiredAt = date("Y-m-d H:i:s", $exp);
            $token = new UserToken;
            $token->token = $tokenString;
            $token->type = 'api';
            $token->device_type = $request->header('Device-Type') ?: $request->header('User-Agent');
            $token->ip_address = null;
            $token->device_token = $request->header('Device-Token');
            $token->user()->associate($user);
            $token->expired_at = $expiredAt;

            if (!$token->save()) {
                throw new Exception("Failed while saving token!", 500);
            }
            DB::commit();

            return $token;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    public function sendVerification(User $user, $type = UserVerificationType::PHONE_NUMBER, $content = null)
    {
        try {
            if (empty($content)) {
                switch ($type) {
                    case UserVerificationType::PHONE_NUMBER:
                        $content = $user->phone_number;
                        break;
                    case UserVerificationType::EMAIL:
                        $content = $user->email;
                        break;
                    default:
                        break;
                }
            }

            DB::beginTransaction();
            $v = new UserVerification;
            $v->type = $type;
            $v->content = $content;
            $v->code = Unique::generateNumber(4, UserVerification::class, "code", null, [["status", UserVerificationStatus::PENDING]]);
            $v->status = UserVerificationStatus::PENDING;
            $v->user()->associate($user);
            $v->expired_at = Carbon::now()->addMinutes(5);;

            if (!$v->save()) {
                throw new Exception("Failed while saving user verification!", 500);
            }
            DB::commit();

            $this->sendCode($v);

            return $v;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        return $this->jsonResponse("Successfully logged out");
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        try {
            $tokenString = $this->guard()->refresh();
            $this->guard()->setToken($tokenString);

            $loggedInUser = $this->guard()->user();
            $token = $this->loginAndCreateToken($request, $loggedInUser, $tokenString);

            return $this->jsonResponse("Successfully refreshed!", ['token' => $token->token, 'user' => $loggedInUser]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function checkExists(Request $request)
    {
        $validatedData = $request->validate([
            'field' => 'required',
            'value' => 'required'
        ]);

        $id = $this->guard()->id();
        
        try {
            $exists = User::where($request->field, $request->value)->where('id', '!=', $id)->exists();

            return $this->jsonResponse("Success", array('exists' => $exists));            
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function sendCode(UserVerification $v)
    {
        try {
            \Log::info('type ' . $v->type);
            \Log::info('send to ' . $v->content);
            \Log::info('code ' . $v->code);
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function checkEmailAvailability($email)
    {
        return User::where('email', $email)->exists();
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->jsonResponse("Successfully logged in", [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}