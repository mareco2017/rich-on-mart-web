<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Helpers\GlobalHelper;
use App\Helpers\Unique;
use App\Models\Admin;
use App\Models\AppConfig;
use App\Models\Order;
use App\Models\User;
use App\Traits\ApiResponse;
use App\Helpers\Managers\TopupManager;


class BalanceController extends Controller
{
    use ApiResponse;

    public function topupList(Request $request)
    {
        $user = $request->user();

        $path = storage_path() . "/json/topup-list.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $json = json_decode(file_get_contents($path), true);

        // $serviceTimePath = storage_path() . "/json/service-time.json";
        // $serviceTimeJson = json_decode(file_get_contents($serviceTimePath), true);
        // $startTime = $serviceTimeJson[OrderType::TOPUP]['start'];
        // $endTime = $serviceTimeJson[OrderType::TOPUP]['end'];

        // $note = __('config/messages.operation_time', ['object' => 'Top Up', 'start_time' => $startTime, 'end_time' => $endTime]);
        $note = __('config/messages.balance_limit', ['balance' => 'Rp1.000.000']);

        return $this->jsonResponse("Success", array('topup' => $json, 'note' => $note), 200);
    }


    public function topup(Request $request)
    {
        $validatedData = $request->validate([
            'dest_bank_account_id' => 'required|exists:bank_accounts,id',
            'amount' => 'required|numeric'
        ]);

        try {
            $user = $request->user();

            $amount = $request->amount;
            $minimumAmount = 50000;

            if ($amount < $minimumAmount) throw new Exception(__('error/messages.min_topup', ['object' => GlobalHelper::toCurrency($minimumAmount)]), 400);

            $activeTopupOrder = $user->orders()->active()->type(OrderType::TOPUP)->first();
            if ($activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            DB::beginTransaction();
            
            $topupManager = new TopupManager($user);
            $order = $topupManager->create($referable, $amount, (int) $request->dest_bank_account_id);

            DB::commit();

            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function topupPaid(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            $activeTopupOrder = $order ?? $user->orders()->active()->type(OrderType::TOPUP)->first();
            if (!$activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            DB::beginTransaction();
            
            $topupManager = new TopupManager($user);
            $order = $topupManager->setOrder($activeTopupOrder)->paid();

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function topupCancel(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            $activeTopupOrder = $order ?? $user->orders()->active()->type(OrderType::TOPUP)->first();
            if (!$activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            if (!in_array($activeTopupOrder->status, [OrderStatus::PENDING, OrderStatus::WAITING_FOR_PAYMENT, OrderStatus::ONGOING])) {
                $str = implode(" or ", [OrderStatus::getString(OrderStatus::PENDING), OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT)]);
                throw new Exception(__('error/messages.order_cannot_cancel', ['object' => $str]), 400);
            }

            DB::beginTransaction();
            $topupManager = new TopupManager($user);
            $cancelOrder = $topupManager->setOrder($activeTopupOrder)->cancel();

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $activeTopupOrder->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}
