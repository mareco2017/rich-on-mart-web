<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Notifications\ResetPasswordNotification;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails, ApiResponse;

    /**
     * Create a new ForgotPasswordController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        try {
            $user = User::where('email', $request->input('email'))->first();
            if (!$user) {
                throw new Exception(trans('passwords.user'), 400);
            }

            $token = $this->broker()->createToken($user);

            $user->notify(new ResetPasswordNotification($token, $user));

            return $this->jsonResponse("Successfully sent reset password link!");
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}