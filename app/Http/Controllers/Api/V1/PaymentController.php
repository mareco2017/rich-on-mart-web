<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\AppConfig;
use App\Models\Admin;
use App\Models\Payment;
use App\Models\Order;
use App\Enums\PaymentType;
use App\Mail\PostPaidPLNReceipt;
use App\Helpers\External\PDAMHelper;
use App\Helpers\Managers\PDAMManager;
use App\Helpers\External\TelkomHelper;
use App\Helpers\Managers\TelkomManager;
use App\Helpers\Managers\TeleponManager;
use App\Helpers\Managers\InternetManager;
use App\Helpers\External\VoucherGameHelper;
use App\Helpers\Managers\VoucherGameManager;
use App\Helpers\External\PulsaHelper;
use App\Helpers\Managers\PulsaManager;
use App\Helpers\Managers\PaketManager;
use App\Helpers\External\ListrikHelper;
use App\Helpers\Managers\ListrikManager;

class PaymentController extends Controller
{
    use ApiResponse;

    public function list(Request $request)
    {
        $typeList = implode(',', PaymentType::getList());

        $validatedData = $request->validate([
            'type' => 'nullable|numeric|in:' . $typeList,
            'phone_number' => 'nullable|string|min:4',
            'phone_number_package' => 'required_with:phone_number|boolean'
        ]);

        try {
            $user = $request->user();
            $types = [];
            if ($request->phone_number) {
                $type = PaymentType::typeByPhoneNumber($request->phone_number, $request->phone_number_package);
                $types[] = $type;
            }
            if ($request->has('type')) {
                $types[] = $request->type;
            }

            $payments = Payment::type($types)->get();

            if (isset($type)) {
                $payments = $this->mapPrice($type, $payments, $request->phone_number);
            }

            return $this->jsonResponse("Success", array('payments' => $payments), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    protected function mapPrice($type, $payments = [], $phoneNumber)
    {
        if (PaymentType::isPulsa($type) || PaymentType::isPaket($type)) {
            $helper = new PulsaHelper;
            $inquiryRes = $helper->inquiry($phoneNumber);
            $removeCodes = [];

            if (is_array($inquiryRes) && $inquiryRes) {
                $data = collect($inquiryRes);
                $payments->map(function($payment) use ($data, &$removeCodes) {
                    $d = $data->firstWhere('product_id', $payment->code);
                    if ($d) {
                        $payment->price = $d->price;
                    } else {
                        $removeCodes[] = $payment->code;
                    }
                    return $payment;
                });
            } else {
                throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);
            }

            $filteredPayments = $payments->reject(function($value, $key) use ($removeCodes) {
                return in_array($value->code, $removeCodes);
            });
        }

        return $filteredPayments;
    }

    public function inquiryPDAM(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::PDAM) throw new Exception("Invalid payment type", 400);

            $helper = new PDAMHelper;
            $inquiry = $helper->inquiry($payment->code, $request->customer_id);

            // inquiry {"responseCode":"00","message":"Successful","subscriberID":"530000000001","nama":"SUBCRIBER NAME","tarif":"R1","daya":"1300","lembarTagihanTotal":1,"lembarTagihan":"1","detilTagihan":[{"periode":"201608","nilaiTagihan":"300000","denda":0,"admin":2500,"total":302500}],"totalTagihan":302500,"productCode":"PLNPOSTPAIDB","refID":"22230317"}

            if (!PaymentType::responseIsSuccess(PaymentType::PDAM, $inquiry->responseCode)) {
                \Log::error('inquiry pdam');
                \Log::error(json_encode($inquiry));
                throw new Exception($inquiry->message, 400);
            }

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $pdamManager = new PDAMManager($user);
            $order = $pdamManager->create($referable, $inquiry, $payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentPDAM(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();

            if (!$order->isActive()) throw new Exception("Inactive order", 400);

            $productCode = $order->options->inquiry->productCode;
            $refID = $order->options->inquiry->refID;
            $nominal = $order->total;

            $helper = new PDAMHelper;
            $payment = $helper->payment($productCode, $refID, $nominal);

            if (!PaymentType::responseIsSuccess(PaymentType::PDAM, $payment->responseCode)) {
                \Log::error('payment pdam');
                \Log::error(json_encode($payment));
                throw new Exception($payment->message, 400);
            }

            $pdamManager = new PDAMManager($user);
            switch ($payment->responseCode) {
                case '68':
                    $order = $pdamManager->setOrder($order)->ongoing($payment);
                    break;
                default:
                    $order = $pdamManager->setOrder($order)->paid($payment);
                    break;
            }

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function inquiryTelkom(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::TELKOM) throw new Exception("Invalid payment type", 400);

            $helper = new TelkomHelper;
            $inquiry = $helper->inquiry($payment->code, $request->customer_id);

            if (!PaymentType::responseIsSuccess(PaymentType::TELKOM, $inquiry->responseCode)) {
                \Log::error('inquiry telkom');
                \Log::error(json_encode($inquiry));
                throw new Exception($inquiry->message, 400);
            }

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $telkomManager = new TelkomManager($user);
            $order = $telkomManager->create($referable, $inquiry, $payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentTelkom(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();

            if (!$order->isActive()) throw new Exception("Inactive order", 400);

            $productCode = $order->options->inquiry->productCode;
            $refID = $order->options->inquiry->refID;
            $nominal = $order->total;

            $helper = new TelkomHelper;
            $payment = $helper->payment($productCode, $refID, $nominal);

            if (!PaymentType::responseIsSuccess(PaymentType::TELKOM, $payment->responseCode)) {
                \Log::error('payment telkom');
                \Log::error(json_encode($payment));
                throw new Exception($payment->message, 400);
            }

            $telkomManager = new TelkomManager($user);
            $order = $telkomManager->setOrder($order)->paid($payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function inquiryTelepon(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::TELEPON) throw new Exception("Invalid payment type", 400);

            $helper = new TelkomHelper;
            $inquiry = $helper->inquiry($payment->code, $request->customer_id);

            if (!PaymentType::responseIsSuccess(PaymentType::TELEPON, $inquiry->responseCode)) {
                \Log::error('inquiry telepon');
                \Log::error(json_encode($inquiry));
                throw new Exception($inquiry->message, 400);
            }

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $teleponManager = new TeleponManager($user);
            $order = $teleponManager->create($referable, $inquiry, $payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentTelepon(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();

            if (!$order->isActive()) throw new Exception("Inactive order", 400);

            $productCode = $order->options->inquiry->productCode;
            $refID = $order->options->inquiry->refID;
            $nominal = $order->total;

            $helper = new TelkomHelper;
            $payment = $helper->payment($productCode, $refID, $nominal);

            if (!PaymentType::responseIsSuccess(PaymentType::TELEPON, $payment->responseCode)) {
                \Log::error('payment telepon');
                \Log::error(json_encode($payment));
                throw new Exception($payment->message, 400);
            }

            $teleponManager = new TeleponManager($user);
            $order = $teleponManager->setOrder($order)->paid($payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function inquiryInternet(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::INTERNET) throw new Exception("Invalid payment type", 400);

            $helper = new TelkomHelper;
            $inquiry = $helper->inquiry($payment->code, $request->customer_id);

            if (!PaymentType::responseIsSuccess(PaymentType::INTERNET, $inquiry->responseCode)) {
                \Log::error('inquiry internet');
                \Log::error(json_encode($inquiry));
                throw new Exception($inquiry->message, 400);
            }

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $internetManager = new InternetManager($user);
            $order = $internetManager->create($referable, $inquiry, $payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentInternet(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();

            if (!$order->isActive()) throw new Exception("Inactive order", 400);

            $productCode = $order->options->inquiry->productCode;
            $refID = $order->options->inquiry->refID;
            $nominal = $order->total;

            $helper = new TelkomHelper;
            $payment = $helper->payment($productCode, $refID, $nominal);

            if (!PaymentType::responseIsSuccess(PaymentType::INTERNET, $payment->responseCode)) {
                \Log::error('payment internet');
                \Log::error(json_encode($payment));
                throw new Exception($payment->message, 400);
            }

            $internetManager = new InternetManager($user);
            $order = $internetManager->setOrder($order)->paid($payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function inquiryListrik(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::LISTRIK) throw new Exception("Invalid payment type", 400);

            $helper = new ListrikHelper;
            $inquiry = $helper->inquiry($payment->code, $request->customer_id);

            if (!PaymentType::responseIsSuccess(PaymentType::LISTRIK, $inquiry->responseCode)) {
                \Log::error('inquiry listrik');
                \Log::error(json_encode($inquiry));
                throw new Exception($inquiry->message, 400);
            }

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $pdamManager = new ListrikManager($user);
            $order = $pdamManager->create($referable, $inquiry, $payment);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentListrik(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            DB::beginTransaction();

            if (!$order->isActive()) throw new Exception("Inactive order", 400);

            $productCode = $order->options->inquiry->productCode;
            $refID = $order->options->inquiry->refID;
            $nominal = $order->total;

            $helper = new ListrikHelper;
            $payment = $helper->payment($productCode, $refID, $nominal);

            if (!PaymentType::responseIsSuccess(PaymentType::LISTRIK, $payment->responseCode)) {
                \Log::error('payment listrik');
                \Log::error(json_encode($payment));
                throw new Exception($payment->message, 400);
            }

            $pdamManager = new ListrikManager($user);
            $order = $pdamManager->setOrder($order)->paid($payment);
            
            DB::commit();

            Mail::to($order->ownable)->send(new PostPaidPLNReceipt($order));
            
            return $this->jsonResponse("Success", array('order' => $order), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentVoucherGame(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'customer_id' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if ($payment->type != PaymentType::VOUCHER_GAME) throw new Exception("Invalid payment type", 400);

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            $voucherGameManager = new VoucherGameManager($user);
            $order = $voucherGameManager->create($referable, $payment);

            $helper = new VoucherGameHelper;
            $paymentRes = $helper->payment($payment->code, $payment->price, $order->reference_number);

            if (!PaymentType::responseIsSuccess(PaymentType::VOUCHER_GAME, $paymentRes->responseCode)) {
                \Log::error('payment voucher game');
                \Log::error(json_encode($paymentRes));
                throw new Exception($paymentRes->message, 400);
            }

            $paidOrder = $voucherGameManager->setOrder($order)->paid($paymentRes);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $paidOrder), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentPulsa(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'phone_number' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if (!PaymentType::isPulsa($payment->type)) throw new Exception("Invalid payment type", 400);

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);


            $helper = new PulsaHelper;
            $product = $helper->productList($payment->code);
            if ($product) {
                if (is_array($product)) $product = $product[0];
                $payment->price = $product->harga;
            } else {
                \Log::error('payment pulsa 1');
                \Log::error(json_encode($product));
                throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);
            }

            $pulsaManager = new PulsaManager($user);
            $order = $pulsaManager->create($referable, $payment, ['phone_number' => $request->phone_number]);

            $paymentRes = $helper->payment($payment->code, $request->phone_number, $payment->price);

            if (!PaymentType::responseIsSuccess($payment->type, $paymentRes->status)) {
                \Log::error('payment pulsa 2');
                \Log::error(json_encode($paymentRes));
                throw new Exception($paymentRes->msg, 400);
            }

            $paidOrder = $pulsaManager->setOrder($order)->paid($paymentRes);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $paidOrder), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function paymentPaket(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|exists:payments,id',
            'phone_number' => 'required|string'
        ]);

        try {
            $user = $request->user();

            DB::beginTransaction();

            $payment = Payment::find($request->id);
            if (!$payment) throw new Exception("Invalid payment", 404);
            if (!$payment->active) throw new Exception("Inactive payment", 400);
            if (!PaymentType::isPaket($payment->type)) throw new Exception("Invalid payment type", 400);

            $id = AppConfig::where('key', 'wallet_account_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);


            $helper = new PulsaHelper;
            $product = $helper->productList($payment->code);
            if ($product) {
                if (is_array($product)) $product = $product[0];
                $payment->price = $product->harga;
            } else {
                \Log::error('payment paket 1');
                \Log::error(json_encode($product));
                throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);
            }

            $paketManager = new PaketManager($user);
            $order = $paketManager->create($referable, $payment, ['phone_number' => $request->phone_number]);

            $paymentRes = $helper->payment($payment->code, $request->phone_number, $payment->price);

            if (!PaymentType::responseIsSuccess($payment->type, $paymentRes->status)) {
                \Log::error('payment paket 2');
                \Log::error(json_encode($paymentRes));
                throw new Exception($paymentRes->msg, 400);
            }

            $paidOrder = $paketManager->setOrder($order)->paid($paymentRes);

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $paidOrder), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }
}