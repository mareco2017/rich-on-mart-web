<?php

namespace App\Http\Controllers\Webview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AppConfig;
use Illuminate\Support\Facades\Auth;

class AboutAppController extends Controller
{

    public function termsConditions(Request $request)
    {
        $data = AppConfig::where('key', 'terms_conditions')->first();
        return view('webview.about-app.terms-conditions')
            ->with('data', $data);
    }
  
    public function privacyPolicy(Request $request)
    {
        $data = AppConfig::where('key', 'privacy_policy')->first();
        return view('webview.about-app.privacy-policy')
            ->with('data', $data);
    }
  
    public function aboutUs(Request $request)
    {
        $data = AppConfig::where('key', 'about_us')->first();
        return view('webview.about-app.about_us')
            ->with('data', $data);
    }
  
    public function packageInfo(Request $request)
    {
        $user = $this->guard()->user();
        
        $phoneNumber = AppConfig::where('key', 'package_registration_number')->value('value');
        return view('webview.package-info')
            ->with('user', $user)
            ->with('phoneNumber', $phoneNumber);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}