<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\NotificationType;

class Notification extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sendable_type', 'sendable_id', 'type', 'status', 'receivable_type', 'receivable_id', 'title', 'description', 'options', 'read'
    ];

    //Type = Identity, Top Up, Withdraw, Pay, Transfer, Receive, -
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['order_id'];
    protected $hidden = [];
    protected $dates = ['deleted_at'];
    protected $casts = [
        'options' => 'array',
    ];

    public function sendable()
    {
        return $this->morphTo();
    }

    public function receivable()
    {
        return $this->morphTo();
    }
    
    public function scopeOwn($query, $user)
    {
        $userId = 'nope';
        if ($user instanceof User) {
            $userId = $user->id;
            $receivable = $user->getMorphClass();
        } else if ($user instanceof Business) {
            $userId = $user;
            $receivable = $user->getMorphClass();
        } else if (is_string($user) || is_int($user)) {
            $userId = $user;
            $receivable = null;
        }
        return $query->where('receivable_type', $receivable)->where('receivable_id', $userId)->whereNotIn('type', [NotificationType::PLN_PREPAID, NotificationType::PLN_POSTPAID]);
    }

    public function scopeUnread($query)
    {
        return $query->where('read', 0);
    }

    public function getOrderIdAttribute($value)
    {
        if (array_key_exists('order_id', $this->options))
        {
            return $this->options['order_id'];
        }
        return null;
    }
}
