<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\WalletType;

class Wallet extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ownable_type', 'ownable_id', 'balance', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function ownable()
    {
        return $this->morphTo();
    }

    public function scopeForWithdraw($query)
    {
        return $query->whereIn('type', [WalletType::PRIMARY]);
    }

    public function deduct($amount = 0)
    {
        // Add log function on deduct
        $this->balance -= $amount;
        $this->save();
        return $this;
    }

    public function add($amount = 0)
    {
        // Add log function on add
        $this->balance += $amount;
        $this->save();
        return $this;
    }
}
