<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use App\Enums\UserDocumentStatus;

class UserDocument extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'verifiable_type', 'verifiable_id', 'type', 'status', 'description', 'identification_number'
    ];

    protected $appends = [
        'attachment_url', 'status_string'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];
    protected $hidden = ['attachment'];
    protected $dates = ['verified_date', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function verifiable()
    {
        return $this->morphTo();
    }

    public function attachment()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function getAttachmentUrlAttribute($value)
    {
        return $this->attachment->url;
    }

    public function getStatusStringAttribute($value)
    {
        return UserDocumentStatus::getString($this->status);
    }
}
