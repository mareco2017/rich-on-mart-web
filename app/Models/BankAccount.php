<?php

namespace App\Models;

use App\Models\Bank;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class BankAccount extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ownable_type', 'ownable_id', 'account_name', 'account_no', 'branch', 'is_primary'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $with = ['bank'];
    protected $appends = [];
    protected $hidden = [];
    protected $dates = ['deleted_at'];
    
    public function ownable()
    {
        return $this->morphTo();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
    
    public function getBankAccountStringAttribute($value)
    {
        $bank = $this->bank ?: null;
        $str = $bank ? $bank->abbr : "-";
        $str .= " / ";
        $str .= $this->account_no;
        return $str;
    }

    public function ownedBy($user)
    {
        if (!$this->ownable) return false;
        $className = get_class($this->ownable);
        if ($user instanceof $className) {
            return $user->id == $this->ownable->id;
        }
        return false;
    }
}
