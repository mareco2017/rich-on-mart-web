<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\ActiveStatus;
use App\Enums\ProductUnit;

class Product extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'price', 'discount', 'status', 'category_id', 'cover_id'
    ];

    protected $appends = [
        'unit_string', 'calculated_price', 'cover_url'
    ];

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE);
    }

    public function getCoverUrlAttribute($value)
    {
        if ($this->cover) {
            return $this->cover->url;
        }
        return '';
    }

    public function getUnitStringAttribute($value)
    {
        return ProductUnit::getString($this->unit);
    }

    public function getCalculatedPriceAttribute($value)
    {
        return $this->price - $this->discount + $this->category->commission;
    }
}
