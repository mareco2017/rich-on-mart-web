<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Enums\UserVerificationType;
use App\Enums\UserDocumentType;
use App\Enums\UserDocumentStatus;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_number', 'password', 'packages'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'google' => 'object',
        'facebook' => 'object',
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
    ];

    protected $appends  = [
        'fullname',
        'phone_number_verified',
        'email_verified',
        'documents_status'
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function tokens()
    {
        return $this->hasMany('App\Models\UserToken');
    }

    public function verifications()
    {
        return $this->hasMany('App\Models\UserVerification');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\UserDocument');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\UserAddress');
    }

    public function wallets()
    {
        return $this->morphMany('App\Models\Wallet', 'ownable');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'ownable');
    }

    public function bankAccounts()
    {
        return $this->morphMany('App\Models\BankAccount', 'ownable');
    }

    public function getPhoneNumberVerifiedAttribute()
    {
        $v = $this->verifications()->type(UserVerificationType::PHONE_NUMBER)->latest()->first();
        return $v ? $v->isVerified() : false;
    }

    public function getEmailVerifiedAttribute()
    {
        $v = $this->verifications()->type(UserVerificationType::EMAIL)->latest()->first();
        return $v ? $v->isVerified() : false;
    }

    public function getFullnameAttribute($value)
    {
        return $this->last_name ? $this->first_name . ' ' . $this->last_name : $this->first_name;
    }

    public function getDocumentsStatusAttribute($value)
    {
        $nric = $this->documents()->type(UserDocumentType::NRIC)->first();
        $selfie = $this->documents()->type(UserDocumentType::SELFIE)->first();
        $verifications = [
            // 'phone_number' => $this->verifications()->where('type', VerificationType::PHONE_NUMBER)->where('status', VerificationStatus::VERIFIED)->first() ? true : false,
            // 'email' => $this->verifications()->where('type', VerificationType::EMAIL)->where('status', VerificationStatus::VERIFIED)->first() ? true : false,
            'nric' => $nric ? $nric->status : 2,
            'selfie' => $selfie ? $selfie->status : 2
        ];
        return $verifications;
    }

    public function getDocument($type)
    {
        return $this->documents()->where('type', $type)->first();
    }

    public function getWallet($type)
    {
        return $this->wallets()->where('type', $type)->first();
    }

    public function scopeDocumentPending($query)
    {
        return $query->whereHas('documents', function($q) {
            return $q->where('type', UserDocumentType::NRIC)->where('status', UserDocumentStatus::PENDING);
        });
    }

    public function primaryAddress()
    {
        return $this->addresses->count() ? $this->addresses()->where('is_primary', 1)->first() : '';
    }

}
