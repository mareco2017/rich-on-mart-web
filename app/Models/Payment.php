<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'icon_id', 'code', 'price', 'discount', 'fee', 'type', 'active', 'options'
    ];

    protected $appends = [
        'final_price', 'icon_url'
    ];

    protected $casts = [
        'options' => 'object'
    ];

    protected $hidden = ['icon'];

    protected $dates = ['deleted_at'];

    public function icon()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeType($query, $type = null)
    {
        if (is_null($type)) return $query;
        if (is_array($type)) return $query->whereIn('type', $type);
        return $query->where('type', $type);
    }

    public function getFinalPriceAttribute()
    {
        return $this->price - $this->discount + $this->fee;
    }

    public function getIconUrlAttribute()
    {
        return $this->icon ? $this->icon->url : '';
    }
}
