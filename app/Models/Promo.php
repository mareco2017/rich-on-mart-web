<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use App\Enums\PromoCategory;
use App\Enums\ActiveStatus;
use Carbon\Carbon;

class Promo extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creatable_type',  'creatable_id', 'title', 'description', 'code', 'start_date', 'end_date',
        'type', 'category', 'status', 'urut', 'minimum_purchase', 'amount', 'sub_type'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['cover_url', 'share_url'];
    protected $casts = [
        'tnc' => 'array',
        'options' => 'array'
    ];
    protected $hidden = [];
    protected $dates = ['start_date', 'end_date', 'deleted_at'];
    
    public function creatable()
    {
        return $this->morphTo();
    }

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }
    
    // public function business()
    // {
    //     return $this->belongsTo('App\Models\Business');
    // }
    
    public function authorizedBy()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function scopeVoucher($query)
    {
        return $query->where('category', PromoCategory::VOUCHER);
    }

    public function scopeBanner($query)
    {
        return $query->where('category', PromoCategory::BANNER);
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE)->where('end_date', '>=', Carbon::now()->subDay());
    }

    public function getCoverUrlAttribute($value)
    {
        if ($this->cover) {
            return $this->cover->url;
        }
        return '';
    }

    public function getShareUrlAttribute($value)
    {
        if ($this->route_name){
            return route($this->route_name, ['promo' => $this]);
        } else {
            return null;
        }
    }
}
