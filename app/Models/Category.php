<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\ActiveStatus;

class Category extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'commission', 'discount', 'status', 'cover_id'
    ];

    protected $appends  = [
        'cover_url',
    ];

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE);
    }

    public function getCoverUrlAttribute($value)
    {
        if ($this->cover) {
            return $this->cover->url;
        }
        return '';
    }
}
