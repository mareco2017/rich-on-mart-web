<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transaction;
use App\Models\BankAccount;
use App\Enums\OrderStatus;
use App\Enums\OrderType;
use DB;
use Carbon\Carbon;

class Order extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'status', 'transaction_id', 'referable_type', 'referable_id', 'ownable_type', 'ownable_id', 'expired_at', 'review_id', 'options'
    ];

    //Type -> Top Up(0) | Withdraw(1)
    //Status -> Pending(0) | Accepted(1) | Expired(2) | Cancelled(3) | Declined(4)

    protected $appends = [
        'virtual', 'total', 'status_string'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];

    protected $dates = ['expired_at', 'deleted_at'];

    public function referable()
    {
        return $this->morphTo();
    }

    public function ownable()
    {
        return $this->morphTo();
    }

    public function approvable()
    {
        $options = is_object($this->options) ? (array) $this->options : $this->options;
        $id = isset($options['approvable_id']) ? $options['approvable_id'] : null;
        if (!$id) return null;

        $typeValue = isset($options['approvable_type']) ? $options['approvable_type'] : '';
        $class = Model::getActualClassNameForMorph($typeValue);
        $model = new $class();

        return $model::find($id);
    }

    public function recheckable()
    {
        $options = is_object($this->options) ? (array) $this->options : $this->options;
        $id = isset($options['recheckable_id']) ? $options['recheckable_id'] : null;
        if (!$id) return null;

        $typeValue = isset($options['recheckable_type']) ? $options['recheckable_type'] : '';
        $class = Model::getActualClassNameForMorph($typeValue);
        $model = new $class();

        return $model::find($id);
    }

    public function orderDetails()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function histories()
    {
        return $this->morphMany('App\Models\History', 'historiable');
    }

    public function scopeType($query, $type)
    {
        if (is_array($type)) {
            return $query->whereIn('type', $type);
        } else {
            return $query->where('type', $type);
        }
    }

    public function scopeStatus($query, $status)
    {
        if (is_array($status)) {
            return $query->whereIn('status', $status);
        } else {
            return $query->where('status', $status);
        }
    }

    public function scopeActive($query)
    {
        $activeStatus = OrderStatus::getActiveList();
        return $query->whereIn('status', $activeStatus);
    }

    public function scopeInactive($query)
    {
        $activeStatus = OrderStatus::getActiveList();
        return $query->whereNotIn('status', $activeStatus);
    }

    public function scopeComplete($query)
    {
        return $query->where('status', OrderStatus::COMPLETED);
    }

    public function isActive()
    {
        $activeStatus = OrderStatus::getActiveList();
        return in_array($this->status, $activeStatus);
    }

    public function ownedBy($user)
    {
        if (!$this->ownable) return false;
        $className = get_class($this->ownable);
        if ($user instanceof $className) {
            return $user->id == $this->ownable->id;
        }
        return false;
    }

    public function getVirtualAttribute($value)
    {
        $options = $this->options;
        if (isset($options->dest_bank_account_id)) {
            $bankAccount = BankAccount::find($options->dest_bank_account_id);
            if ($bankAccount) $bankAccount->load('bank');
            $options->dest_bank_account = $bankAccount;
        }
        if (isset($options->bank_account_id)) {
            $bankAccount = BankAccount::find($options->bank_account_id);
            if ($bankAccount) $bankAccount->load('bank');
            $options->bank_account = $bankAccount;
        }
        return $options;
    }

    public function getTotalAttribute($value)
    {
        $result = DB::table('order_details')->where('order_id', $this->id)->selectRaw('SUM(total - discount) as sub_total, order_id')->groupBy('order_id')->first();

        return $result ? $result->sub_total : 0;
    }

    public function getTotalDiscountAttribute($value)
    {
        $result = $this->orderDetails->sum('discount');
        
        return $result;
    }

    public function getTotalBeforeDiscountAttribute($value)
    {
        $result = $this->orderDetails->sum('total');
        
        return $result;
    }

    public function getStatusStringAttribute($value)
    {
        return OrderStatus::getString($this->status);
    }

    public function history($status = null)
    {
        $query = $this->histories();
        $query->latest();
        if ($status) $query->where('status', $status);
        $history = $query->first();

        return $history;
    }

    public function scopeRecent($query)
    {
        return $query->where('created_at', '>', Carbon::now()->subDays(30));
    }
}
