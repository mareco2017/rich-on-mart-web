<?php

namespace App\Enums;

final class UserVerificationType {

	const PHONE_NUMBER = 0;
	const EMAIL = 1;
	const OTHER = 2;

	public static function getList() {
		return [
			UserVerificationType::PHONE_NUMBER,
			UserVerificationType::EMAIL,
			UserVerificationType::OTHER
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Phone Number";
			case 1:
				return "Email";
			case 2:
				return "Other";
		}
	}

}

?>
