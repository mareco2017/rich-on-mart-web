<?php

namespace App\Enums;

final class PackageType {

	const FREE = 0;
	const SILVER = 1;
	const GOLD = 2;
	const PLATINUM = 3;

	public static function getList() {
		return [
			PackageType::FREE,
			PackageType::SILVER,
			PackageType::GOLD,
			PackageType::PLATINUM
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Free";
			case 1:
				return "Silver";
			case 2:
				return "Gold";
			case 3:
				return "Platinum";
		}
	}

	public static function getPrice($val) {
		switch ($val) {
			case 0:
				return 0;
			case 1:
				return 1000000;
			case 2:
				return 3000000;
			case 3:
				return 5000000;
		}
	}

	public static function getPoint($val) {
		switch ($val) {
			case 0:
				return 0;
			case 1:
				return 1000;
			case 2:
				return 3000;
			case 3:
				return 5000;
		}
	}

	public static function getSponsorBonus($val) {
		switch ($val) {
			case 0:
				return 0;
			case 1:
				return 200000;
			case 2:
				return 600000;
			case 3:
				return 1000000;
		}
	}

	public static function getPairingBonus($val) {
		switch ($val) {
			case 0:
				return 0;
			case 1:
				return 100000;
			case 2:
				return 200000;
			case 3:
				return 300000;
		}
	}

}

?>
