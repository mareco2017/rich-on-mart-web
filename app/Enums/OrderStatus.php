<?php

namespace App\Enums;

final class OrderStatus {

	const PENDING = 0;
	const WAITING_FOR_PAYMENT = 1;
	const ONGOING = 2;
	const COMPLETED = 3;
	const EXPIRED = 4;
	const CANCELLED = 5;
	const DECLINED = 6;
	const REFUNDED = 7;

	const PAYMENT_CONFIRMED = 20;
	const PURCHASING_ITEM = 21;
	const ON_THE_WAY = 22;

	public static function getList() {
		return [
			OrderStatus::PENDING,
			OrderStatus::WAITING_FOR_PAYMENT,
			OrderStatus::ONGOING,
			OrderStatus::COMPLETED,
			OrderStatus::EXPIRED,
			OrderStatus::CANCELLED,
			OrderStatus::DECLINED,
			OrderStatus::REFUNDED,
			OrderStatus::PAYMENT_CONFIRMED,
			OrderStatus::PURCHASING_ITEM,
			OrderStatus::ON_THE_WAY,
		];
	}

	public static function getActiveList()
	{
		return [
			OrderStatus::PENDING,
			OrderStatus::WAITING_FOR_PAYMENT,
			OrderStatus::ONGOING
		];
	}

	public static function getShowList()
	{
		return [
			OrderStatus::PENDING, // pending utk withdraw
			OrderStatus::COMPLETED,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Waiting for payment";
			case 2:
				return "Ongoing";
			case 3:
				return "Completed";
			case 4:
				return "Expired";
			case 5:
				return "Cancelled";
			case 6:
				return "Declined";
			case 7:
				return "Refunded";
			case 20:
				return "Payment Confirmed";
			case 21:
				return "Purchasing Item";
			case 22:
				return "On The Way";
		}
	}

}

?>
