<?php

namespace App\Enums;

final class PaymentType {

	const PDAM = 0;
	const TELKOM = 1;
	const VOUCHER_GAME = 2;
	const TELEPON = 3;
	const INTERNET = 4;
	const LISTRIK = 5;
	const TOKEN_LISTRIK = 6;

	const PULSA_TELKOMSEL = 10;
	const PULSA_XL = 11;
	const PULSA_AXIS = 12;
	const PULSA_INDOSAT = 13;
	const PULSA_THREE = 14;

	const PAKET_TELKOMSEL = 20;
	const PAKET_XL = 21;
	const PAKET_AXIS = 22;
	const PAKET_INDOSAT = 23;
	const PAKET_THREE = 24;

	public static function getList() {
		return [
			PaymentType::PDAM,
			PaymentType::TELKOM,
			PaymentType::VOUCHER_GAME,
			PaymentType::TELEPON,
			PaymentType::INTERNET,
			PaymentType::LISTRIK,
			PaymentType::TOKEN_LISTRIK,
			PaymentType::PULSA_TELKOMSEL,
			PaymentType::PULSA_XL,
			PaymentType::PULSA_AXIS,
			PaymentType::PULSA_INDOSAT,
			PaymentType::PULSA_THREE,
			PaymentType::PAKET_TELKOMSEL,
			PaymentType::PAKET_XL,
			PaymentType::PAKET_AXIS,
			PaymentType::PAKET_INDOSAT,
			PaymentType::PAKET_THREE
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return 'PDAM';
			case 1:
				return 'Telkom';
			case self::VOUCHER_GAME:
				return 'Voucher Game';
			case self::TELEPON:
				return 'Telepon';
			case self::INTERNET:
				return 'Internet';
			case self::LISTRIK:
				return 'Listrik';
			case self::TOKEN_LISTRIK:
				return 'Token Listrik';
			case self::PULSA_TELKOMSEL:
				return 'Pulsa Telkomsel';
			case self::PULSA_XL:
				return 'Pulsa XL';
			case self::PULSA_AXIS:
				return 'Pulsa Axis';
			case self::PULSA_INDOSAT:
				return 'Pulsa Indosat';
			case self::PULSA_THREE:
				return 'Pulsa Three';
			case self::PAKET_TELKOMSEL:
				return 'Paket Telkomsel';
			case self::PAKET_XL:
				return 'Paket XL';
			case self::PAKET_AXIS:
				return 'Paket Axis';
			case self::PAKET_INDOSAT:
				return 'Paket Indosat';
			case self::PAKET_THREE:
				return 'Paket Three';
		}
	}

	public static function responseIsSuccess($type, $code) {
		switch ($type) {
			case self::PDAM:
				$array = ["00", "68"];
				break;
			case self::TELKOM:
				$array = ["00"];
				break;
			case self::VOUCHER_GAME:
				$array = ["00"];
				break;
			case self::TELEPON:
				$array = ["00"];
				break;
			case self::INTERNET:
				$array = ["00"];
				break;
			case self::LISTRIK:
				$array = ["00"];
				break;
			case self::TOKEN_LISTRIK:
				$array = ["00"];
				break;
			case self::PULSA_TELKOMSEL:
				$array = ["00"];
				break;
			case self::PULSA_XL:
				$array = ["00"];
				break;
			case self::PULSA_AXIS:
				$array = ["00"];
				break;
			case self::PULSA_INDOSAT:
				$array = ["00"];
				break;
			case self::PULSA_THREE:
				$array = ["00"];
				break;
			case self::PAKET_TELKOMSEL:
				$array = ["00"];
				break;
			case self::PAKET_XL:
				$array = ["00"];
				break;
			case self::PAKET_AXIS:
				$array = ["00"];
				break;
			case self::PAKET_INDOSAT:
				$array = ["00"];
				break;
			case self::PAKET_THREE:
				$array = ["00"];
				break;
			default:
				$array = [];
				break;
		}

		return in_array($code, $array);
	}

	public static function typeByPhoneNumber($phoneNumber, $paket = false)
	{
		$type = null;
		$phoneNumber = substr($phoneNumber, 0, 4);
		if (in_array($phoneNumber, ["0811", "0812", "0813", "0821", "0822", "0823", "0852", "0853"])) {
			// Telkomsel
			$type = $paket ? self::PAKET_TELKOMSEL : self::PULSA_TELKOMSEL;
		} else if (in_array($phoneNumber, ["0817", "0818", "0819", "0859", "0877", "0878"])) {
			// XL
			$type = $paket ? self::PAKET_XL : self::PULSA_XL;
		} else if (in_array($phoneNumber, ["0831", "0832", "0833", "0837"])) {
			// Axis
			$type = $paket ? self::PAKET_AXIS : self::PULSA_AXIS;
		} else if (in_array($phoneNumber, ["0814", "0815", "0816", "0855", "0856", "0857", "0858"])) {
			// Indosat
			$type = $paket ? self::PAKET_INDOSAT : self::PULSA_INDOSAT;
		} else if (in_array($phoneNumber, ["0895", "0896", "0897", "0898", "0899"])) {
			// 3
			$type = $paket ? self::PAKET_THREE : self::PULSA_THREE;
		}

		return $type;
	}

	public static function isPulsa($type)
	{
		return in_array($type, [self::PULSA_TELKOMSEL, self::PULSA_XL, self::PULSA_AXIS, self::PULSA_INDOSAT, self::PULSA_THREE]);
	}

	public static function isPaket($type)
	{
		return in_array($type, [self::PAKET_TELKOMSEL, self::PAKET_XL, self::PAKET_AXIS, self::PAKET_INDOSAT, self::PAKET_THREE]);
	}
}

?>
