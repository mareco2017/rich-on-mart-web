<?php

namespace App\Enums;

final class PromoCategory {

	const NEWS = 0;
	const BANNER = 1;

	public static function getList() {
		return [
			PromoCategory::NEWS,
			PromoCategory::BANNER
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "News";
			case 1:
				return "Banner";
		}
	}

}

?>
