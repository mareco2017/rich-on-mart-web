<?php

namespace App\Enums;

final class OrderType {

	const TOPUP = 0;

	const PAYMENT_PDAM = 11;
	const PAYMENT_TELKOM = 12;
	const PAYMENT_VOUCHER_GAME = 13;
	const PAYMENT_PULSA = 14;
	const PAYMENT_TELEPON = 15;
	const PAYMENT_INTERNET = 16;
	const PAYMENT_LISTRIK = 17;
	const PAYMENT_TOKEN_LISTRIK = 18;
	const PAYMENT_PAKET = 19;
	const GROCERY = 80;

	public static function getList() {
		return [
			OrderType::TOPUP,
			OrderType::PAYMENT_PDAM,
			OrderType::PAYMENT_TELKOM,
			OrderType::PAYMENT_VOUCHER_GAME,
			OrderType::PAYMENT_PULSA,
			OrderType::PAYMENT_TELEPON,
			OrderType::PAYMENT_INTERNET,
			OrderType::PAYMENT_LISTRIK,
			OrderType::PAYMENT_TOKEN_LISTRIK,
			OrderType::PAYMENT_PAKET,
			OrderType::GROCERY,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Top Up";
			case self::PAYMENT_PDAM:
				return "PDAM Payment";
			case self::PAYMENT_TELKOM:
				return "Telkom Payment";
			case self::PAYMENT_VOUCHER_GAME:
				return "Voucher Game Payment";
			case self::PAYMENT_PULSA:
				return "Pulsa Payment";
			case self::PAYMENT_TELEPON:
				return "Telepon Payment";
			case self::PAYMENT_INTERNET:
				return "Internet Payment";
			case self::PAYMENT_LISTRIK:
				return "Listrik Payment";
			case self::PAYMENT_TOKEN_LISTRIK:
				return "Token Listrik Payment";
			case self::PAYMENT_PAKET:
				return "Paket Payment";
			case self::GROCERY:
				return "Grocery";
		}
	}

	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "TOP";
			case self::PAYMENT_PDAM:
				return "PDM";
			case self::PAYMENT_TELKOM:
				return "TLK";
			case self::PAYMENT_VOUCHER_GAME:
				return "VGM";
			case self::PAYMENT_PULSA:
				return "PLS";
			case self::PAYMENT_TELEPON:
				return "TLP";
			case self::PAYMENT_INTERNET:
				return "INT";
			case self::PAYMENT_LISTRIK:
				return "LST";
			case self::PAYMENT_TOKEN_LISTRIK:
				return "LTK";
			case self::PAYMENT_PAKET:
				return "PKT";
			case self::GROCERY:
				return "ROM";
		}
	}

	public static function getPaymentList() {
		return [
			OrderType::PAYMENT_PDAM,
			OrderType::PAYMENT_TELKOM,
			OrderType::PAYMENT_VOUCHER_GAME,
			OrderType::PAYMENT_PULSA,
			OrderType::PAYMENT_TELEPON,
			OrderType::PAYMENT_INTERNET,
			OrderType::PAYMENT_LISTRIK,
			OrderType::PAYMENT_TOKEN_LISTRIK,
			OrderType::PAYMENT_PAKET,
		];
	}
}

?>
