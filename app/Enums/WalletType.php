<?php

namespace App\Enums;

final class WalletType {

	const PRIMARY = 0;
	const SECONDARY = 1;
	const POINT = 2;
	const FROZEN = 3;
	const CREDIT = 4;

	public static function getList() {
		return [
			WalletType::PRIMARY,
			WalletType::SECONDARY,
			WalletType::POINT,
			WalletType::FROZEN,
			WalletType::CREDIT
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Primary";
			case 1:
				return "Secondary";
			case 2:
				return "Point";
			case 3:
				return "Frozen";
			case 4:
				return "Credit";
		}
	}

	public static function getName($val) {
		switch ($val) {
			case 0:
				return "primary";
			case 1:
				return "secondary";
			case 2:
				return "point";
			case 3:
				return "frozen";
			case 4:
				return "credit";
		}
	}

}

?>
