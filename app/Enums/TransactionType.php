<?php

namespace App\Enums;

final class TransactionType {

	const TOPUP = 0;

	public static function getList() {
		return [
			TransactionType::TOPUP
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Top Up";

		}
	}

	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "TOP";
		}
	}
}

?>
