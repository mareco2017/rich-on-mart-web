<?php

namespace App\Enums;

final class UserVerificationStatus {

	const PENDING = 0;
	const VERIFIED = 1;
	const REJECTED = 2;

	public static function getList() {
		return [
			UserVerificationType::PENDING,
			UserVerificationType::VERIFIED,
			UserVerificationType::REJECTED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Verified";
			case 2:
				return "Rejected";
		}
	}

}

?>
