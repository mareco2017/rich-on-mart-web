<?php

namespace App\Enums;

final class ProductUnit {

	const KG = 0;
	const ML = 1;
	const L = 2;
	const PIECE = 3;
	const PACKET = 4;
	const GR = 5;

	public static function getList() {
		return [
			ProductUnit::KG,
			ProductUnit::ML,
			ProductUnit::L,
			ProductUnit::PIECE,
			ProductUnit::PACKET,
			ProductUnit::GR
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return 'Kg';
			case 1:
				return 'ml';
			case 2:
				return 'L';
			case 3:
				return 'Piece';
			case 4:
				return 'Packet';
			case 5:
				return 'gr';
		}
	}

}

?>
