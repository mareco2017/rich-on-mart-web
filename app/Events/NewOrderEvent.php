<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Order;

class NewOrderEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(Order $order = null)
    {
        $this->order = $order;
    }

    public function broadcastAs()
    {
        return 'order.created';
    }

    public function broadcastOn()
    {
        return new Channel('admin.channel');
    }

    public function broadcastWith()
    {
        return [
            'order' => $this->order
        ];
    }
}
